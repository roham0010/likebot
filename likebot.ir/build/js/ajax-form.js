function ajax(url, method, data)
{
    method = typeof method != 'undefined' ? method : 'post';
    data = typeof data != 'undefined' ? data : {};
    data = Object.assign({_token: $("meta[name='_token']").attr('content'), ajax:'true'}, data);
    $.ajax({url: url, method: method, data: data, 
        success: function(data, textStatus, jqXHR)
        {
            return ajax_callback(data);
        }
    });
}

$(document).on('submit', '.ajax-form', function (e) {
    e.preventDefault();
    // if (submitting)
    //     return false;
    submitting = true;
    var thisForm = $(this);
    // submittingForm = $(thisForm.data("target"));
    var btnSubmit = $(this).find("button[type='submit']");
    if (undefined != thisForm.attr('formLoading')) {
        var targetContainer = $(thisForm.attr("target"));
        var targetForm = targetContainer.find('form').first();
        var loadingTarget = thisForm.parent(".card");
        loadingTarget.addClass('loading-card');
        // $(thisForm).hide();
    } else {
        btnSubmit.removeClass('loading').removeClass('done').addClass('loading');
    }
    // var data = $(this).serialize();
    // data = $(this).serialize();
    var data = new FormData($(this)[0]);
    data.append('ajax', 'true');
    data.append('_token', $("meta[name='_token']").attr('content'));
    if(typeof files != 'undefined') {
        $.each(files, function(key, value){
            data.append(key, value);
        });
        
        files = '';
    }
    // data += '&ajax=true&_token=' + $("meta[name='_token']").attr('content');
    // action=$(".ajax-btn:focus").attr('myaction');
    var action = '';
    action = $(".ajax-btn:focus").attr('myaction');
    action = action == undefined ? $(this).prop('action') : action;
    type = thisForm.attr('method') != undefined ? thisForm.attr('method') : 'post';
    if (action)
        $.ajax({
            url:action,
            type: type,
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            enctype: $(this).attr('enctype'),
            success: function(data, textStatus, jqXHR)
            {
                data.thisForm = thisForm;
                data.targetForm = targetForm;
                if (undefined != thisForm.attr('formLoading')) {
                    data.targetContainer = targetContainer;
                    data.targetForm = targetForm;
                    data.loadingTarget = loadingTarget;
                    data.btnSubmit = btnSubmit;
                }
                return ajax_callback(data);
            }
        }).always(function () {
            if (undefined != thisForm.attr('formLoading')) {
                loadingTarget.removeClass('loading-card');
            } else {
                btnSubmit.removeClass('loading').removeClass('done').addClass('done');
            }
            submitting = false;
            $('button').each(function(i, obj) {
                $this = $(obj);
                if($this.hasClass('loading-card')) {
                    console.log(obj, i)
                    $(obj).removeClass('loading-card');
                    $(obj).removeClass('disabled');
                }
                //test
            });
        });
        // $.post(action,
        //     data,
        //     function (data) {
                
        //     'json').done(function () {
        //         //alert("second success");
        //     }).fail(function () {
        //         //alert("error");
        //     }).always(function () {
        //         if (undefined != thisForm.attr('formLoading'))
        //             loadingTarget.removeClass('loading-card');
        //         else
        //             btnSubmit.removeClass('loading').removeClass('done').addClass('done');
        //         submitting = false;
        //     });
    return false;
});
var data = 'ajax=true&_token=' + $("meta[name='_token']").attr('content')



function ajax_callback(data) {
    if(data == 'false' || data == 'true' || data == 'ok' || data == 'OK') {
        return false;
    }
    if(typeof data.result == 'object') {
        actions(data.result);
    }
    if (undefined != data) {
        if (undefined != data.status && 'OK' == data.status) {
            if(undefined != data.result && undefined != data.result.redirectTo && data.result.redirectTo.length > 0)
            {
                window.location.href=data.result.redirectTo;
            }
            if (undefined != data.thisForm && undefined != data.thisForm.attr('formLoading')) {
                if (undefined != data.result && undefined != data.result.targetContainer){
                    data.targetContainer = $(data.result.targetContainer);
                    delete data.result.targetContainer;
                    data.targetForm = data.targetContainer.find('form').first();
                }
                data.loadingTarget.hide();
                data.targetContainer.show();
            }
        }
        if (undefined != data.callback && data.callback.length) {
            var theCallback = data.callback.split('-');
            delete data.callback;
            if (undefined != theCallback[1] && theCallback[1].length) {
                //var theFunc = theCallback[1];
                window[theCallback[1]](data);
            }
        }
        if (undefined != data._token && data._token.length) {

            var token = data._token.split('-');
            if (undefined != token[1] && token[1].length) {
                $("meta[name='_token']").attr('content', token[1]);

            }
        }
        if (undefined == data.status || 'false' == data.status) {
            data.thisForm.find('.help-block').remove();
            data.thisForm.find('.has-error').removeClass('has-error');
            if(undefined != data.result && undefined != data.result.errors)
                for (var i in data.result.errors) {
                    if (data.result.errors.hasOwnProperty(i)) {
                        var errorElem = $(data.thisForm).find($('input[name='+i+']'));
                        if(!errorElem.length)
                            errorElem = $(data.thisForm).find($('textarea[name='+i+']'));
                        if(!errorElem.length)
                            errorElem = $(data.thisForm).find($('select[name='+i+']')).parent();
                        
                        errorElem = errorElem.parent();
                        if (errorElem.hasClass('has-error')) {
                            errorElem.find('.help-block').remove();
                        };
                        errorElem.addClass('has-error');
                        errorElem.append('<p class="help-block">'+data.result.errors[i]+'</p>')
                    }
                }
        }
    }
    return false;
}

function default_error_callback(data)
{
    if (typeof data.status != "OK") {
        nofification(data.message, 'success')
    } else {
        nofification(data.message, "danger")
    }
    // actions(data.result);
}
function actions(data)
{
    elements = '';
    replacements = '';
    if(data.actions != undefined && data.actions)
        elements = data.actions
    else if (data && data.data && data.data.actions)
        elements = data.data.actions

    if(data.replacements != undefined && data.replacements)
        replacements = data.replacements
    else if (data && data.data && data.data.replacements)
        replacements = data.data.replacements
    // replacements = data.data.replacements;
    if (elements.length > 0) {
        data.actions;
        for(var i = 0; i < elements.length; i++) {
            action = elements[i][1];
            element = elements[i][0];
            value = null;
            if(elements[i][2] != undefined) {
                value = elements[i][2];
            }
            if($(element).hasClass('modal')){
                if(action == 'hide')
                    modal.hide($(element));
                else if(action == 'show')
                    modal.show($(element));
                // else if(action == 'delete')
                //     modal.remove($(element));
            }
            else if($(element).hasClass('dropdown')) {
                if(action == 'hide')
                    dropdown.close($(element))
            }
            else {
                if(action == 'hide') {
                    $(element).hide();
                } else if(action == 'delete') {
                    $(element).remove();
                } else if(action == 'show') {
                    $(element).show();
                } else if(value !== null) {
                    $(element)[action](value);
                } else {
                    $(element).trigger(action);
                }
            }
        }
    }
    
    if(replacements !== "undefined") {
        for(var element in replacements) {
            if($(element).length > 0) {
                if($(element).is('input')) {
                    $(element).val(replacements[element]);
                } else {
                    $(element).html(replacements[element]);
                }
            }
        }
    }
}