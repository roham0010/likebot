function lg(a, n = null)
{
	if(n) {
		console.log(a, n);
	} else {
		console.log(a)
	}
}
var p = {

	api: function(v, a, d) {
		var base = "http://likebot.local/";
		var baseApi = base + "api/v1/";
		return p = {
			urls: {
				sync: function(a) {
					// lg(baseApi + a + "/sync")
					return baseApi + "sync";
				},
				token: {
					create: function(e) {
						return baseApi + "token/" + e + "/create"
					}
				}
			},
			calls: {
				token: {
					create: function(e, r) {
						return fetch(p.urls.token.create(r), p.apiHeaders().guest);
					}
				}
			}

		}

	}, p_urlsp: function(v, a, d) {
		var base = "http://likebot.ir/";
		var baseApi = base + "api/v1/";
		return {
			sync: function(a) {
				// lg(baseApi + a + "/sync")
				return baseApi + "sync";
			},
			token: {
				create: baseApi + "token/create"
			}
		}

	}, apiHeaders: function(v, a, d) {
		d.append('local', 'fa');
		return {
			guest: {
				method: "POST",
				headers: {
					Accept: "application/json",
					"X-Requested-With": "XMLHttpRequest"
				},
				credentials: "include",
				body: d
			},
			user: {
				method: "POST",
	            headers: {
					Accept: "application/json",
		    		'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + a,
					"X-Requested-With": "XMLHttpRequest"
				},
				credentials: "include",
				body: d
			}
		}
	}, p_storagep: function(v, a, d) {
		return chrome.storage.local;

	}, p_getAccessTokenp: function(v, a, d) {
		p.post(p.p_urlsp().token.create, p.apiHeaders('', '', v).guest).then(p.toJson).then(function(jsonAccessToken) {
			lg(jsonAccessToken);
			serverAccessToken = jsonAccessToken.access_token;
			p.p_storagep().set({ storageAccessToken: serverAccessToken });
		});

	}, p_checkStatusp: function(v, a, d) {
		p.p_storagep().get(['activity', 'storageAccessToken'], function(e){
			var activity = e.activity,
				token = e.storageAccessToken;
			if (token && token.length > 0) {
				lg('show logged_in_area')
				$('#logged_in_area').show();
				$('#login_area').hide();
			}
			var status = activity != undefined && activity.status != undefined ? activity.status : 'unknown';
			$('#background_status').html(p.p_transp(v, 'background.status.' + status) + status);
			setTimeout(function() {
				p.p_checkStatusp(v);
			}, p.p_timep(0.1))
		})

	}, p_startp: function(v, a, d) {
		v = {
			serverStorage:{},
			local: 'fa'
		};
		p.p_checkStatusp(v);

	}, post: function(v, a, d) {
		var result = fetch(v, a)
			return result;
	
	}, p_getTimep: function(v, a, d) {
		var time = v;
	    fullYear = time.getFullYear();
	    Mount = time.getMonth() + 1;
	    Day = time.getDate();
	    Hour = time.getHours();
	    Minute = time.getMinutes();
	    Second = time.getSeconds();
	    
	    var currentDate = fullYear + '-' + Mount + '-' + Day,
    		currentTime = Hour + ':' + Minute + ':' + Second;
    	
    	return currentDate + " " + currentTime;

	}, toJson: function(v) {
		if (!v.ok) throw new Error(v.status);
		return v.json();
	}, p_timep: function(v, a, d) {
		return ((v * 1000) * 60)

	}, p_getp: function(v, a, d) {
		var ks = a.split('.');
		var r = v;
		for(ki in ks) {
			k = ks[ki];
			if(r[k] != undefined) {
				r = r[k];
			} else {
				return null;
			}
		}
		return r;

	}, p_transp: function(v, a, d) {
		return p.p_getp(p.p_langsp(), a);

	}, p_langsp: function(v, a, d) {
		var fa = p.p_langfap(),
			en = p.p_langenp()
		var langs = {
			fa: fa,
			en: en
		}
		return langs[(v && v.local) || 'fa'];

	}, p_langfap: function(v, a, d) {
		return {
			background: {
				status: {
					error: "خطایی رخ داده لطفا مرورگر را بسته و دوباره باز کنید",
					active: "در حال فعالیت",
					pause: "در حال مکث",
					access_token_problem: "لطفا نام کاربری رمز عبور سایت را دوباره وارد کنید",
					like_pause: "در حال تعمل برای لایک بعدی", // pause 5-10 min after 100 likes
					instagram_challenge: "لطفا صفحه اینستاگرام را برای تایید شماره همراه بررسی کنید",
					like_blocked: "عدم دسترسی به لایک پست ها",
					not_enauth_likes: "متاسفانه لایک کافی ندارید",
					instagram_logged_out: "لطفا از طریق مرورگر وارد اکانت <a href='http://instagram.com'>اینستاگرام</a> شوید",
					unknown: "خطای نامشخص! لطفا مرورگر را بسته و دوباره باز کنید"
				}
			},
			popup:{

				//other languages
			}
		}
	}, p_langenp: function(v, a, d) {
	},
}
p.p_startp();
jQuery(document).ready(function($) {
	$("#get_access_token_button").on('click', function(){
		$this = $(this);
		var $username = $("#username").val();
		var $password = $("#password").val();
		formData = new FormData();
		formData.append('username', $username);
		formData.append('password', $password);
		p.p_getAccessTokenp(formData);
	});

	// $("#get_access_token_button").click();
})