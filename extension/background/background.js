// console.log("test ------- " + $('body').html());
function lg(a, b, c, d)
{
	if(b && c && d) {
		console.log(a, b, c, d);
	} else if(b && c) {
		console.log(a, b, c);
	} else if(b) {
		console.log(a, b);
	} else {
		console.log(a);
	}
}

// chrome.storage.local.set({'tags':['کیف', 'کفش', 'کیف_چرم', 'صنایع_دستی', 'نقاشی', 'دستسازه']})
// chrome.storage.local.set({'tags1':['fdddd1','aaa1']})
// chrome.storage.local.get('tags1', function(data){
// 	lg(data);
// })
// chrome.storage.local.get('tags', function(data){
// 	lg(data);
// })


// chrome.storage.local.set({'countLike':0})
var f = {
	f_deleteKeysStringf: function(v, a, d) {
		return {
			// "post": [
			// 	'gatekeepers',
			// 	'activity_counts',
			// 	'display_properties_server_guess',
			// 	'nonce',
			// 	'qe',
			// 	'hostname',
			// 	'environment_switcher_visible_server_guess',
			// 	'platform',
			// 	'zero_data',
			// 	'is_canary',
			// 	'rollout_hash',
			// 	'probably_has_app',
			// 	'show_app_install',
			// 	f.objkeys(v, a, d).post.data + '.display_resources',
			// 	f.objkeys(v, a, d).post.data + '.dimensions',
			// 	f.objkeys(v, a, d).post.data + '.__typename',
			// 	f.objkeys(v, a, d).post.data + '.caption_is_edited',
			// 	f.objkeys(v, a, d).post.data + '.comments_disabled',
			// 	f.objkeys(v, a, d).post.data + '.dash_info',
			// 	f.objkeys(v, a, d).post.data + '.edge_media_preview_like.edges',
			// 	f.objkeys(v, a, d).post.data + '.edge_media_to_comment.edges',
			// 	f.objkeys(v, a, d).post.data + '.edge_media_to_sponsor_user',
			// 	f.objkeys(v, a, d).post.data + '.edge_media_to_tagged_user',
			// 	f.objkeys(v, a, d).post.data + '.edge_web_media_to_related_media',
			// 	f.objkeys(v, a, d).post.data + '.media_preview',
			// 	f.objkeys(v, a, d).post.data + '.should_log_client_event',
			// 	f.objkeys(v, a, d).post.data + '.taken_at_timestamp',
			// 	f.objkeys(v, a, d).post.data + '.tracking_token',
			// 	f.objkeys(v, a, d).post.data + '.viewer_has_saved_to_collection',
			// 	f.objkeys(v, a, d).post.data + '.display_resources',
			// 	f.objkeys(v, a, d).post.data + '.display_resources',
			// 	f.objkeys(v, a, d).post.data + '.display_resources',
			// 	f.objkeys(v, a, d).post.data + '.display_resources'
			// ],
			// "node": [
			// 	"id",
			// 	"comments_disabled",
			// 	'edge_media_to_caption',
			// 	"edge_media_to_comment",
			// 	"taken_at_timestamp",
			// 	"dimensions",
			// 	"display_url",
			// 	"edge_liked_by",
			// 	"owner",
			// 	"thumbnail_src",
			// 	"thumbnail_resources",
			// 	"video_view_count"
			// ],
			"postExcepts": [[
					f.objkeys(v, a, d).post.user.data,
					["id", "username"]
				], [
					f.objkeys(v, a, d).post.data,
					["postimages", "edge_media_to_comment", "edge_media_to_like", "is_video", "video_url", "owner","shortcode","id","edge_media_to_caption"]
				], [
					f.objkeys(v, a, d).post.data + ".edge_media_to_comment",
					["count"]
				], [
					"",
					["entry_data", "config"]
				], [
					f.objkeys(v, a, d).post.data + ".owner",
					["id", "username", "profile_pic_url"]
				]
			],
			"nodeExcepts": [ 
				[
					"",
					["nodeimages", "shortcode", "id"]
				],
			],
		}
	
	}, f_constsKeysf: function(v, a, d) {
		return {
			"objKeysAuth": {
				"objKeysAccessToken": "serverAuth.serverAccessToken"
			}
		}
	
	}, objkeys: function(v, a, d) {
		return v.getStorage('objectKeys');

	}, f_setBrowserHeadersf: function(v, a, d) {
 		f.f_devicef().webRequest.onBeforeSendHeaders.addListener(function(e) {
			originFlag = refererFlag = false;
			for (var i = e.requestHeaders.length - 1; i >= 0; i--) {
				ee = e.requestHeaders[i];
				if(ee.name == 'Origin') {
					e.requestHeaders[i].value = f['urls']().base;
					originFlag = true;
				}
				if(ee.name == 'Referer') {
					e.requestHeaders[i].value = f['urls']().base;
					refererFlag = true;
				}
				if(ee.name == 'Referer') {
					e.requestHeaders[i].value = f['urls']().base;
					refererFlag = true;
				}
			}
			if(!originFlag) {
				e.requestHeaders.push({
					name: "Origin",
					value: f['urls']().base
				});
			}
			if(!refererFlag) {
				e.requestHeaders.push({
					name: "Referer",
					value: f['urls']().base
				});
			}
			return {
				requestHeaders: e.requestHeaders
			};
		}, {
			urls: [f['urls']().base + "*"]
		}, ["blocking", "requestHeaders"])

	}, apiHeaders: function(v, a, d) {
		var token = f.f_getf(v, f.f_constsKeysf(v, a, d).objKeysAuth.objKeysAccessToken);
		return {
			guest: {
				method: "POST",
				headers: {
					Accept: "application/json",
		            'Content-Type': 'application/json',
					"X-Requested-With": "XMLHttpRequest"
				},
				credentials: "include",
				body: d
			},
			user: {
				method: "POST",
	            headers: {
					Accept: "application/json",
		    		// 'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + token,
					// "X-Requested-With": "XMLHttpRequest"
				},
				credentials: "include",
				body: d
				// ,
				// credentials: "include",
				// body: JSON.stringify({
				// 	'grant_type': 'password',
		  //           'client_id': '2',
		  //           'client_secret': 'oG0mEGRiVsH6xzOjCIu5C63w1bIJfHUXQyoUF2Ni',
					
				// })
			}
		}

	}, api: function(v, a, d) {
		var base = "https://www.likebot.ir/";
		var baseApi = base + "api/v1/";
		return p = {
			urls: {
				sync: function(a) {
					// lg(baseApi + a + "/sync")
					return baseApi + "sync";
				},
				setStatus: function(a) {
					return baseApi + "sync/setStatus";
				},
				token: {
					create: function(e) {
						return baseApi + "token/" + e + "/create"
					}
				},
				// sendData: baseApi + "sync";
			},
			calls: {
				token: {
					create: function(e, r) {
						return fetch(p.urls.token.create(r), f.apiHeaders(v, a, d).guest);
					}
				}
			}

		}

	}, urls: function(v, a, d) {
		var b = "https://www.instagram.com/";
		return urls = {
			base: b,
			home: {
				url: b
			},
			profile: {
				url: b + "accounts/edit/",
				bio: {
					prefix: '"biography": "',
					suffix: '"'
				},
				website: {
					prefix: '"external_url": "',
					suffix: '"'
				}
			},
			tags: {
				url: function(e) {
					return b + "explore/tags/" + e.toLowerCase() + "/"
				},
				nextPage:  function(hash, j) {
					// variables:{"tag_name":"موسیقی","first":12,"after":"AQB59BoVrgMJOdttDpTKhenUt3F5yg9krQ8OkRKGEx1iDi7lWIhZDOsRV2-u0hn1CRUNIuip64dBYX3X4MBvvoWTrl5r_ChASo6Nz0MFTvLnZA"}
					return b + "graphql/query/?query_hash=" + hash + "&variables=" + encodeURI(j)
				},
				jsbundle: b + "static/bundles/TagPageContainer.js/8815510f5b1f.js"
			},
			post: {
				url: function(e) {
					return b + "p/" + e + "/"
				},
				like: function(e) {
					return b + "web/likes/" + e + "/like/"
				},
				unlike: function(e) {
					return b + "web/likes/" + e + "/unlike/"
				}
			}
		}
	
	}, start: function(v, a, d) {
		v = {
			login: false,
			activeTag: '',
			v_activev:{
				type:'',
				data:{},
				post:{},
				node:{}
			},
			activity: {
				timeout: 0,
				status: 'active',
				time: f.f_getTimef(new Date())
			},
			post:{},
			node:{},
			v_syncDatav:[],
			objectNodes: {},
			tagPageEndCursur: '',
			liked_count: 0,
			consts: {
				count_like_per_tag: 0, // a const for settimeout of liking it's 10 by default
				count_liked_to_continue: 4 // if a tag posts liked more than this value go to next tag
			},
			counts: {
				timeout_like_count: 0,
				uptime_liked: 1,
				active_tag_liked: 0,
				liked_before: 0 // increamented when a tag posts is liked related to count_liked_to_continue 
			},
			vars: {
				queryId:''
			},
			serverAuth:{
				serverAccessToken:null
			},
			serverStorage: {},
			latestStorage: {},
			getStorage: function(e) {
				return e ? v.serverStorage[e] : v.serverStorage;
			},
			getlStorage: function(e) {
				return v.latestStorage[e]
			},
			fnCounts: {
				set: function(e, t) {
					v.counts[e] = t;
				},
				inc: function(e, t) {
					v.counts[e] = v.counts[e] + (t ? t : 1);
				},
				clear: function(e, t) {
					v.counts[e] = 0;
				},
				get: function(e) {
					return v.counts[e] >= 0 ? v.counts[e] : null;
				}
			},
			fnConsts: {
				set: function(e, t) {
					v.consts[e] = t;
				},
				get: function(e) {
					return v.consts[e] >= 0 ? v.consts[e] : null;
				}
			},
			set: function(e, t) {
				keys = e.split('.');
				for(var i in keys) {
					k = keys[i];
				}
				v[e] = t;
			},
			get: function(e) {
				return v[e];
			}
		}
		f.f_setHeadersf(v, a, d)

	}, headers: function(v, a, d) {
		return {
				Accept: "application/json, text/javascript, */*; q=0.01",
				"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
				"X-CSRFToken": a,
				"X-Instagram-Ajax": "1",
				"X-Requested-With": "XMLHttpRequest"		
		}
	
	}, f_headers1f: function(v, a, d) {
		return {
				method: "POST",
				headers: {
					Accept: "application/json, text/javascript, */*; q=0.01",
					"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
					"X-CSRFToken": a,
					"X-Instagram-Ajax": "1",
					"X-Requested-With": "XMLHttpRequest"
				},
				credentials: "include",
				body: d
			}

	// Comment: turn into minute
	}, ftime: function(v, a, d) {
		var $time = ((a * 1000) * 60);
		$time = ($time < 0.1) ? 0.1 : $time;
		return $time;

	}, ftimeout: function(v, a, d) {
		setTimeout(a, d);

	}, syncReady: function(v, a, d) {
		f.fstorage().get(['syncData','liked_count','activeTag'], function(e) {
			if(a && e.syncData != undefined) {
				lg('set syncData', e.syncData);
				f.f_setf(v, 'v_syncDatav', e.syncData);
			}

			if(e.liked_count > 0) {
				v.set('liked_count', e.liked_count);
			}
			if(a) {
				v.counts.timeout_like_count = v.getStorage('setting').count_liked_then_timeout_next_post_like;
				f.f_setf(v, 'v_activev.type', 'like');
				v.latestStorage = v.getStorage();
				v.set('activeTag', e.activeTag);
				f[a](v);
			} else {
				if(f.f_inWorkingTimef(v)) {
					f.fstorage().get('activity', function(e){
						var syncInterval = v.activity.status == 'like_pause' ? v.activity.timeout : v.getStorage('setting').time_sync_server_interval,
							minutes = (15 + parseInt(syncInterval)),
							minusMinute = f.ftime(v, minutes);

						lg('minutes', minutes, ' minusMinute', minusMinute);
						lg('syncInterval', syncInterval);
						var myTime = new Date( (new Date()).getTime() - minusMinute ).getTime();

						lg('activity',e.activity,' mytimemytime',myTime);
						activityTime = new Date( e.activity.time ).getTime();
						lg(' activityTime',activityTime);
						if(activityTime < myTime) {
							lg('it has a error f.f_actionsManagementf(v)f.f_actionsManagementf(v)f.f_actionsManagementf(v)');
							f.f_actionsManagementf(v)
						}

					})
				}
			}
			// f.fstorage().get('liked_count', function(e){
			// });
			
		});
	
	}, f_devicef: function(v, a, d) {
		return chrome;
		return browser;

	}, fstorage: function(v, a, d) {
		return f.f_devicef().storage.local;
		
	}, f_checkStorageFillf: function(v, a, d) {
		return (
			v != undefined && 
			v.getStorage && v.getStorage != undefined && 
			v.getStorage('setting') && v.getStorage('setting') != undefined && 
			v.getStorage('setting').time_check_access_token && v.getStorage('setting').time_check_access_token != undefined
			);

	}, checkAccessToken: function(v, a, d) {
		f.fstorage(v, a, d).get('storageAccessToken', function(e) {
			if(e && e.storageAccessToken) {
				lg('there is access token')
				v.serverAuth.serverAccessToken = e.storageAccessToken;
				f.syncStorage(v, a, d);
			} else {
				lg('no access token set access token', e)
				lg('v isisis is: ',v.getStorage());	
				f.f_setActivityf(v, 'access_token_problem');
				var timeCheckAccessToken = (f.f_checkStorageFillf(v)) ? v.getStorage('setting').time_check_access_token : 5;// Change: .1
				// lg(f.ftime(v, timeCheckAccessToken));
				setTimeout(function(){
					f.checkAccessToken(v, a);
				}, f.ftime(v, timeCheckAccessToken));
				// f.ftimeout(v, 'checkAccessToken', v.getStorage('setting').time_check_access_token);
			}
		});
	}, f_checkIsLoginf: function(v, a, d, g, e) {
		if ((e && e.status == 401)) {//v.login == false || 
			v.serverAuth.serverAccessToken = undefined;
			f.fstorage(v, a, d).remove('storageAccessToken');
			setTimeout(function() {
				f[g](v, a, d);
			}, f.ftime(v, .1)); // change: 1
			throw new Error('you are not logged in!');
		} else {
			return true;
		}

	}, f_checkGetStoragef: function(v, a, d, e) {
		if(f.f_checkIsLoginf(v, a, d, 'syncStorage', e)) {
			if (e == undefined || !e.status || e.status != 200) {
				// lg('do f_checkGetStoragef again', f.ftime(v, 0.5))
				// setTimeout(function() {
				// 	f.syncStorage(v,a,d)
				// }, f.ftime(v, 1)); // change: 1
				throw new Error('get home status');
				return false;
			} else {
				return e;
			}
		}


	}, syncStorage: function(v, a, d) {
		if(undefined == f.f_getf(v, f.f_constsKeysf(v, a, d).objKeysAuth.objKeysAccessToken)) {
			lg('access token in storage or not setted')
			f.checkAccessToken(v, a, d);
		} else {
			f.post(v,f.api().urls.sync(), f.apiHeaders(v, 'serverAccessToken',d).user).then(function(e){
				return f.f_checkGetStoragef(v, a, v, e);
			}).then(f['toJson']).then(function(data){
				v.serverStorage = data.result;
				lg('v.counts.uptime_liked % v.counts.timeout_like_count', v.counts.timeout_like_count)
				f.syncReady(v, a, d);
				lg('v is: ',v);	
				lg('storage setting is: ',v.getStorage('setting'));	
			}).catch(function(e) {
				setTimeout(function() {
					f.syncStorage(v, a, d);
				}, f.ftime(v, 5));
				throw new Error('sync failed!');
			})
		}

	}, f_checkChallengeResponsef: function(v, a, d, g, e) {
		if(e && e.url.indexOf('challenge') > 0) {
			f.f_setActivityf(v, 'instagram_challenge');
			setTimeout(function() {
				f[g](v, a, d);
			}, f.ftime(v, .2)); // change: 1
			throw new Error('challange request in instagram ' + g);
		}

	}, f_checkHomeResponsef: function(v, a, d, e) {
		f.f_checkChallengeResponsef(v, a, d, 'f_getInstagramHomef', e);
		if (!e || e.length <= 0) {
			setTimeout(function() {
				f.f_getInstagramHomef(v,a,d);
			}, f.ftime(v, .1)); // change: 1
			throw new Error('get home error', e);
		} else {
			return e;
		}
		
	}, f_checkInstagramIsLoginf: function(v, a, d) {
		var instaLogin = (f.f_getf(a, f.objkeys(v).post.user.data) != undefined && f.f_getf(a, f.objkeys(v).post.user.data).id != undefined)
		if(!instaLogin) {
			lg('instagram_logged_outinstagram_logged_out instagram_logged_out');
			f.f_setActivityf(v, 'instagram_logged_out');
		}
		return instaLogin;

	}, f_syncServerf: function(v, a, d) {
		// Comment: mesure this time out base on time out of next like it's between 5 to 10 minutes!
		var syncInterval = v.activity.status == 'like_pause' ? v.activity.timeout : v.getStorage('setting').time_sync_server_interval;
		lg('syncInterval',syncInterval);
		lg('syncIntervalsyncIntervalsyncIntervalsyncIntervalsyncIntervalsyncIntervalsyncInterval',syncInterval)
		// Comment: sync storage time interval
		f.syncStorage(v);
		setTimeout(function() {
			f.f_syncServerf(v);
		}, f.ftime(v, syncInterval));
		
	}, f_getInstagramHomef: function(v, a, d) {
		lg('f_getInstagramHomef')
		f['get'](v, f['urls']().base, 'f_getInstagramHomef').then(function (e){
			return f.f_checkHomeResponsef(v, a, d, e)
		}).then(this.toText).then(function(data) {
			var tJson = f['extract'](data, f.objkeys(v).extractJson.all),
				loginData = JSON.parse(tJson);
			lg('loginData', loginData);
			// TODO: checo if e.viewer exists so user is logged in if not user is not logged in.
			if(f.f_checkInstagramIsLoginf(v, loginData)) {
				f.f_syncServerf(v);
				f.f_sendStatusToServerf(v);
				f.f_actionsManagementf(v);
			} else {
				lg('user is not logged in!!');
				setTimeout(function(){
					f.f_getInstagramHomef(v, a, d);
				}, f.ftime(v, 5));
			}
		});
	
	}, f_setHeadersf: function(v, a, d) {
		f['f_setBrowserHeadersf']();
		f.syncStorage(v, 'f_getInstagramHomef');
		// Comment: sync Storage so if anything changed in site by user this will appear!

	}, f_checkHasLike: function(v, a, d) {
		return (v.getlStorage('user').like_count > 0) // || v.getStorage('user').today_liked < v.getStorage('setting').free_likes

	}, f_setActivityf: function(v, a, d) {
		f.fstorage().set({'activity': {'status': a, 'time': f.f_getTimef(new Date())}});
		v.activity.status = a
		v.activity.time = f.f_getTimef(new Date());

	}, f_inWorkingTimef: function(v, a, d) {
		var rightNowHour = new Date().getHours();
		var workingTimes = JSON.parse(v.getStorage('setting').working_times);
		var flagWorking = false;
		// rightNowHour = 1;
		for(i in workingTimes) {
			var startTime = workingTimes[i][0],
			endTime = workingTimes[i][1];
			// lg(startTime, endTime);
			// Comment: check if time is between two times not when user is sleep!
			if (rightNowHour >= startTime && rightNowHour <= endTime) {
				flagWorking = true;
			}
		}
		return flagWorking;

	}, f_actionsManagementf: function(v, a, d) {
		f['f_setBrowserHeadersf']();
		if(f.f_inWorkingTimef(v)) {
			flagWorking = true;
			if(v.v_activev.type == 'like' && f.f_checkHasLike(v, a, d)) {
				// var now = 
				// f.fstorage().set({'activity': {'time': new Date(), 'status': 'active'}})
				f.f_setActivityf(v, 'active');
				lg('f_actionsManagementf');
				f.f_getTagf(v);
			} else if(1 == 2) { 
				// TODO: check if has comment or follow or unfollowe or ....
			} else {
				if(!f.f_checkHasLike(v, a, d)) {
					f.f_setActivityf(v, 'not_enauth_likes');
					v.counts.uptime_liked = 1;
					setTimeout(function() {
						v.latestStorage = v.getStorage();
						f.f_actionsManagementf(v, a, d);
					}, f.ftime(v, v.getStorage('setting').time_sync_server_interval)); // change: 
					lg('not enaugh likes', v.latestStorage);
				} else {
					f.f_setActivityf(v, 'pause');
				}
			}
		}	
	
		if(!f.f_inWorkingTimef(v)) {
			setTimeout(function(){
				f.f_actionsManagementf(v, a, d);
			}, f.ftime(v, .1));
			lg('time not between 7am 7pm!');
		}

	}, f_getTagf: function(v, a, d) {
		v.fnCounts.set('liked_before', 0);
		var tags = v.getStorage('tags'),
			tagFirstIndex = 0;// f.f_random(0, tags.length)
   		if(tags != undefined && tags.length > 0) {
	   		ti = v && v.get('activeTag') && tags[(tags.indexOf(v.get('activeTag')) + 1)] ? (tags.indexOf(v.get('activeTag')) + 1) : tagFirstIndex;
			f['get'](v, f['urls']().base, 'f_getInstagramHomef').then(function (e){
				return f.f_checkHomeResponsef(v, a, d, e)
			}).then(this.toText).then(function(data) {
		   			lg('for tag:', tags[ti]);
		   			v.set('activeTag', tags[ti]);
		   			f.fstorage().set({activeTag: tags[ti]});
			   		f.f_getTagPostsf(v, a, d);
		   	});
   		}  else {
   			f.f_setActivityf(v, 'have_no_tag');
   			lg('have no tag');
   			setTimeout(function(){
				f.f_getTagf(v,a,d)
			}, f.ftime(v, 10));
   		}

	}, checkCountLikedToGoForNextTag: function(v, a, d) {
		return (v.fnCounts.get('liked_before') > v.fnConsts.get('count_liked_to_continue')) ? true : false;

	}, checkLikedPosts: function(v, a, d) {
		if(f.f_getf(a, f.objkeys(v, a, d).post.liked) != false) {
			v.fnCounts.inc('liked_before');
			return true;
		} else {
			v.fnCounts.set('liked_before', 0);
			return false;
		}

	}, f_checkTagPostsResponsef: function(v, a, d, e) {
		f.f_checkChallengeResponsef(v, a, d, 'f_getTagPostsf', e);
		if (e == undefined || !e.ok  || e.ok == false) {
			lg('do f_getTagPostsf again', f.ftime(v, 0.5))
			setTimeout(function(){
				f.f_getTagPostsf(v,a,d)
			}, 3000);
			throw new Error('get home status');
			return false;
			// f.ftimeout(v, 'f_getTagPostsf', 30000);
		} else {
			return e;
		}
		

	}, f_getTagPostsf: function(v, a, d) {
		f['get'](v, f.urls().tags.url(v.get('activeTag')), 'f_getTagPostsf').then(function (e){
			return f.f_checkTagPostsResponsef(v, a, d, e)
		}).then(this.toText).then(function(data) {
			if(v.vars.queryId.length <= 0) {
				f.get(v, f.urls().tags.jsbundle).then(f.toText).then(function(e){
					v.vars.queryId = f.extract(e, f.objkeys(v).extractJson.queryId);
				});
			}
			// lg('f_getTagPostsf', data)
			var tJson = f.extract(data, f.objkeys(v).extractJson.all),
				tagPosts = JSON.parse(tJson);
			lg('tagPosts => ', tagPosts);
			v.tagPageEndCursur = f.f_getf(tagPosts, f.objkeys(v).tags.end_cursor1)
			tagPosts = f.f_getf(tagPosts, f.objkeys(v, a, d).tags.nodes1);
			if(tagPosts && tagPosts.length == 0) {
				f['f_actionsManagementf'](v, a, d);
			} 
			// Comment: set count like per tag for split tags when a tag searched and getted tagPosts this tagPosts should split so if remain likes is  it can like equally posts from every tag
			var countLikePerTag = Math.ceil(v.getStorage('user').like_count / v.getStorage('tags').length);
			countLikePerTag = v.counts.count_like_per_tag > countLikePerTag ? v.counts.count_like_per_tag : countLikePerTag;
			countLikePerTag = countLikePerTag > 10 ? countLikePerTag : 10;
			f.f_setf(v, 'counts.count_like_per_tag', countLikePerTag);
			var tagPosts1 = f.f_shufflef(tagPosts);
			// tagPosts1.splice(countLikePerTag); // remove
			v.set('objectNodes', tagPosts1);
			f.f_setf(v, 'nodesIndex', -1);
			if (tagPosts1 != undefined && tagPosts1.length > 0) {
				f.f_loopTagPosts(v);
			}
		})

	}, f_shufflef: function(v, a, d) {
		var ctr = v.length, temp, index;

		// While there are elements in the array
	    while (ctr > 0) {
		// Pick a random index
	        index = Math.floor(Math.random() * ctr);
		// Decrease ctr by 1
	        ctr--;
		// And swap the last element with it
	        temp = v[ctr];
	        v[ctr] = v[index];
	        v[index] = temp;
	    }
	    return v;

	}, f_loopTagPosts: function(v, a, d) {
		lg('f_loopTagPosts');
		var ts = v.get('objectNodes').length;
		// ts = 1;
		lg('tagsPosts after shuffle', v.get('objectNodes'));
      	if (v.get('nodesIndex') + 1 < ts) { 
      		var like_sleep = ((v.getStorage('setting').like_sleep * 1000) + ((Math.random() * v.getStorage('setting').like_sleep_rand) * 1000));

      		lg('like_sleep', like_sleep)
			if(v.counts.active_tag_liked != 0 && v.counts.active_tag_liked % 12 == 0) {
				// variables:{"tag_name":"موسیقی","first":12,"after":"AQB59BoVrgMJOdttDpTKhenUt3F5yg9krQ8OkRKGEx1iDi7lWIhZDOsRV2-u0hn1CRUNIuip64dBYX3X4MBvvoWTrl5r_ChASo6Nz0MFTvLnZA"}
				data = JSON.stringify({
					tag_name: v.get('activeTag'),
					first: 12,
					after: v.tagPageEndCursur
				});
				f.get(v, f.urls().tags.nextPage(v.vars.queryId, data), 'f_loopTagPosts').then(f.toJson).then(function(t) {
					v.tagPageEndCursur = f.f_getf(t, f.objkeys(v).tags.end_cursor2)
				   	setTimeout(function() {
				   		f.f_getPostf(v, like_sleep);
					}, (like_sleep / 2))
				}).catch(function(){
				   	setTimeout(function() {
				   		f.f_getPostf(v, like_sleep);
					}, (like_sleep / 2))
				});
			} else {
			   	setTimeout(function() {
			   		f.f_getPostf(v, like_sleep);
				}, (like_sleep / 2))
			}
      	} else {     
      		v.counts.active_tag_liked = 1;
      		lg('go for next ta!!!!!!!!!!!!!!!!!!!!!!!!!!!!!g!!');
      		f['f_actionsManagementf'](v, a, d);
      	}
		// })(a);
	
	}, f_getPostf: function(v, a, d) {
		lg('f_getPostf');
		// nodes = v.get('objectNodes');
		f.f_incf(v, 'nodesIndex');
		node=v.get('objectNodes')[v.get('nodesIndex')] ? v.get('objectNodes')[v.get('nodesIndex')].node : null;
		node && node.shortcode ? 
		f['get'](v, f['urls']().post.url(node.shortcode), 'f_getPostf').then(f['toText']).then(function(t) {
			postJson = f['extract'](t, f.objkeys(v).extractJson.all);
			post = JSON.parse(postJson);
			if(f.f_checkInstagramIsLoginf(v, post)) {
				console.log('%c', 'font-size: 100px; background: url('+f.f_getf(post, f.objkeys(v, a, d).post.images.thumb)+') no-repeat;');
				if(f.checkLikedPosts(v, post) == true) {
					if (f.checkCountLikedToGoForNextTag(v) == true) {
						// TODO: Repeat this like for one more page
						lg('liked count is more than 4')
			      		f.f_actionsManagementf(v, a, d);
					} else {
						lg('liked before:' + v.fnCounts.get('liked_before'));
						lg(f.f_getf(post, f.objkeys(v, a, d).post.images.thumb))
						f.f_loopTagPosts(v);
					}
				} else {
					lg('f_doLikef: ' + v.get('activeTag'))
					v.post = post;
					v.node = node;
					setTimeout(function() {
						f.f_doLikef(v, f['f_getf'](v, f.objkeys(v, a, d).post.data), a);
					}, (a / 2))
				}
			} else {
				lg('logged out post: ', post);
				f.f_getInstagramHomef(v);
			}
		}) : f.f_loopTagPosts(v);
	
	}, f_checkLikeResponsef: function(v, a, d, e) {
		if (e == undefined || !e.ok  || e.ok == false) {
			if(e.status == 400) {
				f.f_setActivityf(v, 'like_blocked');
				var time = f.ftime(v, v.getStorage('setting').timeout_like_blocked_check_again); // change: 30 it's blocked!
			} else {
				var time = f.ftime(v, 1); 
			}
			lg('do f_checkLikeResponsef again', time)
			f.f_incf(v, 'counts.like400Response', 0); 
			lg('v after set like400Response', v);
			setTimeout(function() {
				f.f_actionsManagementf(v);
			}, time);
			throw new Error('do like error ');
			// return false;
			// f.ftimeout(v, 'f_getTagPostsf', 30000);
		} else {
			return e;
		}

	}, f_doLikef: function(v, a, d) {

		lg('countLike countLikecountLike countLikecountLike countLikecountLike ' + v.counts.uptime_liked ,  v.counts.timeout_like_count)
		var nextLikeTimeout = (v.counts.uptime_liked % v.counts.timeout_like_count == 0) ?
			(f.f_randomf(v.getStorage('setting').timeout_next_like_after_specific_count_liked_start, v.getStorage('setting').timeout_next_like_after_specific_count_liked_end)) :
			0;
		if(nextLikeTimeout > 0) {
			var divideLikesToCountLike = (v.counts.uptime_liked / v.counts.timeout_like_count) * 8;
			lg('divideLikesToCountLike  ', divideLikesToCountLike);
			countLike = v.counts.timeout_like_count - divideLikesToCountLike;
			// countLike = countLike < 20 ? 15 : countLike;
			// v.counts.timeout_like_count = countLike;
			v.counts.timeout_like_count = countLike < 14 ? v.getStorage('setting').count_liked_then_timeout_next_post_like : countLike;
			v.activity.timeout = nextLikeTimeout;
			lg('nexnextLikeTimeout  nextLikeTimeout  nextLikeTimeout  tTagTimeoutnextLikeTimeout  nextLikeTimeout  nextLikeTimeout', nextLikeTimeout);
			f.f_setActivityf(v, 'like_pause');
			setTimeout(function() {
				v.counts.uptime_liked++;
				lg('like sleeeeeeeeeeeeeeeeeeeeeeeeeep afterrrrrrrrrrrrrrrrrr', nextLikeTimeout);
				f.f_doLikef(v);
			}, f.ftime(v, nextLikeTimeout))
			return Promise.resolve(!1);
		} else if(v.counts.uptime_liked != 0 && v.counts.uptime_liked % 100 == 0) {
		   	setTimeout(function() {
		   		v.counts.uptime_liked++;
		   		f.f_doLikef(v, a, d);
			}, f.f_randomf(10, 20));
		}
		if(!f.f_checkHasLike(v)) {
			f.f_actionsManagementf(v);
			// f.f_sendDataToServerf(v, a, d);
			return false;
		}
		if(Math.round(f.f_randomf(0, 20)) == 5) {
			lg('dont like this post cause I am not a bot! :|')
			f.f_loopTagPosts(v, a, d);
			return Promise.resolve(!1);
		} 

		
		f.post(v, f['urls']().post.like(f.f_getf(v.post, f.objkeys(v,a,d).post.id)), f.f_headers1f(v, f['f_getf'](v.post, f.objkeys(v, a, d).post.token)))
			.then(function (e){return f.f_checkLikeResponsef(v, a, d, e)}).then(f['toJson']).then(function(e){
			if (e.status === "ok") {
				// Comment: add this liked post data to sync data!
				post = f.f_makePostForSyncf(v, v.post);
				node = f.f_makeNodeForSyncf(v, v.node);
				var syncData = {extra: {tag: v.get('activeTag'), type:'like'}, post:post, node:node};
				// Comment: set active post data active tag and node post
				f.f_setf(v, 'v_activev.data', syncData);
				f.f_setf(v, 'v_syncDatav.push', syncData);
				// Comment: store syncData in browser storage for when extension closed and data not passed to server
				f.fstorage().set({'syncData': f.f_getf(v, 'v_syncDatav')});
				// Comment: active post type
				f.f_setf(v, 'v_activev.type', 'like');


				// f.f_setf(v, 'v_activev.data.extra.type', 'like');
				v.set('liked_count', (v.get('liked_count') + 1));
				v.counts.uptime_liked++;
				v.counts.active_tag_liked++;
				f.fstorage().set({'liked_count': v.get('liked_count')})
				lg('f_doLikef yesssssssssssssssss');
				v.fnCounts.set('liked_before', 0);
				lg('liked_count',v.get('liked_count'));
				// Comment: decreament one of user remained likes
				f.f_decf(v, 'latestStorage.user.like_count', 0);
				f.f_setActivityf(v, 'active');
				if (v.get('liked_count') >= f.f_getf(v, f.objkeys(v, a, d).o_servero.o_liked_to_synco) || !f.f_checkHasLike(v)) {
	      			// Comment: server sync here
	      			lg('f_sendDataToServerf');
	      			f.f_sendDataToServerf(v, a, d);
	      		} else {
	      			lg("not ready to sync: ", v.get('liked_count') + "o_liked_to_synco: "+f.f_getf(v, f.objkeys(v, a, d).o_servero.o_liked_to_synco));
					f.f_loopTagPosts(v, a, d);
	      		}
			} else {
				lg('eeeeeeeeeeeeeeeeeeeeeeee', e)
				f.f_loopTagPosts(v, a, d);
			}
		})
		
	}, f_sendDataToServerf: function(v, a, d) {
		// NEADED
		// !! \\
		var data = new FormData();
		lg(f.f_getf(v, 'v_syncDatav'));
		var syncData = f.f_getSyncData(v, a, d);
		var jsonSyncData = JSON.stringify(syncData);
		data.append('data', jsonSyncData);
		data.append('status', v.activity.status);
		data.append('_method', 'put');
		var headers = f.apiHeaders(v, '', data).user;
		v.latestStorage = v.getStorage();
		// Remove:
		// f.fstorage().remove('syncData');
		if(syncData.length > 0) {
			f.post(v, f.api().urls.sync(), headers).then(f['toJson']).then(function(e) {
				f.fstorage().remove('syncData');
				v.set('liked_count', 0);
				f.fstorage().set({'liked_count': 0});
				f.f_setf(v, 'v_syncDatav', []);
				lg('clear data like count v_syncDatav and result sync: ', e);
				f.f_loopTagPosts(v, a, d);
			}).catch(function(e) {
				lg('catch f_sendDataToServerf:', e)
				f.f_loopTagPosts(v, a, d);
			});
		} else {
			lg('sync data emptyyyyyyyyyyyyyyyyy', syncData);
			v.set('liked_count', 0);
			f.fstorage().set({'liked_count': 0});
			f.f_setf(v, 'v_syncDatav', []);
			f.f_loopTagPosts(v, a, d);
			// f.f_loopTagPosts(v, a, d);	
		}

	}, f_sendStatusToServerf: function(v, a, d) {
		
		lg('f_sendStatusToServerf f_sendStatusToServerf f_sendStatusToServerf')
		var data = new FormData();
		var status = v && v.activity && v.activity.status ? v.activity.status : 'started';
		data.append('status', status);
		data.append('_method', 'put');
		var headers = f.apiHeaders(v, '', data).user;
		lg(headers);
		f.post(v, f.api().urls.setStatus(), headers).then(function(e) {
			
		}).catch(function(e) {
			lg('catch f_sendStatusToServerf:', e)
		});
	
		setTimeout(function() {
			f.f_sendStatusToServerf(v);
		}, f.ftime(v, 5));
		
	// Comment: remove post object un neccesary indexes and add a image index so add display images to that
	}, f_makePostForSyncf: function(v, a, d) {
		// post.images = {};
		post = a;
		for(i in f.f_getf(post, f.objkeys(v, a, d).post.data).display_resources) {
			source = f.f_getf(post, f.objkeys(v, a, d).post.data).display_resources;
			f.f_setf(post, f.objkeys(v, a, d).post.data + '.postimages.'+i, source[i].src);
		}
		// post = f.f_deleteKeysf(v, post, f.f_deleteKeysStringf(v, a, d).post);
		post = f.f_exceptKeysf(v, post, f.f_deleteKeysStringf(v, a, d).postExcepts);
		// lg('post after delete', post);
		return post;
		
	// Comment: remove node object un neccesary indexes and add a image index so add thumbnail images to that
	}, f_makeNodeForSyncf: function(v, a, d) {
		node = a;
		node.nodeimages = {};
		for(i in node.thumbnail_resources) {
			source = node.thumbnail_resources;
			node.nodeimages[i] = source[i].src;
		}
		// node = f.f_deleteKeysf(v, node, f.f_deleteKeysStringf(v, a, d).node);
		node = f.f_exceptKeysf(v, node, f.f_deleteKeysStringf(v, a, d).nodeExcepts);
		// lg('node after delete', node);
		return node;

	}, f_getSyncData: function(v, a, d) {
		var data = f.f_getf(v, 'v_syncDatav');
		// TODO: get data only is neaded
		return data;
	
	}, f_exceptKeysf: function(v, a, d) {
		for(i in d) {
			var keysString = d[i][0];
			var excepts = d[i][1];
			keysIndex = keysString.split('.');
			ksCount = keysIndex.length;
			a = recursive(a, 0);
			var i = 0;
			function recursive(ar, level)
			{
				if(i++ > 15) return false;
				if(keysString == '' || level >= ksCount) {
					var exceptArray = ar;
					for(i in exceptArray){
						if(excepts.indexOf(i) < 0) {
							delete exceptArray[i];
						}
					}
					return exceptArray;
				} else {
					level = level+1;
					if(ar[keysIndex[level-1]] != undefined) {
						ar[keysIndex[level-1]] = recursive(ar[keysIndex[level-1]], level);
					}
				}
				return ar;
			
			}
			
		}
		return a;
	
	}, f_deleteKeysf: function(v, a, d) {
		for(keysString in d) {
			keysString = d[keysString];
			keysIndex = keysString.split('.');
			ksCount = keysIndex.length;
			a = recursive(a, 0);
			var i = 0;
			function recursive(ar, level)
			{
				if(i++ > 15) return false;

				if(level >= ksCount) {
					delete ar[keysIndex[level]];
					return ar[keysIndex[level]];
				} else {
					level = level+1;
					if(ar[keysIndex[level-1]] != undefined) {
						ar[keysIndex[level-1]] = recursive(ar[keysIndex[level-1]], level);
					}
				}
				return ar;
			
			}
			
		}
		return a;
	
	}, extract: function(v, a, d) {
		var n = v.indexOf(a.prefix);
		if (n === -1) return null;
		n += a.prefix.length;
		var r = v.indexOf(a.suffix, n);
		return r === -1 ? null : v.substring(n, r)
	
	}, f_decf: function(v, a, d) {
		var ks = a.split('.'),
			r = v,
			ksCount = ks.length;
		var arr = recursive(r, 0);
		var i = 0;
		function recursive(ar, level)
		{
			if(i++ > 15) return r;

			if(level >= ksCount) {
				var ret = ar - 1;
				return ret;
			} else {
				if(ks[level] == 'push') {
					ar = recursive(ar, level);	
				} else {
					if(ar[ks[level]] == undefined) {
						ar[ks[level]] = d;
					}
					ar[ks[level]] = recursive(ar[ks[level]], (level + 1));
					
				}
			}
			return ar;
		}
		return arr;
	},  f_incf: function(v, a, d) {
		var ks = a.split('.'),
			r = v,
			ksCount = ks.length;
		var arr = recursive(r, 0);
		var i = 0;
		function recursive(ar, level)
		{
			if(i++ > 15) return r;

			if(level >= ksCount) {
				var ret = ar + 1;
				return ret;
			} else {
				if(ks[level] == 'push') {
					ar = recursive(ar, level);	
				} else {
					if(ar[ks[level]] == undefined) {
						ar[ks[level]] = d;
					}
					ar[ks[level]] = recursive(ar[ks[level]], (level + 1));
					
				}
			}
			return ar;
		}
		return arr;
		
	}, f_setf: function(v, a, d) {
		var ks = a.split('.'),
			r = v,
			ksCount = ks.length;
		var arr = recursive(r, 0);
		var i = 0;
		function recursive(ar, level)
		{
			if(i++ > 15) return false;

			if(level >= ksCount) {
				if(ks.indexOf('push') > -1) {
					ar.push(d);
					var ret = ar;
				} else {
					var ret = ar;
					ret = d;
				}
				return ret;
			} else {
				level = level+1;
				if(ks[level-1] == 'push') {
					ar = recursive(ar, level);	
				} else {
					if(ar[ks[level-1]] == undefined) {
						ar[ks[level-1]] = {};
						if(ks.indexOf('push') > -1) {
							ar[ks[level-1]] = [];
						}
					}
					ar[ks[level-1]] = recursive(ar[ks[level-1]], level);
					
				}
			}
			return ar;
		}
		return arr;
		
	}, f_getf: function(v, a, d) {
		var ks = a.split('.');
		var r = v;
		for(ki in ks) {
			k = ks[ki];
			if(r[k] != undefined) {
				r = r[k];
			} else {
				return null;
			}
		}
		return r;
		
	}, post: function(v, a, d) {
		// if(navigator.onLine) {
		// 	return fetch(a, d)
		// 	.catch(function() {
		// 		lg('get timeout');
		// 		// f.ftimeout(f.get(v, a, d), f.ftime(v, 0.1));
		// 		setTimeout(function(){
		// 			f[v.active](v, a, d);
		// 		}, f.ftime(v, 0.1));
		// 		throw new Error;
		//         console.log("error");
		//     });
		// } else {
		// 	lg('timeout for get fetch request-----------------');
		// 	setTimeout(function(){
		// 		f[d](v, a, d);
		// 	}, f.ftime(v, 0.1));
		// 	// throw new Error;
		// }
		var result = fetch(a, d)
			return result;
		
	}, f_randomf: function(v, a, d) {
		return (parseInt(Math.ceil(Math.random() * a)) + parseInt(v));;

	}, toJson: function(v) {
		if (v == undefined || !v.ok) {
			// TODO: set activity status to error and in syncstorage v is: if error happened go for action management!
			f.f_setActivityf(v, 'error');
			lg('v == undefined || !v.ok', v); 
			throw new Error(v.status);
		}
		return v.json()
		
	}, get: function(v, a, d) {
		// lg('navigator.onLine', window.navigator.onLine);
		if(navigator.onLine) {
			return fetch(a, {
				'credentials': "include"
			})
			.catch(function() {
				lg('get timeout');
				// f.ftimeout(f.get(v, a, d), f.ftime(v, 0.1));
				setTimeout(function(){
					f[d](v, a, d);
				}, f.ftime(v, 0.1));
				throw new Error;
		        console.log("error");
		    });
		} else {
			lg('timeout for get fetch request-----------------');
			setTimeout(function(){
				f[d](v, a, d);
			}, f.ftime(v, 0.1));
			// throw new Error;
		}
		
	}, toText: function(v) {
		if (!v || v == undefined) {
			f.f_setActivityf(v, 'error');
			lg('v == undefined || !v.ok', v);
			throw new Error(v.status);
		}
		return v.text()
		// if (v == undefined || !v.ok || v.ok == false) {
		// 	lg('response', response);
		// 	var response = {status: v.status, ok: v.ok, text: v.statusText};
		// 	return response;
		// 	// throw new Error;
		// } else {
		// }
	}, f_getTimef: function(v, a, d) {
		var time = v;
	    fullYear = time.getFullYear();
	    Mount = time.getMonth() + 1;
	    Day = time.getDate();
	    Hour = time.getHours();
	    Minute = time.getMinutes();
	    Second = time.getSeconds();
	    
	    var currentDate = fullYear + '-' + Mount + '-' + Day,
    		currentTime = Hour + ':' + Minute + ':' + Second;
    	
    	return currentDate + " " + currentTime;

	}, sendPost: function(v, a, d) {
		var file = 'file.jpg'; 
		f.f_setBrowserHeadersf(v, a, d);
		// fetch('http://likebot.ir/build/channel_files/telegram/image/1512308421.jpg').then(function(image){
		// 	image = image.blob();
		// 	lg(image);
		// 	var data = new FormData()
		// 	data.append('photo', image)
		// 	data.append('upload_id', 1512327216315)
		// 	data.append('media_type', 1)
		// 	var headers = f.f_headers1f(v, 'QcP8afAHtiBdCOMFNDY8qzpfimFrRoBN', data);
		// 	lg(headers)
		// 	f.post(v, 'https://www.instagram.com/create/upload/photo/', headers).then(f.toJson).then(function(id){
		// 		lg(id);
		// 	})
		// });
		// lg(file.buildlog());
		

		// UTILS
		var processStatus = function (response) {// process status
		  if (response.status === 200 || response.status === 0) {
		    return Promise.resolve(response)
		  } else {
		    return Promise.reject(new Error('Error loading: ' + url))
		  }
		};

		var parseBlob = function (response) {
		  return response.blob({type: "image/jpeg"});
		};

		var parseJson = function (response) {
		  return response.json();
		};

		// download/upload
		var downloadFile = function (url) {
		  return fetch(url)
		    .then(processStatus)
		    .then(parseBlob);
		};

		function uploadImageToImgur(blob) {
		  var formData = new FormData();

			formData.append('upload_id', 1512327216315)
		  	formData.append('photo', blob, 'photo.jpg');
			formData.append('media_type', "1")
			var headers = {
		    method: "POST",
				headers: {
					Accept: "*/*",
					// "Content-Type": "multipart/form-data; boundary=------WebKitFormBoundary0QSAxzaPT4dAfwuP",
					"X-CSRFToken": "QcP8afAHtiBdCOMFNDY8qzpfimFrRoBN",
					"X-Instagram-Ajax": "1",
					"X-Requested-With": "XMLHttpRequest"
				},
			credentials: "include",
			body: formData
		  };
		  return fetch('https://www.instagram.com/create/upload/photo/', headers).then(f.toJson).then(function(id){
		  	lg(id.upload_id);
		  	var data = new FormData();
		  	data.append('caption', 'yesssssssssss')
			data.append('upload_id', id.upload_id)
			// var data1 = {caption: 'yesssssss', upload_id:id.upload_id};
			// data.append('json', JSON.stringify(data1));

			var headers = {
			    method: "POST",
					headers: {
						Accept: "*/*",
						// "Content-Type": "application/x-www-form-urlencoded",
						"X-CSRFToken": "QcP8afAHtiBdCOMFNDY8qzpfimFrRoBN",
						"X-Instagram-Ajax": "1",
						"X-Requested-With": "XMLHttpRequest",
						"user-agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Mobile Safari/537.36"
					},
					credentials: "include",
					// body: JSON.stringify(data)
					body: data
			  };
		  	fetch('https://www.instagram.com/create/configure/', headers).then(function(res) {
		  		lg(res)
		  	})
		  })
		}

		// --- ACTION ---
		var sourceImageUrl = 'https://www.likebot.ir/build/channel_files/telegram/image/1512308421.jpg';
		console.log('Started downloading image from <a href="' + sourceImageUrl + '">hospodarets.com url</a>');

		downloadFile(sourceImageUrl)// download file from one resource
		  .then(uploadImageToImgur)// upload it to another
		  .then(function (data) {
		    console.log(data);
		    //console.log('<img src="' + data.data.link.replace('http:', 'https:') + '"/>');// for demo
		  })
		  .catch(function (error) {
		    console.error(error.message ? error.message : error);
		  });
	}

}
f['start']();
// lg(1&2);

// var d = {
// 	a: {
// 		b:'1',
// 		d:'3'
// 	},
// 	k: {
// 		c:'3',
// 		w:'4'
// 	}
// };
// // // lg(d);
// s = f.f_deleteKeysf('aa', d, ['a.b', 'k.s', 'k.w']);
// lg(s);
// f['sendPost']();
// var 
// 	urlHome = "https://www.instagram.com/",
// 	urlTags = "https://www.instagram.com/explore/tags/%D8%B3%D9%86%DA%AF_f%D8%A7%D9%88%D9%86%DB%8C%DA%A9%D8%B3/",
// 	urlPost = "https://www.instagram.com/p/Ba6qevqlenD/",
// 	likeUrl = "https://www.instagram.com/web/likes/1240630512637754704/like/";
// var csrfMobile = "uUyfZayvnIQMGls8hi1rCe7H37Zjrygh",
// 	csrfDesktop = "Jz8UsjBcBZUWNjT4paFtUpOVkvqwKaOV",
// 	csrfDesktopc = "L6c7aXDQaXy4p6aCmiI2VIOzpkIx14nt";

// var rr = {	
	
// 		Accept: "application/json, text/javascript, */*; q=0.01",
// 		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
// 		"X-CSRFToken": csrfDesktopc,
// 		"X-Instagram-Ajax": "1",
// 		"X-Requested-With": "XMLHttpRequest"
	
// }
// // setUser();
// function setUser()
// {
	
// }

