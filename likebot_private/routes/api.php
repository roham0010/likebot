<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::group(['prefix' => 'v1', 'namespace' => 'Api', 'middleware' => ['api']], function () {

    Route::group(['prefix' => 'token'], function () {
        Route::post('create', 'Auth\LoginController@createAccessToken');
        Route::post('create/withCode', 'Auth\LoginController@createAccessTokenWithCode');
    });

    // Auth api
    Route::group(['middleware' => ['auth:api']], function () {

        Route::post('/user', function (Request $request) {
            echo json_encode(['user' => $request->user()]);
        });

        Route::group(['prefix' => 'sync'], function () {
            Route::get('', 'HomeController@sync');
            Route::post('', 'HomeController@sync');
            Route::put('', 'HomeController@syncPut');
            Route::put('setStatus', 'HomeController@setStatus');
        });

    });
    // Tests
    Route::group(['prefix' => 'test'], function () {
        Route::get('sync/put', 'HomeController@syncPut');
        Route::get('sync/get', 'HomeController@sync');
    });
});
