<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['namespace' => 'Web', 'middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    });
    Auth::routes();
    // Logins Auth Check login
    Route::post('/checkUsername', 'Auth\LoginController@checkUsername')->name('checkUsername');
    Route::get('/logout', 'HomeController@logout')->name('logout');

    Route::get('/home', 'HomeController@index')->name('home');

    // pricing
    Route::group(['prefix' => 'pricing', 'as' => 'pricing.'], function () {
        Route::get('', 'HomeController@pricing')->name('pricing');
        Route::post('apply', 'FactorController@applyPricing')->name('apply');
    });

    // Blog
    Route::group(['prefix' => 'blog', 'as' => 'blog.'], function () {
        Route::get('', 'BlogController@all')->name('all');
        Route::get('cat/{title}-{category}', 'BlogController@all')->name('category');
        Route::get('{titleId}', 'BlogController@one')->name('one');
    });

    // Auth profile
    Route::group(['prefix' => 'profile', 'middleware' => 'auth', 'as' => 'profile.'], function () {
        Route::get('', 'UserController@dashboard')->name('dashboard');

        Route::get('/dashboard', 'UserController@dashboard')->name('profile');

        // profile extension
        Route::group(['prefix' => 'extension', 'as' => 'extension.'], function () {
            Route::post('/code/create', 'UserController@extensionCodeCreate')->name('code.create');
            Route::get('/code/create', 'UserController@extensionCodeCreate')->name('code.create.get');
        });

        // profile payments
        Route::group(['prefix' => 'payments', 'as' => 'payments.'], function () {
            Route::get('', 'FactorController@payments')->name('payments');
            Route::get('callback', 'FactorController@paymentsCallback')->name('callback');
        });

        Route::group(['prefix' => 'tags', 'as' => 'tags.'], function () {
            Route::post('/edit', 'TagController@edit')->name('edit');
        });

    });

    Route::group(['prefix' => 'test'], function () {
    });

});
Route::group(['namespace' => 'Cron', 'prefix' => 'cron'], function () {
    Route::group(['prefix' => 'f90dsafsuf0fjsd9a0fsuf0700y5klvr4y8b9tqut4q8t4q270'], function () {
        Route::group(['prefix' => 'twitter'], function () {
            Route::get('/get', 'TwitterController@get');
            Route::get('/send', 'TwitterController@sendToTelegram');
        });
        Route::group(['prefix' => 'telegram'], function () {
            Route::post('/get', 'TelegramController@get');
            Route::get('/get', 'TelegramController@get');
        });
        Route::group(['prefix' => 'instagram'], function () {
            Route::get('/get', 'InstagramController@get');
            Route::get('/sendSweetVideo', 'InstagramController@sendSweetVideo');
            Route::get('/renew', 'InstagramController@renewUrls');
            Route::get('/get/tags', 'InstagramController@getTags');
            Route::get('/send/wallpaper', 'InstagramController@sendToTelegramWallpaper');
        });
    });
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::group(['prefix' => 'telegram'], function () {
        Route::get('/sentPostsFix', 'TelegramController@sentPostsFix');
        Route::get('/all', 'TelegramController@all');
        Route::post('{telegram_post}/delete', 'TelegramController@delete');
        Route::post('{telegram_post}/active', 'TelegramController@active');
        Route::get('/fillFileNames', 'TelegramController@fillFileNames');
    });
    Route::group(['prefix' => 'instagram'], function () {
        Route::get('/fillFileNames', 'InstagramController@fillFileNames');
        Route::get('/newFileName', 'InstagramController@newFileNames');
    });
    Route::group(['prefix' => 'tests'], function () {
        Route::get('/all', 'TelegramController@tests');
    });
});
