<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factors', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['init', 'failed', 'payed']);
            $table->integer('user_id');
            $table->integer('price')->nullable();
            $table->integer('likes')->nullable();
            $table->integer('real_price')->nullable();
            $table->integer('real_likes')->nullable();
            $table->enum('type', ['buy_plan']);
            $table->text('discount_description')->nullable();
            $table->biginteger('gateway_id')->nullable();
            $table->string('tracking_code')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            // $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factors');
    }
}
