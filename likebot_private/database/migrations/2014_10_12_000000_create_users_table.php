<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            // $table->string('username')->unique();
            $table->string('email')->nullable();
            $table->string('mobile', 13)->nullable();
            $table->text('tags')->nullable();
            $table->string('password');
            $table->integer('extension_code')->nullable();
            $table->string('username')->nullable();
            $table->integer('liked_count')->default(0);
            $table->integer('like_count')->default(0);
            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            // $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
