<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikedPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liked_posts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('post_id');
            $table->enum('type', ['like', 'comment', 'follow', 'unfollow', 'post']);
            $table->enum('status', ['active', 'deactive']);
            $table->string('user_id');
            $table->string('content_type')->nullable();
            $table->string('instagram_username')->nullable();
            $table->string('instagram_userid')->nullable();
            $table->string('tag')->nullable();
            $table->string('shortcode')->nullable();
            $table->text('text')->nullable();
            $table->text('extra')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liked_posts');
    }
}
