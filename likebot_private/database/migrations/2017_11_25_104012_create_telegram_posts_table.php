<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_posts', function (Blueprint $table) {
            $table->increments('id');

            $table->enum('type', ['text', 'image', 'video', 'audio', 'voice', 'document', 'wallpaper', 'instafunvideo', 'craft', 'morning']);
            $table->enum('status', ['new', 'active', 'deactive', 'sent']);
            $table->string('content_type');
            $table->string('user_name');
            $table->string('reply');
            $table->string('text')->nullable();
            $table->string('user_title')->nullable();
            $table->string('media_id')->nullable();
            $table->string('file_name')->nullable();
            $table->string('telegram_update_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('url')->nullable();
            $table->string('from', 64)->nullable();
            $table->text('extra')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_posts');
    }
}
