@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                        <p class="well ">
                            <span>Filtered Total : {{ $posts->total() }}</span> |||
                            @foreach($counts as $key => $count)
                                <span>Count {{ $key }}: {{ $counts->{$key} }}</span> |||
                            @endforeach
                        </p>
                    <span>{{ $posts->appends($_GET)->links() }}</span>
                        <p class="well lead" style="position: fixed;left: 0px;top: 0px;width: 280px;z-index: 1000;">
                            <span class='emojies'>#ویدئو_فان</span><span class='emojies'> \n</span><span class='emojies'>😂</span><span class='emojies'>👻</span><span class='emojies'>😁</span><span class='emojies'>😌</span><span class='emojies'>🙈</span><span class='emojies'>👍</span><span class='emojies'>🏻👍</span><span class='emojies'>😭</span><span class='emojies'>😍</span><span class='emojies'>🌹</span><span class='emojies'>😩</span><span class='emojies'>👌</span><span class='emojies'>🏻</span><span class='emojies'>👏</span><span class='emojies'>🏻</span><span class='emojies'>😒</span><span class='emojies'>😕</span><span class='emojies'>🙄</span><span class='emojies'>😑</span><span class='emojies'>😳</span><span class='emojies'>🤔</span><span class='emojies'>🌚</span><span class='emojies'>🙏</span><span class='emojies'>🏻</span><span class='emojies'>😔</span><span class='emojies'>😐</span><span class='emojies'>😫</span><span class='emojies'>😱</span><span class='emojies'>😜</span><span class='emojies'>😘</span><span class='emojies'>😊</span><span class='emojies'>❤</span><span class='emojies'>️</span><span class='emojies'>☺</span><span class='emojies'>️</span><span class='emojies'>😄</span><span class='emojies'>💋</span><span class='emojies'>😉</span><span class='emojies'>😃</span><span class='emojies'>😢</span><span class='emojies'>🙊</span><span class='emojies'>😚</span><span class='emojies'>😅</span><span class='emojies'>😞</span><span class='emojies'>😏</span><span class='emojies'>😡</span><span class='emojies'>😝</span>
                        </p>
                        <form class="well"  action="#">
                            @foreach(\App\Telegram_post::getTypes('all') as $type)
                                <label for="{{ $type }}"> {{ $type }}: </label> <input id="{{ $type }}" value="{{ $type }}" type="radio" name="type">
                            @endforeach
                            <br>
                            @foreach(['cake', 'cakelearn'] as $type)
                                <label for="{{ $type }}"> {{ $type }}: </label> <input id="{{ $type }}" value="{{ $type }}" type="radio" name="content_type">
                            @endforeach
                            <br>
                            @foreach(['new', 'active', 'deactive'] as $status)
                                <label for="{{ $status }}"> {{ $status }}: </label> <input id="{{ $status }}" value="{{ $status }}" type="radio" name="status">
                            @endforeach
                            <br>
                            <button>filter</button>
                        </form>
                        @foreach($posts as $post)
                            @php
                                $extra = json_decode($post->extra);
                                if (!empty($post->file_name)) {
                                    $postType = preg_match('/\.mp4$/', $post->file_name) ? 'video' : 'image';
                                    $localUrl = url("build/channel_files/$post->from/$postType/$post->file_name");
                                    $localUrl1 = url("build/channel_files/$post->from/$postType/");
                                } else {
                                    if (isset($extra->images)) {
                                        $localUrl = $extra->images[0]->url;
                                    } else if (isset($extra->videos)) {
                                        $localUrl = $extra->videos[0]->url;
                                    }
                                }
                                $text = (($post->content_type == 'cakelearn') ? $extra->oldInfo->text.' ':'').$post->text;
                            @endphp
                            <span id="row-post-{{ $post->id }}">
                                <br>
                                {{ $post->user_name }}
                                <br>
                                <form method="post" class="ajax-form">
                                    <p>
                                        {{-- <label for="immidiatelysend">send</label> --}}
                                        <input style="padding: 40px" value="{{ $post->id }}" type="checkbox" name="immidiatelysend" id="immidiatelysend">
                                        <input value="{{ $post->id }}" type="hidden" name="post_id">
                                        <textarea class="post-text" style="width: 400px;height: {{ (mb_strlen($text)/3)*2 }}px;direction: rtl;" name="text">{{ $text }}</textarea>
                                        <span  class="pull-right">
                                            @if($post->type != 'text')
                                                @if(isset($extra->images) || $post->type == 'image')
                                                    <a target="_blank" href="">
                                                        <img style="width:250px;height: 200px;" src="{{ $localUrl }}">
                                                    </a>
                                                @elseif(isset($extra->audios))
                                                    <audio controls preload="none">
                                                        <source src="{{ $extra->audios[0]->url }}" type="audio/mpeg">
                                                        <source src="{{ $extra->audios[0]->url }}" type="audio/ogg">
                                                        Your browser does not support the audio element.
                                                    </audio>
                                                @elseif(in_array($post->type, \App\Telegram_post::getTypes('video')))
                                                    @php
                                                        $extra = json_decode($post->extra);
                                                        $instaUrl = @$extra->videos[0]->url;
                                                        if($post->from == 'instagram') {
                                                            $instaType = $post->type == 'image' ? 'image' : 'video';
                                                            // $instaUrl = "/build/channel_files/$post->from/$instaType/$post->media_id.mp4";
                                                            $instaUrl = $localUrl;
                                                        }

                                                    @endphp
                                                    <video width="270" height="240" controls preload="none">
                                                      <source src="{{ $instaUrl }}">
                                                    </video>
                                                @elseif(isset($extra->documents))
                                                    @if($extra->documentInfo->mime_type == \App\Telegram_post::getMimeTypes('video'))
                                                        <a href="{{ $extra->documents[0]->url }}">
                                                            <video width="320" height="240" controls preload="none">
                                                              <source src="{{ $extra->documents[0]->url }}">
                                                              {{-- <source src="movie.ogg" type="video/ogg"> --}}
                                                            </video>
                                                            {{-- <video style="width:100px;height: 100px;" src=""> --}}
                                                        </a>
                                                    @elseif($extra->documentInfo->mime_type == \App\Telegram_post::getMimeTypes('pdf'))
                                                        {{ $extra->documentInfo->file_name }}
                                                    @endif
                                                @endif
                                            @endif
                                        </span>
                                    </p>

                                    @isset($extra->thumb_src)
                                        <img class="pull-right" style="width:150px;height: 70;" src="{{ $extra->thumb_src }}">
                                    @endisset
                                    <span>{{ @substr(preg_replace('/[^a-zA-Z1-9\s]/', '', $extra->oldInfo->text), 0, 200) }}</span>
                                    <br>
                                    <span>

                                        <button myaction="{{ url("admin/telegram/$post->id/active") }}" class="ajax-btn">active</button>
                                        <button myaction="{{ url("admin/telegram/$post->id/delete") }}" class="ajax-btn">delete</button>
                                    </span>
                                </form>
                                    <br>
                                    <br>
                                    <br>
                                    <br>


                                <br>


                            </span>
                        @endforeach
                    <span>{{ $posts->appends($_GET)->links() }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        activeTextarea = $('.post-text').first();
        $('.post-text').on('click', function() {
           activeTextarea = $(this);
        });

        $('.emojies').click(function() {
            var caretPos = activeTextarea[0].selectionStart;
            var textAreaTxt = activeTextarea.val();
            var txtToAdd = $(this).html();
            activeTextarea.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
            activeTextarea.focus();
            activeTextarea[0].selectionStart = caretPos+txtToAdd.length;
            activeTextarea[0].selectionEnd = caretPos+txtToAdd.length;
            // activeTextarea.val(activeTextarea.val() + " " + );
        });
        $('.ajax-btn').click(function() {
            setTimeout(function(){
                activeTextarea = $('.post-text').first();
            }, 1000);
            console.log(activeTextarea)
        });
    // alert($('.post-text').val());

    })
</script>
@endsection
