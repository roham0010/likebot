@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @foreach($tests as $test)
                        @php
                            print_r(json_decode($test['a']));
                        @endphp
                    @endforeach
                    <span>{{ $tests->appends($_GET)->links() }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
