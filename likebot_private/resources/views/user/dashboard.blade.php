@extends('layouts.app', ['include' => ['profile-nav'], 'excepts' => ['nav']])
@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default ">
                <div class="panel-heading">پنل کاربری</div>

                <div class="panel-body form-group">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="pre-dashboard">
                    <p> نام کاربری : {{ user()->name }}</p>
                    @if(user()->extension_code > 100000)
                    <p>کد افزونه : {{ user()->extension_code }}</p>

                    @else
                        <form action="{{ myRoute('profile.extension.code.create') }}" class="ajax-form">

                   <p> دریافت تگ جدید:  </p>
                          <button class="btn btn-success">{{ trans('all.send') }}</button>
                        </form>
                    @endif
                    <p>  {{ trans('dashboard.liked_count') }}: {{ user()->liked_count }} </p>
                    <p>  {{ trans('dashboard.remained_like_count') }}: {{ user()->like_count }} </p>

                    <form action="{{ myRoute('profile.tags.edit') }}" class="ajax-form">
                        <textarea class="form-control" rows="3" name="tags" placeholder="تگ های مورد نظر برای لایک کردن را با # وارد کنید">{{ implode(',',json_decode((!empty(user()->tags) ? user()->tags : '[""]'))) }}</textarea>
                        <button class="btn btn-info fl-left">{{ trans('all.edit').' '.trans('all.tags') }}</button>
                    </form>
                    @foreach($likedPosts as $likedPost)
                        @php
                        @endphp
                        <img src="{{ $likedPost->media() }}">
                    @endforeach
</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
