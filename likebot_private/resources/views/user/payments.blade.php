@extends('layouts.app', ['include' => ['profile-nav'], 'excepts' => ['nav']])

@section('content')





<div class="limiter">
    <div class="container-login100">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">خریدها</div>

                <div class="panel-body">
                     <div class="pre-dashboard">
                    @foreach($payments as $payment)
                    <div class="table-responsive">
<table class="table pay-table">
      <thead>
        <tr>
          <th>ردیف</th>
          <th>کد</th>
          <th>مبلغ</th>
          <th>تعداد </th>
          <th>تخفیف</th>
          <th>لایک اضافه</th>
          <th>وضعیت</th>
        </tr>
      </thead>
      <tbody">
        <tr>
          <td>1</td>
          <td>{{ $payment->id }}</td>
          <td>{{ $payment->price }}</td>
          <td>{{ $payment->likes }}</td>
          <td>{{ $payment->priceDiscount() }}</td>
          <td>{{ $payment->likeDiscount() }}</td>
          <td>{{ $payment->status() }}</td>
        </tr>
      </tbody>
    </table>


</div>
                    @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
