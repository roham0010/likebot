@extends('layouts.app')

@section('content')
<div class="container blue-bg">
        <div class="row">
            <div class="main_home">
                <div class="col-md-12">

                    {{ session('message') }}


<section>
                <div class="container">
                    <div class="row">
                        <div class="main_drag roomy-50">
                            <div class="col-md-12">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide" style="background-image:url({{myAsset('images/drag1.png')}})">

                                <div class="home_btns btn-drag">
                                     <p class="like-ok">5k لایک</p>
                                     <p class="prev-text">5k لایک</p>
                                     <p class="price-ok">12000 تومان</p>
                                     <p class="prev-text">12000 تومان</p>
                                                                     <a href="" class="btn btn-danger ">انتخاب کنید</a>
                                </div>
                                        </div>
                                                                                <div class="swiper-slide" style="background-image:url({{myAsset('images/drag2.png')}})">

                                                                        <div class="home_btns btn-drag">
                                                                             <p class="like-ok">10k لایک</p>
                                                                             <p class="prev-text">10k لایک</p>
                                                                              <p class="price-ok">21000 تومان</p>
                                                                              <p class="prev-text">21000 تومان</p>
                                                                                                             <a href="" class="btn btn-danger ">انتخاب کنید</a>
                                                                        </div>
                                                                                </div>
                                        <div class="swiper-slide"style="background-image:url({{myAsset('images/drag3.png')}})">
                                                 <div class="home_btns btn-drag">
                                                      <p class="like-ok">25k لایک</p>
                                                      <p class="prev-text">25k لایک</p>
                                                      <p class="price-ok">49000 تومان</p>
                                                      <p class="prev-text">49000 تومان</p>
                                                                     <a href="" class="btn btn-danger ">انتخاب کنید</a>
                                </div>
                                        </div>

             <div class="swiper-slide" style="background-image:url({{myAsset('images/drag4.png')}})">
                                                 <div class="home_btns btn-drag">
                                                   <p class="like-ok">50k لایک</p>
                                                   <p class="prev-text">50k لایک</p>
                                                    <p class="price-ok">89000 تومان</p>
                                                    <p class="prev-text">89000 تومان</p>
                                                                     <a href="" class="btn btn-danger ">انتخاب کنید</a>
                                </div>
                                        </div>


             <div class="swiper-slide" style="background-image:url({{myAsset('images/drag5.png')}})">
                                                 <div class="home_btns btn-drag">
                                                 <p class="like-ok">100k لایک</p>
                                                 <p class="prev-text">100k لایک</p>
                                                 <p class="price-ok">159000 تومان</p>
                                                 <p class="prev-text">159000 تومان</p>
                                                                     <a href="" class="btn btn-danger ">انتخاب کنید</a>
                                </div>
                                        </div>


                                    </div>
                                    <!-- If we need navigation buttons -->
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-button-next"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>










                    <form action="{{ route('pricing.apply') }}" method="post">
                        {{ csrf_field() }}
                        @foreach($plans as $key => $plan)
                            @php
                                $discountId = $plan['discount_id'];
                                $likes = $plan['likes'];
                                $price = $plan['price'];
                                $discountDescription = '';
                                $discount = $discounts[$discountId];
                                if ($discountId > 0 && $discount &&
                                        (
                                            ($discount['price'] > 0 && $discount['price'] < $plan['price']) ||
                                            ($discount['likes'] > 0 && $discount['likes'] > $plan['likes'])
                                        )
                                    )  {
                                    $discountDescription = $discount['description'];
                                    $likes = $plan['likes'].(($discount['likes'] > 0 && $discount['likes'] > $plan['likes']) ? '/'.$discount['likes'] : '');
                                    $price = $plan['price'].(($discount['price'] > 0 && $discount['price'] < $plan['price']) ? '/'.$discount['price'] : '');
                                }
                            @endphp
                            {{-- <input type="hidden" name="planId" value="{{ $key }}"> --}}
                            <input type="radio" name="planId" value="{{ $key }}">
                            likes: {{ $likes }}
                            price: {{ $price }}
                            {{ $discountDescription }}
                        @endforeach
                        <button type="submit">Send</button>
                    </form>

                </div>
            </div>
        </div><!--End off row-->
    </div><!--End off container -->
@endsection
