@extends('layouts.app')

@section('content')
<!--Home Sections-->
<section id="home" class="home">
    <div class="container">
        <div class="row">
            <div class="main_home">
                <div class="col-md-7">
                    <div class="home_text">
                        <h1 class="text-white">سرویس رایگان لایک اتوماتیک اینستاگرام</h1>
                        <h3 class="text-yellow">راهی هوشمند برای بدست آوردن لایک و فالوور واقعی!</h3>
                    </div>

                    <div class="home_btns m-top-40">
                        <a href="" class="btn btn-primary m-top-20">بیشتر بدانید</a>
                        <a href="start.html" class="btn btn-danger m-top-20">همین حالا شروع کنید</a>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="phone_one phone_attr1 text-center sm-m-top-50">

                        <img src="{{ myAsset('images/first.gif') }}" alt="" />
                    </div>
                </div>

            </div>
            <div class="scrooldown">
                <a href="#features"><i class="fa fa-chevron-down"></i></a>
            </div>

        </div><!--End off row-->
    </div><!--End off container -->
</section> <!--End off Home Sections-->

<!--screen short section-->
<section id="features" class="screen_area">
    <div class="container">
        <div class="row">
            <div class="main_screen">
                <div class="col-md-8 col-md-offset-2 col-sm-12">
                    <div class="head_title text-center">
                        <h2>مزایا</h2>
                        <h5>سادگی،امنیت،هوشمندی و سرعت</h5>
                    </div>
                </div>

                <!-- Screen01-->
                <div class="screen01">
                    <div class="col-md-6">
                        <div class="screen01_img text-center">
                            <img src="{{ myAsset('images/simplicity.png') }}" alt="" />

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="screen01_content m-top-50">

                            <h2 class="m-top-40">سادگی</h2>
                            <p class="m-top-20">لایکبوت طوری طراحی شده که همه کاربران برای افزایش لایک و فالوورشون میتونن
                                خیلی راحت ازش استفاده کنن.لایکبوت با لایک کردن پست های دیگران بجای شما،توجه افراد را به
                                اکانت شما جلب می کند.
                            </p>

                        </div>
                    </div>
                </div><!--End off Screen01-->

                <!-- Screen02-->
                <div class="screen02 m-top-50">
                    <div class="col-md-6 col-md-push-6">
                        <div class="screen02_img text-center">

                            <img src="{{ myAsset('images/security.png') }}" alt="" />

                        </div>
                    </div>
                    <div class="col-md-6 col-md-pull-6">
                        <div class="screen02_content m-top-50">

                            <h2 class="m-top-40">امنیت</h2>
                            <p class="m-top-20">
                                لایکبوت به حریم خصوصی کاربرانش اهمیت ویژه ای میده.ما هیچ گونه اطلاعات ازقبیل نام کاربری و رمز ورود به حساب اینستاگرام شما را در خواست نمی کند.
                            </p>


                        </div>
                    </div>
                </div><!--End off Screen02-->

                <!-- Screen03-->
                <div class="screen03 m-top-50">
                    <div class="col-md-6">
                        <div class="screen03_img text-center">

                            <img src="{{ myAsset('images/intelligence.png') }}" alt="" />

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="screen03_content m-top-50">

                            <h2 class="m-top-40">هوشمندی</h2>
                            <p class="m-top-20">در بازه های زمانی مختلف اقدام به لایک می کند.برای شبیه سازی رفتار انسانی،لایکبوت پس از یک لایک کردن پست ها در یک بازه زمانی،برای لحظاتی متوقف می شود تا ربات
                                بودن آن تشخیص داده نشود.</p>

           
                        </div>
                    </div>
                </div><!--End off Screen02-->

                <!-- Screen04-->


                <div class="screen02 m-top-50">
                    <div class="col-md-6 col-md-push-6">
                        <div class="screen02_img text-center">

                            <img src="{{ myAsset('images/speed.png') }}" alt="" />

                        </div>
                    </div>
                    <div class="col-md-6 col-md-pull-6">
                        <div class="screen02_content m-top-50">

                            <h2 class="m-top-40">سرعت</h2>
                            <p class="m-top-20">در بازه های زمانی مختلف اقدام به لایک می کند.برای شبیه سازی رفتار
                                انسانی،لایکبوت پس از یک لایک کردن پست ها در یک بازه زمانی،برای لحظاتی متوقف می شود تا ربات
                                بودن آن تشخیص داده نشود.
                            </p>


                        </div>
                    </div>
                </div>


    <!--End off Screen04-->
            
            </div><!--End off row-->
        </div>
    </div><!--End off container-->
</section><!--End off Screen01 Area Section -->

<!--blog Section-->
<section id="blog" class="reviews m-top-100">
    <div class="container">
        <div class="col-md-12  col-sm-12">
            <div class="head_title text-center">
                <h2>بلاگ</h2>
                <h5>بخوانیم، یاد بگیریم، به کار ببریم</h5>
            </div>

            <div class="col-lg-4">
                <div class="card">
                    <img src="http://lorempixel.com/400/200/people/1" class="img-responsive cover-photo">
                   <section class="mini-post">
                    <a href="">  <h5>برندهای معروف در شبکه های اجتماعی</h5>  </a>
                       <small>
                           <a href=""><i class="fa fa-folder-open-o padding-left-small"></i>متفرقه</a>
                       </small>
                       <p>معرفی تعدادی از برند های معروف در دنیا و همچنین تعدادی از کسب و کار های ایرانی که به فعالیت در توییتر واینستاگرام مشغول هستند</p>
                   
                   </section>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card">
                    <img src="http://lorempixel.com/400/200/people/2" class="img-responsive cover-photo">
                    <section class="mini-post">
                        <a href=""> <h5> افزایش فروش کالا با فعالیت در شبکه های اجتماعی</h5>  </a>
                        <small>
                            <a href=""><i class="fa fa-folder-open-o padding-left-small"></i>آموزشی</a>
                        </small>
                        <p>یکی از مهترین روش های تبلیغات کالا یا خدماتی که یه کسب و کار به مشتریان خود ارائه میکند استفاده از شبکه های اجتماعی و استفاده از امکان ارتباط مستقیم با خریداران و یا افرادی است که از خدمات ارائه شده ی آنها استفاده میکنند.</p>
      
                    </section>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card">
                    <img src="http://lorempixel.com/400/200/people/3" class="img-responsive cover-photo">
                    <section class="mini-post">
                        <a href=""><h5>چطور به بهترین روش در اینستاگرام کالا بگذاریم؟</h5></a>
                        <small>
                            <a href=""><i class="fa fa-folder-open-o padding-left-small"></i>متفرقه</a>
                        </small>
                        <p class="overflow-ellipsis">تعدادی عکس از روش های هیجان انگیز ومختلفی که میشه نوشیدنی های شکلاتی یه میان وعده  خوش مزه رو باهاش تزئین کرد براتون منتشر کردیم تا خودتون رو با یکی از این روش های خلاقانه مهمون کنید .</p>
   
                                        </section>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="text-center p-top-20 p-bottom-20">
                <a href="" class="btn btn-danger m-top-30">نوشته های بیشتر</a>
            </div>
        </div>
    </div><!-- End off container -->
</section><!-- End off People Section-->

<!--App Call Section-->
<section id="call" class="call">
    <div class="call_overlay"></div>
    <div class="container">
        <div class="row">
            <div class="main_call ">

                <div class="col-md-6">
                    <div class="call_item roomy-50">
                        <h2 class="text-white">از صحبت با شما خوشحال می‌شویم</h2>
                        <h5 class="text-white">برای اینکه بتونیم سرویس بهتری به شما ارائه بدیم مشتاقانه منتظر نظرات و پیشنهادهای شما هستیم :)</h5>

                        <div class="attr-nav call_icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-phone text-white"></i></a></li>
                                <li><a href="#"><i class="fa fa-telegram text-white"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram  text-white"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter  text-white"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="call_item m-top-70">
                        <img class="app_right" src="{{ myAsset('images/call.png') }}" alt="" />
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>

<!-- scroll up-->
<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div><!-- End off scroll up -->
@endsection