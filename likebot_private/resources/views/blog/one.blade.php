@extends('layouts.app')

@section('content')

<section id="blog" class="reviews">
    <div class="container">
    <br>
    <br>
        <div class="col-md-12  col-sm-12">

                <div class="card">
                    <img src="{{ $post->id }}.jpg" class="img-responsive cover-photo">
                   <section class="mini-post">
                    <a href="{{ myUrl("blog/")."/".url_optimizer($post->title)."-$post->id" }}">  <h5>{{ $post->title }}</h5>  </a>
                       <small>
                           <a href=""><i class="fa fa-folder-open-o padding-left-small"></i>{{ trans("blog.cats.$post->type") }}</a>
                       </small>
                       <p>{!! $post->full_text !!}</p>
                   </section>
                </div>
        </div>

    </div><!-- End off container -->
</section><!-- End off People Section-->

<!-- scroll up-->
<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div><!-- End off scroll up -->
@endsection
