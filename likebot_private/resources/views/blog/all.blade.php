@extends('layouts.app')

@section('content')

<section id="blog" class="reviews">
    <div class="container">
    <br>
    <br>
        <div class="col-md-12  col-sm-12">
            @foreach(trans('blog.cats') as $key => $cat)
                <a href="{{ myUrl("blog/cat/$cat-$key") }}"> {{ $key }} {{ $cat }}</a>
            @endforeach
        </div>
        <div class="col-md-12  col-sm-12">
            <div class="head_title text-center">
                <h2>بلاگ</h2>
                <h5>بخوانیم، یاد بگیریم، به کار ببریم</h5>
            </div>
            @foreach($posts as $post)
                <div class="col-lg-4">
                    <div class="card">
                        <img src="{{ $post->id }}.jpg" class="img-responsive cover-photo">
                       <section class="mini-post">
                        <a href="{{ myUrl("blog/")."/".url_optimizer($post->title)."-$post->id" }}">  <h5>{{ $post->title }}</h5>  </a>
                           <small>
                               <a href=""><i class="fa fa-folder-open-o padding-left-small"></i>{{ trans("blog.cats.$post->type") }}</a>
                           </small>
                           <p>{!! (mb_strlen($post->full_text) > 260) ? substr($post->full_text, 0, strpos($post->full_text, ' ', 260)) : $post->full_text !!}</p>
                       </section>
                    </div>
                </div>
            @endforeach
        </div>
        {{ $posts->links() }}

        <div class="col-md-12">
            <div class="text-center p-top-20 p-bottom-20">
                <a href="" class="btn btn-danger m-top-30">نوشته های بیشتر</a>
            </div>
        </div>
    </div><!-- End off container -->
</section><!-- End off People Section-->

<!-- scroll up-->
<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div><!-- End off scroll up -->
@endsection
