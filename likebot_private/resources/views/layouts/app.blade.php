<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fa"> <!--<![endif]-->
<head>
        <meta charset="utf-8">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <meta name="_token" content="{{ csrf_token() }}">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" sizes="180x180" href="{{ myAsset('images/favicon/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ myAsset('images/favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ myAsset('images/favicon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ myAsset('images/favicon/manifest.json') }}">
        <link rel="mask-icon" href="{{ myAsset('images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">




        <link rel="stylesheet" href="{{ myAsset('css/style-googsell.css') }}">
        {{-- <link rel="stylesheet" href="{{ myAsset('css/style.css') }}"> --}}
        <link rel="stylesheet" href="{{ myAsset('css/app.css') }}">
        <link rel="stylesheet" href="{{ myAsset('css/swiper.min.css') }}">
        <link rel="stylesheet" href="{{ myAsset('css/animate.css') }}">
        <link rel="stylesheet" href="{{ myAsset('css/iconfont.css') }}">
        <link rel="stylesheet" href="{{ myAsset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ myAsset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ myAsset('css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ myAsset('css/bootsnav.css') }}">


        <!--For Plugins external css-->
        <!--<link rel="stylesheet" href="{{ myAsset('css/plugins.css') }}/>-->
        <!--Theme custom css -->
        <link rel="stylesheet" href="{{ myAsset('css/style.css') }}">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="{{ myAsset('css/responsive.css') }}" />
        <!-- IranSans Font -->
        <meta name="fontiran.com:license" content="کد ۵ رقمی لایسنس">

        <script src="{{ myAsset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
        @if(!isLocal())

        <!--Google Font link-->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-63051038-3"></script>
        @endif
        <!-- Google Webmaster tools -->
        <meta name="google-site-verification" content="0Ce8DkItpmGAVPJJ2THkt-lJGNW4jaHsWJ4dUS1b_xc" />
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-63051038-3');
        </script>

    </head>

    <body data-spy="scroll" data-target=".navbar-collapse">


        <!-- Preloader -->
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="heartbeat"></div>
               </div>
            </div>
        </div><!--End off Preloader -->



            <!--Home page style-->
            @php
            @endphp
            @if(!isset($excepts) || !in_array('nav', $excepts))
            <nav class="navbar navbar-default bootsnav navbar-fixed white no-background">
                <div class="container">

                    <!-- Start Atribute Navigation -->
                    <div class="attr-nav">
                        <ul>
                            <li><a href="#"><i class="fa fa-telegram"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <!-- End Atribute Navigation -->


                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="#brand">
                            <img src="{{ myAsset('images/logo.png') }}" class="logo logo-display" alt="">
                            <img src="{{ myAsset('images/footer-logo.png') }}" class="logo logo-scrolled" alt="">
                        </a>

                    </div>
                    <!-- End Header Navigation -->

                    <!-- navbar menu -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-center">
                            <li><a href="#home">خانه</a></li>
                            <li><a href="#features">مزایا</a></li>
                            <li><a href="#blog">بلاگ</a></li>
                            <li><a href="#call">تماس</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </nav>
            @elseif(isset($include) && in_array('profile-nav', $include))
            <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <div class="navbar-header">
                                <a class="navbar-brand" href="{{ myUrl('/') }}">
                                              <img src="{{ myAsset('images/logo.png') }}" class="logo logo-display" alt="">
          </a>
                </div>
                <ul class="nav navbar-nav">
                  <li class="active"><a href="{{ myUrl('/profile/dashboard') }}">داشبورد</a></li>
                  <li><a href="{{ myUrl('/profile/payments') }}">خریدها</a></li>
                  <li><a href="{{ myUrl('logout') }}">خروج</a></li>
                </ul>
              </div>
            </nav>
            @endif

        @yield('content')

        <!-- JS includes -->

        <script src="{{ myAsset('js/vendor/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ myAsset('js/vendor/bootstrap.min.js') }}"></script>

        <script src="{{ myAsset('js/jquery.magnific-popup.js') }}"></script>
        <script src="{{ myAsset('js/jquery.easing.1.3.js') }}"></script>
        <script src="{{ myAsset('js/swiper.min.js') }}"></script>
        <script src="{{ myAsset('js/jquery.collapse.js') }}"></script>
        <script src="{{ myAsset('js/bootsnav.js') }}"></script>



        <script src="{{ myAsset('js/plugins.js') }}"></script>
        <script src="{{ myAsset('js/main.js') }}"></script>
        <script src="{{ myAsset('js/myapp.js') }}"></script>
        <script type="text/javascript">
            document.getElementById("loading").remove();
        </script>
        @yield('script')


    </body>
</html>
