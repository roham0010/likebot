@extends('layouts.app', ['mobile_nav_title'=>'', 'nav_type'=> 'modal','target_modal'=> 'settings','nav_icon'=> 'icon-settings', 'no_cat_nav' => 'true', 'nav_class' => 'transparent' , 'footer'=>'copyright', 'title' =>  title_generator(['page' => trans('user_admin.Dashboard')])])

@section('content')

<main class="cl pb-4">
    <!-- /.cover-photo -->
    <header id="dashboard" class="cover-photo holder default_back">
        <div class="container">
            <div class="avatar rounded-image avatar-sm holder">
                <img src="{{my_asset()}}/img/avatar.jpg">
            </div> <!-- /.avatar -->

            <div class="reviews holder text-light holder pt-1 text-bold">
                {{-- <h1 class="text-lg holder text-light">{{trans('auth.welcome')}}</h1> --}}
                <a href="#" class="btn open-modal btn-round btn-border margin-right-xs open-modal btn-icon-left" data-target="#sign-in"> {{trans('auth.welcome')}} <i class="zmdi zmdi-chevron-left"></i></a>
             </div> 

         </div><!--  /.big-container -->
         <svg id="svg-box" class="block hide-lg holder" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 420 36"><defs><style>.cls-1{fill:#fff;}</style></defs><path class="cls-1" d="M0,183s67,17,210,17c140,0,210-16,210-16v16H0Z" transform="translate(0 -164)"/></svg>
    </header> <!-- /.cover-photo -->

    <!-- main articles -->
    <div class="container">
        

        <!-- side navigation in help center -->
        <div class="user-admin-side hide-xs card">
            <nav class="side-nav">
                <ul>
                    <li>
                        <a class="active" href="{{ url('/login') }}" title="{{trans('user_admin.Dashboard')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-home"></i><span> {{trans('user_admin.Dashboard')}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}" title="{{trans('all.mystore')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-store"></i><span> {{trans_choice('all.mystore', 0)}}</span>
                        </a>
                    </li>
                    <li>
                        <a class="open-nav has-side-submenu  {{$instagram or ''}}" href="#" title="{{trans('user_admin.orders')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-instagram"></i><span> {{trans('user_admin.instagram_manager')}}  <i class="pull-left zmdi zmdi-chevron-down"></i></span></span>
                        </a>
                        <ul class="side-submenu opacity-75 collapse">
                            <li>
                                <a class="{{$insta_dash or ''}}" href="{{ url('/login') }}" title="{{trans('instagram.dashboard')}}">
                                    <i class="zmdi zmdi-widgets"></i><span> {{trans('instagram.dashboard')}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="{{$follow_manage or ''}}" href="{{ url('/login') }}" title="{{trans('instagram.follow.title')}}">
                                    <i class="zmdi zmdi-accounts-add"></i><span> {{trans('instagram.follow.title')}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="{{$media_manager or ''}}" href="{{ url('/login') }}" title="{{trans('instagram.media.title')}}">
                                    <i class="zmdi zmdi-collection-image"></i><span> {{trans('instagram.media.title')}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="{{$likes_manage or ''}}" href="{{ url('/login') }}" title="{{trans('instagram.likes')}}">
                                    <i class="zmdi zmdi-favorite"></i><span> {{trans('instagram.like.title')}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="{{$tags_dash or ''}}" href="{{ url('/login') }}" title="{{trans('instagram.tags.title')}}">
                                    <i class="zmdi zmdi-tag"></i><span> {{trans('instagram.tags.title')}}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="open-nav has-side-submenu  {{$product_report or ''}}" href="#" title="{{trans('user_admin.products_manage')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-mall"></i><span>{{trans('user_admin.products_manage')}}  <i class="pull-left zmdi zmdi-chevron-down"></i></span></span>
                        </a>
                        <ul class="side-submenu opacity-75 collapse {{$product_report or ''}}">
                            <li class=" {{$product_report or ''}} ">
                                <a href=""{{ url('/login') }}">
                                    <i class="zmdi-hc-fw zmdi zmdi-plus"></i><span> {{trans('user_admin.new_product')}}</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ url('/login') }}">
                                    <i class="zmdi-hc-fw zmdi zmdi-mall"></i><span>{{trans('user_admin.products_proved')}} </span>
                                </a>
                            </li>
                            <li class="">
                                <a  href="{{ url('/login') }}" title="{{trans('product.my_product_list')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-format-list-bulleted"></i><span>{{trans('product.my_product_list')}} </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="open-nav has-side-submenu  {{$inorders or ''}}" href="#" title="{{trans('user_admin.orders')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-shopping-cart"></i><span> {{trans('user_admin.orders')}}  <i class="pull-left zmdi zmdi-chevron-down"></i></span></span>
                        </a>
                        <ul class="side-submenu opacity-75 collapse  {{$inorders or ''}}">
                            <li class=" {{$storeAllOrders or ''}} ">
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.all_orders')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-assignment"></i><span> {{trans('user_admin.all_orders')}} </span>
                                </a>
                            </li>
                            <li class="{{$storeInOrders or ''}} ">
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.received_orders')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-assignment-returned"></i><span>{{trans('user_admin.received_orders')}} </span>
                                </a>
                            </li>
                            <li class="{{$storeOrderReadyForSend or ''}} ">
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.waiting_orders')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-assignment-o"></i><span>{{trans('user_admin.waiting_orders')}} </span>
                                </a>
                            </li>
                            <li class="{{$storeOrderSent or ''}} ">
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.sended_orders')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-assignment-return"></i><span>{{trans('user_admin.sended_orders')}} </span>
                                </a>
                            </li>
                            <li class="{{$storeOrderReturned or ''}} ">
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.returned_orders')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-assignment-alert"></i><span>{{trans('user_admin.returned_orders')}}  </span>
                                </a>
                            </li>
                            <li class="{{$storeOrderComplete or ''}} ">
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.completed_orders')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-assignment-check"></i><span>{{trans('user_admin.completed_orders')}} </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="open-nav has-side-submenu  {{$disccounts or ''}}" href="#" title="{{trans('campaigns.discounts_manage')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-ticket-star"></i><span> {{trans('campaigns.discounts_manage')}}  <i class="pull-left zmdi zmdi-chevron-down"></i></span></span>
                        </a>
                        <ul class="side-submenu opacity-75 collapse {{$disccounts or ''}}">
                            <li>
                                <a  class=" {{$bons or ''}} " href="{{ url('/login') }}" title="{{trans('campaigns.bons.title')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-ticket-star"></i><span> {{trans('campaigns.bons.title')}}</span>
                                </a>
                            </li>
                            <li>
                                <a  class=" {{$createBon or ''}} " href="{{ url('/login') }}" title="{{trans('campaigns.bons.create_title')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-plus"></i><span> {{trans('campaigns.bons.create_title')}}</span>
                                </a>
                            </li>
                            <li>
                                <a  class=" {{$camps or ''}} " href="{{ url('/login') }}" title="{{trans('campaigns.camps')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-flare"></i><span> {{trans('campaigns.camps')}}</span>
                                </a>
                            </li>
                            <li>
                                <a  class=" {{$createCamp or ''}} " href="{{ url('/login') }}" title="{{trans('campaigns.createCamp')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-plus"></i><span> {{trans('campaigns.createCamp')}}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="{{$wishList or ''}} open-modal" href="#" data-target="#instagram-login" title="{{trans('user_admin.Likes')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-favorite"></i><span> {{trans('user_admin.Likes')}}</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{$orders or ''}}" href="{{ url('/login') }}" title="{{trans('user_admin.my-orders')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-shopping-basket"></i><span> {{trans('user_admin.my-orders')}}</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{$myCoupons or ''}}" href="{{ url('/login') }}" title="{{trans('campaigns.bons.my_coupons')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-ticket-star"></i><span> {{trans('campaigns.bons.my_coupons')}}</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{$notifications or ''}}" href="{{ url('/login') }}" title="{{trans('all.notification')}}">
                            <i class="zmdi-hc-fw zmdi zmdi-alert-circle"></i><span> {{trans('all.notification')}}</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{$tickets_side or ''}}" href="{{ url('/login') }}" title="{{trans('all.tickets')}}">

                            <i class="zmdi-hc-fw zmdi zmdi-help-outline"></i><span> {{trans('all.tickets')}}</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="open-nav has-side-submenu  {{$payments or ''}} {{$withDraw or ''}}"><i class="zmdi-hc-fw zmdi zmdi-money"></i><span> {{trans('user_admin.molly')}} <i class="pull-left zmdi zmdi-chevron-down"></i></span></a>
                        <ul class="side-submenu opacity-75 {{$payments or ''}} {{$withDraw or ''}} collapse">
                            <li>
                                <a class="{{$payments or ''}}" href="{{ url('/login') }}" title="{{trans('user_admin.payments')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-money-box"></i><span> {{trans('user_admin.payments')}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="{{$withDraw or ''}}" href="{{ url('/login') }}" title="{{trans('wallet.withdraw')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-money"></i><span> {{trans('wallet.withdraw')}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.bank_info')}}"><i class="zmdi-hc-fw zmdi zmdi-card"></i><span> {{trans('user_admin.bank_info')}} </span></a>
                                </li>
                            <li>
                        </ul>
                    </li>
                    <li>
                        <a class="{{$setting or ''}} open-nav has-side-submenu" href="#" title="{{trans('user_admin.setting')}}"><i class="zmdi-hc-fw zmdi zmdi-settings"></i><span> {{trans('user_admin.setting')}}  <i class="pull-left zmdi zmdi-chevron-down"></i></span></a>
                        <ul class="side-submenu opacity-75 {{$setting or ''}} {{$addresses or ''}} collapse">
                            <li>
                                <a class="{{$setting or ''}}" href="{{ url('/login') }}" title="{{trans('user_admin.setting')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-chevron-left"></i><span> {{trans('user_admin.setting')}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="{{$addresses or ''}}" href="{{ url('/login') }}" title="{{trans('user_admin.my-addresses')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-pin-drop"></i><span> {{trans('user_admin.my-addresses')}}</span>
                                </a>
                            </li>
                            
                            <li>
                                <a class="{{$reports or ''}}" href="{{ url('/login') }}" title="{{trans('all.report')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-alert-triangle"></i><span> {{trans('all.report')}}</span>
                                </a>
                            </li>
                            
                            <li>
                                <a href="{{ url('/login') }}" title="{{trans('auth.first_info')}}"><i class="zmdi-hc-fw zmdi zmdi-account-circle"></i><span> {{trans('auth.first_info')}} </span></a>
                                </li>
                            
                            <li>
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.store_info')}}"><i class="zmdi-hc-fw zmdi zmdi-store"></i><span> {{trans('user_admin.store_info')}} </span></a>
                                </li>
                            <li>
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.contact_info')}}"><i class="zmdi-hc-fw zmdi zmdi-phone-setting"></i><span> {{trans('user_admin.contact_info')}} </span></a>
                                </li>
                            
                            <li>
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.bank_info')}}"><i class="zmdi-hc-fw zmdi zmdi-card"></i><span> {{trans('user_admin.bank_info')}} </span></a>
                                </li>
                            <li>
                                <a href="{{ url('/login') }}" title="{{trans('user_admin.change_password')}}"><i class="zmdi-hc-fw zmdi zmdi-key"></i><span> {{trans('user_admin.change_password')}} </span></a>
                            </li>
                            <li>
                                <a class="" href="{{url('/')}}/logout" title="{{trans('user_admin.logout')}}">
                                    <i class="zmdi-hc-fw zmdi zmdi-power"></i><span> {{trans('user_admin.logout')}}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav><!-- /side navigation in help center -->
        </div> <!-- /.card -->

        <section class="main-article cl">

            @if(!empty(session('message')))
                <p class="alert alert-success margin-top-md col-24">
                    {{session('message')}}
                </p>
            @endif

            <!--- store  -->
            <div class="cl bg-light dashboard-menu ">
                <ul class="table-nav pt-15 pb-15">
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="icon-plus text-primary"></i>
                            <small>0</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="icon-">0</i>
                            <small>{{trans('user_admin.products_proved')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="icon-">0</i>
                            <small>{{trans('user_admin.pic_on_insta')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="icon-">0</i>
                            <small>{{trans('campaigns.bons.title')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}" class=" ">
                            <i class="holder icon-badge">
                            </i>
                            <small>{{trans('جشنواره')}}</small>
                        </a>
                    </li>
                </ul><!-- small menu  -->
            </div>
            <div class="cl bg-light  dashboard-menu mb-1 mt-1">
                <ul class="table-nav pt-15 pb-15">
                    <li>
                        <a href="{{ url('/login') }}" class="border-left">
                            <i class="holder zmdi  zmdi-hc-lg zmdi-assignment">

                            </i>
                            <small>{{trans('user_admin.smallmenu.orders')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="holder text-primary zmdi  zmdi-hc-lg zmdi-flash">
                            </i>
                            <small>{{trans('user_admin.smallmenu.new_order')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="holder text-info zmdi  zmdi-hc-lg zmdi-assignment-o">
                            </i>
                            <small>{{trans('user_admin.smallmenu.wait_for_pay')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="holder text-info zmdi  zmdi-hc-lg zmdi-comment-more">
                            </i>
                            <small>{{trans('user_admin.smallmenu.reviews')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="holder text-warning zmdi  zmdi-hc-lg zmdi-wrench">
                            </i>
                            <small>{{trans('user_admin.smallmenu.problem_store')}}</small>
                        </a>
                    </li>
                </ul><!-- small menu  -->
            </div>

            <!--- small menu  -->
            <div class="cl bg-light border-bottom dashboard-menu">
                <ul class="table-nav pt-15 pb-15">
                    <li>
                        <a href="{{ url('/login') }}" class="border-left">
                            <i class="holder icon-briefcase  text-primary">

                            </i>
                            <small>{{trans('user_admin.smallmenu.buy_list')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="holder icon-credit-card">
                            </i>
                            <small>{{trans('user_admin.smallmenu.notpaid')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="holder icon-plane">
                            </i>
                            <small>{{trans('user_admin.smallmenu.onways')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="holder icon-speech">
                            </i>
                            <small>{{trans('user_admin.smallmenu.onreview')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="holder icon-wrench">
                            </i>
                            <small>{{trans('user_admin.smallmenu.problem')}}</small>
                        </a>
                    </li>
                </ul><!-- small menu  -->
            </div>
            <div class="cl bg-light  border-bottom dashboard-menu">
                <ul class="table-nav pt-15 pb-15">
                    <li>
                        <a href="{{ url('/login') }}" class="border-left open-dropdown" data-target="#wallet-dropdown">
                            <i class="holder icon-wallet text-primary">
                            </i>
                            <small>{{trans('wallet.Wallet')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="icon-">
                                3000
                            </i>
                            <small>{{trans('wallet.balance')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="icon-">
                                0
                            </i>
                            <small>{{trans('wallet.potential_balance')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="icon-">
                                0
                            </i>
                            <small>{{trans('campaigns.bons.my_coupons')}}</small>
                        </a>
                    </li>
                    <li>
                        <a  href="{{ url('/login') }}" title="{{trans('wallet.topup')}}">
                            <i class="icon-arrow-up-circle">

                            </i>
                            <small>{{trans('wallet.topup')}}</small>
                        </a>
                    </li>
                </ul><!-- small menu  -->
            </div>
            
            <div class="cl bg-light  dashboard-menu">
                <ul class="table-nav pt-15 pb-15">
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="text-primary">
                                0
                            </i>
                            <small>{{trans('user_admin.product_favorites')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="text-primary">
                                0
                            </i>
                            <small>{{trans('user_admin.store_favorites')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="text-primary">
                                0
                            </i>
                            <small>{{trans('user_admin.blog_stories')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/login') }}">
                            <i class="text-primary">
                                0
                            </i>
                            <small>{{trans('user_admin.history')}}</small>
                        </a>
                    </li>
                </ul><!-- small menu  -->
            </div>
            
            <!--- small menu  -->
            <div class="cl bg-light border-top dashboard-menu mt-1">
                <ul class="table-nav pt-15 pb-15">
                    <li>
                        <a href="{{url('/')}}/login" class="pb-2">
                            <i class="holder zmdi zmdi-face text-success zmdi-hc-lg">
                            </i>
                            <small>{{trans('user_admin.ticket')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/login" class="pb-2">
                            <i class="holder zmdi zmdi-instagram text-primary zmdi-hc-lg">
                            </i>
                            <small>{{trans('user_admin.instagram_manager')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/explore" class="pb-2">
                            <i class="holder zmdi zmdi-search-in-file text-info zmdi-hc-lg">
                            </i>
                            <small>{{trans('all.explore')}}</small>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/')}}/login" class="pb-2">
                            <i class="holder zmdi zmdi-settings text-warning zmdi-hc-lg">
                            </i>
                            <small>{{trans('user_admin.setting')}}</small>
                        </a>
                    </li>
                </ul><!-- small menu  -->
            </div>

        </section><!-- main articles -->
    </div><!-- /.container -->
</main>
@endsection
