@extends('layouts.app', ['nav' => 'no_top_nav', 'footer' => 'no_footer','title' => title_generator(['page' => trans('auth.login & register')])])

@section('content')
        <!-- content -->
<main id="login-page" class="cl">

    <div class="left-bar holder">
        <!-- content -->
        <div class="login-wrap login-steps card card-trans" id="user-login">
            <a href="{{url('/')}}" class="logo-wrap" title="{{trans('all.googsell')}}">

            @if(App::getLocale() == '' || App::getLocale() == 'fa')
                <img src="{{ my_asset() }}/img/colored-logo.svg" alt="{{trans('all.googsell')}}">
            @else
                <img src="{{ my_asset() }}/img/enlogo.svg" alt="{{trans('all.googsell')}}">
            @endif
             {{-- <img src="{{ my_asset() }}/img/logo-typography{{\Config::get('constants.'.App::getLocale())}}.png" alt="{{trans('all.googsell')}}"> --}}
            </a>

            <!-- instagram login -->
            <div class="form-group cl">
                <a rel="nofollow" href="{{ url('/loginWithInstagram') }}" class="btn btn-lg btn-instagram btn-block btn-reverse open-modal" data-redirect="true" data-target="#connect-instagram" title="{{trans('auth.btn_instagram')}}"><i class="instagram"></i>{{trans('auth.btn_instagram')}}</a>
                <input name="remember-instagram" checked="checked" type="checkbox" id="remember-me-instagram" class="hide">
                <label for="remember-me-instagram" class="no-padding checkbox-label success pull-right text-xs margin-top-xs">{{trans('auth.remember_me')}}</label>
                <h3 class="text-xs margin-top-sm text-link pull-left"><a href="{{url('/')}}/legal/privacy#instagram_privay" title="{{trans('auth.insta_security')}}">{{trans('auth.insta_security')}}</a></h3>
            </div>
            <!-- / instagram login -->

            <!-- email login -->

            <form action="{{url('/')}}/checkEmailForLogin" method="post" class="form ajax-form" role="form" id="email-login-step1" formLoading target="#user-register-with-email" targetOnSuccess="#user-login-with-email">
                <h1 class="text-md text-default margin-top-lg  margin-bottom-sm">{{trans('auth.login & register via email or mobile')}}</h1>

                <div class="form-group {{ (isset($errors))? $errors->has('email') ? ' has-error' : '' : '' }}">
                    <div class="input-group-icon holder input">
                        <label for="email" class="control-label required">{{trans('auth.email_or_mobile')}}</label>
                        <span class="absolute-label icon-label"><i class="zmdi zmdi-smartphone-iphone"></i></span>
                        <input type="text" name="email" id="email" value="{{ old('email') }}"
                               class="form-control input-md btn-icon input-ltr"

                               placeholder="09123456789">
                    </div>
                    @if (isset($errors))
                        @if ($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                    @endif
                </div>
                <button type="submit" class="btn btn-success btn-cons btn-round pull-left btn-icon-absolute"> <i class="zmdi zmdi-account"></i> {{trans('auth.login')}}</button>
            </form>
            <!-- / email login -->

            <!-- social login -->
            <!-- <div class="social-login margin-top-lg text-center text-light">
              <h3 class="text-sm">{{trans('auth.login via other social')}}</h3>
              <ul class="table-nav">
                <li><a href="" class="btn-icon-round twitter"><i class="zmdi zmdi-twitter"></i></a></li>
                <li><a href="" class="btn-icon-round facebook"><i class="zmdi zmdi-facebook"></i></a></li>
                <li><a href="" class="btn-icon-round googleplus"><i class="zmdi zmdi-google"></i></a></li>
              </ul>
            </div> -->
        </div>

        <!-- login if user exist -->
        <div class="card card-trans collapse login-steps" id="user-login-with-email">

            <!-- avatar  -->
            <div class="avatar-md rounded-image margin-top-sm margin-bottom-md">
                <img src="{{url('/')}}/build/img/avatar.jpg" alt="{{trans('auth.profile_picture')}}">
            </div>
            <!-- /.avatar -->

            <h3 class="text-xlg margin-bottom-md text-center user-name">{{trans('auth.username')}}</h3>
            <a href="#" id="login-with-other" class="center-link text-link text-sm back-to-login" title="{{trans('auth.login other account')}}"> <i class="zmdi zmdi-arrow-right"></i> {{trans('auth.login other account')}}</a>

            <!-- password -->
            <form action="{{ url('/') }}/loginWithEmail" method="post" class="form ajax-form" role="form"
                  id="email-login-step2">
                <div class="form-group no-side-margin">
                    <div class="input-group-icon holder input">
                        <label for="password-login" class="control-label">{{trans('auth.password')}}</label>
                        <input type="password" name="password" id="password-login"
                               class="form-control input-md btn-icon" placeholder="">
                        <span class="absolute-label icon-label"><i class="zmdi zmdi-key"></i></span>
                    </div>
                    <p class="margin-top-xs cl">
                        <input name="remember" type="checkbox" checked="checked" id="remember-me-login" class="hide">
                        <label for="remember-me-login"
                               class="control-label no-padding checkbox-label success pull-right text-sm">
                            {{trans('auth.remember_me')}}</label>
                        <a href="#" data-target="#forget-password" class="open-step pull-left text-sm text-link" title="{{trans('auth.forget_password')}}">{{trans('auth.forget_password')}}</a>
                    </p>
                </div>
                <input type="hidden" name="email">
                <button class="btn btn-success btn-cons btn-round pull-left btn-icon-absolute"> <i class="zmdi zmdi-account"></i> {{trans('auth.login')}}</button>
            </form>
            <!-- / email login -->

            <!-- social login -->
            <!-- <div class="social-login margin-top-lg text-center text-light">
              <h3 class="text-sm">ورود با شبکه های اجتماعی</h3>
              <ul class="table-nav">
                <li><a href="" class="btn-icon-round twitter"><i class="zmdi zmdi-twitter"></i></a></li>
                <li><a href="" class="btn-icon-round facebook"><i class="zmdi zmdi-facebook"></i></a></li>
                <li><a href="" class="btn-icon-round googleplus"><i class="zmdi zmdi-google"></i></a></li>
              </ul>
            </div> -->
        </div>

        <!-- register when email doesnt exist -->
        <div class="card card-trans collapse login-steps large-padding" id="user-register-with-email">

            <form action="{{ url('/') }}/registerWithEmail" method="post" class="form ajax-form" role="form"
                  id="email-register" formLoading target="#user-registered-with-email">
                <!-- avatar  -->
                <a href="{{url('/')}}" class="logo-wrap margin-top-sm" title="{{trans('all.googsell')}}">
                    <img src="{{ my_asset() }}/img/colored-logo.svg" alt="{{trans('all.googsell')}}">
                </a>
                <!-- /.avatar -->

                <!-- first-name  -->
                <div class="input-group-icon form-group holder input">
                    <label for="first_name" class="control-label required">{{trans('auth.first_name')}}</label>
                    <input type="first_name"  name="first_name" id="first_name"
                           class="form-control input-md btn-icon"
                           placeholder="{{trans('auth.persian_english')}}">
                    <span class="absolute-label icon-label"><i class="zmdi zmdi-account"></i></span>
                </div>
                <!-- first-name  -->

                {{-- <!-- last name -->
                <div class="input-group-icon form-group holder input">
                    <label for="last_name" class="control-label required">{{trans('auth.last_name')}}</label>
                    <input type="last_name"  id="last_name" name="last_name" class="form-control input-md btn-icon"
                           placeholder="{{trans('auth.persian_english')}}">
                    <span class="absolute-label icon-label"><i class="zmdi zmdi-account"></i></span>
                </div>
                <!-- last name --> --}}

                <!-- password -->
                <div class="input-group margin-top-md holder input">
                    <label for="password" class="control-label">{{trans('auth.password')}}</label>
                    <input type="password" required autocomplete="off" id="password" name="password" data-target="#password-security-tips"
                           class="password-checker form-control input-md input-ltr" placeholder="">
                    <span class="input-group-addon text-light text-lg input-md password-validator"><i
                                class="zmdi-key zmdi"></i></span>
                </div>

                <div class="tips-panel collapse" id="password-security-tips">
                    {{trans('auth.password_security')}}
                </div>
                <p class="margin-top-xs cl margin-bottom-md">
                    <input type="checkbox" checked="checked" id="remember-me-register" class="hide">
                    <label for="remember-me-register" name="remember"
                           class="control-label checkbox-label no-padding success pull-right text-sm"> {{trans('auth.remember_me')}}</label>
                </p>
                <input type="hidden" name="email">
                <button type="reset" class="btn btn-link text-link back-to-login "> {{trans('all.back')}}</button>
                <button type="submit" class="btn btn-success btn-cons pull-left btn-round btn-icon-absolute"><i class="zmdi zmdi-account-add"></i> {{trans('auth.register')}}</button>
            </form>
            <!-- / email login -->

        </div>

        <!-- forget password -->
        <div class="card card-trans collapse login-steps large-padding" id="forget-password">

            <form action="{{ url('/') }}/password/email" method="post" class="form ajax-form" role="form"
                  id="email-register" formLoading target="#forget-pass-2">
                  {!! csrf_field() !!}
                <!-- avatar  -->
                <a href="{{url('/')}}" class="logo-wrap margin-top-sm" title="{{trans('all.googsell')}}">
                    <img src='{{ my_asset() }}/img/colored-logo.svg' alt="{{trans('all.googsell')}}">
                </a>
                <!-- /.avatar -->
                <h3 class="text-xlg margin-bottom-md text-center">{{trans('auth.forget_password')}}</h3>
                <a href="#" data-target='#user-login' class="open-step margin-bottom-md center-link text-link text-sm">{{trans('auth.login other account')}}</a>

                <!-- email -->
                <div class="input-group-icon form-group holder input">
                    <label for="email" class="control-label required">{{trans('auth.email')}}</label>
                    <input type="email"  id="email" name="email" class="form-control input-md btn-icon"
                           placeholder="{{trans('auth.email')}}">
                    <span class="absolute-label icon-label"><i class="zmdi zmdi-account"></i></span>
                </div>
                <!-- email -->
                <button type="submit" class="btn btn-success btn-cons"><i class="zmdi zmid-signin"></i> {{trans('all.send')}}</button>
            </form>
            <!-- / email login -->

        </div>

        <!-- email sent -->
        <div class="card card-trans collapse login-steps large-padding" targetOnSuccess="#forget-password-email-sent" id="forget-password-email-sent">

            <a href="{{url('/')}}" class="logo-wrap margin-top-sm" title="{{trans('all.googsell')}}">
                <img src='{{ my_asset() }}/img/colored-logo.svg' alt="{{trans('all.googsell')}}">
            </a>
            <h3 class="text-xlg margin-bottom-md text-center text-success"><i class="zmdi zmdi-check"></i>    {{trans('all.sent')}}</h3>
            <p class="text-center text-content-light text-xs">{{trans('auth.email_sent')}}</p>
            <div class="center-box margin-top-lg">
                <a href="url('/')" data-target='#user-login' class="open-step btn btn-success btn-cons"><i class="zmdi zmdi-account"></i> {{trans('auth.login other account')}}</a>
                <a href="url('/')" class="btn btn-default "><i class="zmdi zmdi-home"></i> {{trans('all.home')}}</a>

            </div>
            <!-- / email login -->

        </div>
        <!-- user registered successfully -->
        <div class="card card-trans collapse login-steps large-padding" id="user-registered-with-email">

            <form action="{{ url('/') }}/loginWithInstagram" method="get" class="form ajax-form" role="form"
                  id="email-register" formLoading>
                <!-- avatar  -->
                <a href="{{url('/')}}" class="logo-wrap margin-top-sm" title="{{trans('all.googsell')}}">
                    <img src="{{ my_asset() }}/img/colored-logo.svg" alt="{{trans('all.googsell')}}">
                </a>
                <!-- /.avatar -->

                <div class="text-bold text-lg no-side-margin text-center">{{trans('auth.register_done')}}</div>

                <a href="{{url('/')}}" class="text-link margin-bottom-lg btn-center" title="{{trans('auth.go_home')}}">{{trans('auth.go_home')}}</a>

                <!-- instagram login -->
                <div class="form-group">
                    <a href="{{ url('/loginWithInstagram') }}" class="btn btn-md btn-instagram btn-block btn-reverse open-modal" data-redirect="true" data-target="#connect-instagram" title="{{trans('auth.add_instagram')}}"><i class="instagram"></i>{{trans('auth.add_instagram')}}</a>

                    <h3 class="text-center text-xs small-margin text-link"><a href="#" title="{{trans('auth.instagram_adv')}}">{{trans('auth.instagram_adv')}}</a></h3>
                </div>
                <!-- / instagram login -->

            </form>


            <!-- / user registered successfully form -->

        </div>
    </div>

    <div class="right-bar holder cover-bg">
        <div class="stick-bottom-right large-margin content">
            <p class="text-bold margin-bottom-sm text-xl text-light">{{trans('auth.why_instagram')}}<br></p>
            <p class="text-light">{!!trans('auth.why_instagram_desc')!!}</p>
            <p class="text-light login-bottom-links margin-top-sm">
                <a href="{{url('/')}}" title="{{trans('all.home')}}">{{trans('all.home')}}</a> - <a href="{{url('/')}}/content/about-us" title="{{trans('all.about_us')}}">{{trans('all.about_us')}}</a> - <a href="{{url('/')}}/legal/privacy" title="{{trans('all.privacy')}}">{{trans('all.privacy')}}</a> - <a href="{{url('/')}}/help" title="{{trans('all.help')}}">{{trans('all.help')}}</a></p>
        </div>
    </div>

</main>
@endsection

@section('style')
    <style>
        body, html {
            height: 100%;
        }
    </style>
@endsection

@section ('script')
    <script>
        // $('.right-bar').append('<div style=""><iframe src="https://instagram.com/accounts/logout/" width="0" height="0"></iframe></div>');
        // checking password for registe page
        var message = '{{ @session('message') }}';
        if(message.length)
            nofification(message, "success");
        $('#remember-me-instagram').on('click', function(){
            if(this.checked)
                createCookie('rm_insta', 'true', 100);
            else
                createCookie('rm_insta', 'false', 0);
        });

        $('.back-to-login').click(function () {
            $("#user-login-with-email").hide();
            $("#user-register-with-email").hide();
            $("#user-login").show();
            $("#user-login form").show();
        });

        $('.open-step').on('click', function(event) {
            event.preventDefault();
            var target = $(this).data('target');
            $(".login-steps").hide();
            $(target).show();
        });

        function go_for_register(data) {
            if ('OK' == data.status) {
                data.targetForm.find("input[name='email']").val(data.thisForm.find("input[name='email']").val());
            }
        }

        function go_for_login_step2(data) {
            if ('OK' == data.status) {
                data.targetForm.find("input[name='email']").val(data.thisForm.find("input[name='email']").val())
                data.targetContainer.find(".user-name").html((undefined != data.result.fullName) ? data.result.fullName : 'کاربر');
                data.targetContainer.find(".avatar-md img").prop('src',data.result.profile_picture_url);
                data.targetContainer.find(".avatar-md img").prop('src',data.result.profile_picture_url);
            }
        }

        function forget_password_send_email(data) {
            if ('OK' == data.status)
                nofification("{{trans('auth.check_email')}}", "success")
            else
                nofification("{{trans('auth.recheck_email')}}", "danger")
            //     alert();
            //     // $('#password-login').parent().addClass('has-error').append('<p class="help-block">{{trans("auth.wrong_pass")}}</p>');

            // }
        }
        function reset_password(data) {
            if ('OK' == data.status)
                nofification("{{trans('auth.check_email')}}", "success")
            else
            {
                nofification(data.message, "warning")
                $('.loading-card').removeClass('loading-card')
            }
            //     alert();
            //     // $('#password-login').parent().addClass('has-error').append('<p class="help-block">{{trans("auth.wrong_pass")}}</p>');

            // }
        }

    </script>

@endsection
