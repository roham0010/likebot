<style>
            @font-face {
		    font-family: IranSansWeb;
		    src: url("http://www.googsell.com/build/css/font/IRANSansWeb(FaNum)_Light.eot");
		    src: url("http://www.googsell.com/build/css/font/IRANSansWeb(FaNum)_Light.eot") format("embedded-opentype"), url(.), url('http://www.googsell.com/build/css/font/IRANSansWeb(FaNum)_Light.ttf') format('truetype'), url('http://www.googsell.com/build/css/font/IRANSansWeb(FaNum)_Light.woff') format("woff");
		    font-weight: 400;
		    font-style: normal
		}

		@font-face {
		    font-family: IranSansWeb;
		    src: url(.), url("http://www.googsell.com/build/css/font/IRANSansWeb(FaNum)_Medium.eot");
		    src: url(.), url("http://www.googsell.com/build/css/font/IRANSansWeb(FaNum)_Medium.eot") format("embedded-opentype"), url("http://www.googsell.com/build/css/font/IRANSansWeb(FaNum)_Medium.woff") format("woff"), url(.), url("http://www.googsell.com/build/css/font/IRANSansWeb(FaNum)_Medium.ttf") format("truetype");
		    font-weight: 700;
		    font-style: normal
		}
        </style>
        <div style="direction: rtl; background: #eee; font-family: 'IRANSansWeb(FaNum) Light';">
            <div style="display: table; margin: 0 auto; width: 80%">
                <!-- <a href="http://www.googsell.com" style="text-decoration: none; color: #666">
                    <svg height="50" version="1.1" viewbox="240 220 30 60" width="30" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="icomoon-ignore">
                        </g>
                        <path d="M270 268h-2v-4c0-1.104-0.894-2-2-2s-2 0.896-2 2v4h-16v-4c0-1.104-0.894-2-2-2s-2 0.896-2 2v4h-2c-1.106 0-2 0.896-2 2s0.894 2 2 2h28c1.106 0 2-0.896 2-2s-0.894-2-2-2zM270 240h-28c-1.106 0-2 0.895-2 2v12c0 2.209 1.791 4 4 4s4-1.791 4-4c0 2.209 1.791 4 4 4s4-1.791 4-4c0 2.209 1.791 4 4 4s4-1.791 4-4c0 2.209 1.791 4 4 4s4-1.791 4-4v-12c0-1.105-0.894-2-2-2z" fill="#000">
                        </path>
                    </svg>
                    گوگسل
                </a> -->
                <div style="background: white; 
                    box-shadow: 0 1px 4px -1px #999;
                    padding: 0;
                    border-radius: 3px;
                    margin: 40px 0;
                    overflow: hidden;
				    box-sizing: border-box;
				    margin-top: 40px;">
                <h2 style="background: #51a6c2; margin-top: 0; padding-top: 10px; color: white; height: 50px; text-align: center; font-family: 'Brush Script MT'">Googsell</h2>
                	<div style="padding: 0 20px;">
	                	<h3>سلام کاربر گرامی!</h3>
	                    <p style="margin: 0; margin-bottom: 10px; font-size: 14px; ">
	                        شخصی در سایت گوگسل برای شما درخواست فراموشی گذرواژه داده است، در صورتی که گذرواژه خود را فراموش کرده اید و نیاز به تغییر آن دارید روی لینک زیر کلیک نمایید.
	                        {{-- یک نفر درخواست پسورد جدید برای اکانت گوگسل شما را کرده است. با کلیک بر روی لینک زیر می توانید پسورد خود را عوض کنید. --}}
	                    </p>
	                    <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}" style="
													 display: table;
													 margin: 20px auto;
													 font-weight: normal;
													 text-align: center;
													 vertical-align: middle;
													 touch-action: manipulation;
													 cursor: pointer;
													 background-image: none;
													 border: 1px solid transparent;
													 white-space: nowrap;
													 padding: 8px 30px;
													 font-size: 16px;
													 line-height: 1.42857143;
													 border-radius: 3px;
													 height: 27px;
													 -webkit-user-select: none;
													 -moz-user-select: none;
													 -ms-user-select: none;
													 user-select: none;
													 color: #fff;
													background-color: #03A678;
													border-color: #038d66;
												    text-decoration: none;
												    box-sizing: border-box;
								 " title="reset your password">
	                         بازیابی گذرواژه
	                    </a>
	                   <p style="font-size: 14px;padding-bottom:30px;"> اگر این درخواست از سوی شما ارسال نشده، از این ایمیل صرف نظر نمایید.</p>
                   </div>
                </div>
            </div>
        </div>
