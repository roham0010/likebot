@extends('layouts.app', ['nav' => 'no_top_nav', 'footer' => 'no_footer','title' => title_generator(['page' => trans('auth.reset_pass')])])

@section('content')
        <!-- content -->
<main id="login-page" class="cl">

    <div class="left-bar holder">
        

        <!-- register when email doesnt exist -->
        <div class="card login-steps large-padding" id="user-register-with-email">

            <form action="{{ url('/password/reset') }}" method="post" class="form" role="form">
                  {!! csrf_field() !!}
                <!-- avatar  -->
                <a href="{{url('/')}}" class="logo-wrap margin-top-sm" title="{{trans('all.googsell')}}">
                    <img src="{{ my_asset() }}/img/logo-typography{{\Config::get('constants.'.App::getLocale())}}.png" alt="{{trans('all.googsell')}}">
                </a>
                <!-- /.avatar -->

                <!-- password -->
                <div class="input-group margin-top-md holder input">
                    <label for="password" class="control-label">{{trans('auth.password')}}</label>
                    <input type="password" required autocomplete="off" id="password" name="password" data-target="#password-security-tips"
                           class="password-checker form-control input-md input-ltr">
                    <span class="input-group-addon text-light text-lg input-md password-validator"><i
                                class="zmdi-key zmdi"></i></span>
                </div>

                <div class="tips-panel collapse" id="password-security-tips">
                    {{trans('auth.password_security')}}
                </div>
                <!-- repassword -->
                <div class="input-group margin-top-md holder input margin-bottom-md">
                    <label for="repassword" class="control-label">{{trans('auth.repassword')}}</label>
                    <input type="repassword" required autocomplete="off" id="repassword" name="repassword" 
                           class=" form-control input-md input-ltr" >
                    {{-- <span class="input-group-addon text-light text-lg input-md repassword-validator"><i class="zmdi-key zmdi"></i></span> --}}
                </div>

                <button type="submit" class="btn btn-success btn-cons"><i class="zmdi zmid-signin"></i> {{trans('auth.reset_pass')}}</button>
                <a href="{{url('/')}}" type="reset" class="btn btn-link text-link back-to-login"> {{trans('all.back')}}</a>
            </form>
            <!-- / email login -->

        </div>
    </div>

    <div class="right-bar holder cover-bg">
        <div class="stick-bottom-right large-margin content">
            <h1 class="text-xl text-light">{{trans('auth.why_instagram')}} <br> </h1>

            <p class="text-light">{!!trans('auth.why_instagram_desc')!!}</p>
            <p class="text-light login-bottom-links margin-top-sm">
                <a href="{{url('/')}}" title="{{trans('all.home')}}">{{trans('all.home')}}</a> - <a href="{{url('/')}}/content/about-us" title="{{trans('all.about_us')}}">{{trans('all.about_us')}}</a> - <a href="{{url('/')}}/legal/privacy" title="{{trans('all.privacy')}}">{{trans('all.privacy')}}</a> - <a href="{{url('/')}}/help" title="{{trans('all.help')}}">{{trans('all.help')}}</a></p>
        </div>
    </div>

</main>


<div class="container hide">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {!! csrf_field() !!}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i>Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
    <style>
        body, html {
            height: 100%;
        }
    </style>
@endsection

@section ('script')
    <script>
        // checking password for registe page
        $('.password-checker').keyup(function () {
            checkStrength($(this).val());
        });


        $(".password-checker").on('focusout', function (event) {
            event.preventDefault();
            var target = $($(this).data('target'));
            target.slideUp();
        }).on('focusin', function (event) {
            event.preventDefault();
            var target = $($(this).data('target'));
            target.slideDown();
        });


        // password checker

        /*
         assigning keyup event to password field
         so everytime user type code will execute
         */


        /*
         checkStrength is function which will do the
         main password strength checking for us
         */

        function checkStrength(password) {
            //initial strength
            var strength = 0;
            var passValidatior = $('.password-validator');

            //if the password length is less than 6, return message.
            if (password.length < 6) {
                passValidatior.removeClass('label-success label-warning').addClass('label-danger')
                        .find('i').removeClass().addClass('zmdi zmdi-alert-triangle');
                return 'Too short'
            }

            //length is ok, lets continue.

            //if length is 8 characters or more, increase strength value
            if (password.length > 7) strength += 1

            //if password contains both lower and uppercase characters, increase strength value
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1

            //if it has numbers and characters, increase strength value
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1

            //if it has one special character, increase strength value
            // if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1

            //if it has two special characters, increase strength value
            // if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1

            //now we have calculated strength value, we can return messages

            //if value is less than 2
            if (strength < 1) {
                passValidatior.removeClass('label-success label-warning').addClass('label-danger')
                        .find('i').removeClass().addClass('zmdi zmdi-alert-octagon');
                return 'Weak'
            }
            else if (strength == 1) {
                passValidatior.removeClass('label-success label-danger').addClass('label-warning')
                        .find('i').removeClass().addClass('zmdi zmdi-alert-circle');
                return 'Good'
            }
            else {
                passValidatior.removeClass('label-danger label-warning').addClass('label-success')
                        .find('i').removeClass().addClass('zmdi zmdi-check');
                return 'Strong'
            }
        }
    </script>

@endsection


