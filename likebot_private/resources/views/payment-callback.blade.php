@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row">
            <div class="main_home">
                <div class="col-md-12">
                    @if($resultFlag == 'success')
                        {{ $message }}
                    @elseif($resultFlag == 'refresh')
                        {{ $message }}
                    @elseif($resultFlag == 'bank_error')
                        خطای بانک: {{ $message }}
                    @else
                        {{ $message }}
                    @endif
                    کد پیگیری: {{ @$trackingCode }}
                    <a href="{{ myRoute('profile.payments.payments') }}">برو به سفارشات</a>
                </div>
            </div>
        </div><!--End off row-->
    </div><!--End off container -->
@endsection
