<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed' => '',
    'throttle' => '',
    'btn_instagram' => 'ورود با اینستاگرام',
    'insta_security' => 'امنیت من در اینستاگرام',
    'login & register via email' => 'ورود و ثبت نام با ایمیل',
    'login & register via email or mobile' => 'شروع کن',
    'login & register' => 'ورود و ثبت نام',
    'login via other social' => 'ورود با شبکه های اجتماعی دیگر',
    'login other account' => 'ورود با اکانت دیگر',
    'email_sent' => 'ایمیلی حاوی یک لینک به ایمیل شما ارسال شد لطفا ایمیلتان را چک کنید!',
    'email' => 'ایمیل',
    'email_or_mobile' => 'شماره همراه',
    'repassword' => 'تکرار گذرواژه',
    'reset_pass' => 'بازیابی گذرواژه',
    'your_email' => 'ایمیل شما',
    'password' => 'گذرواژه',
    'login' => 'ورود',
    'register' => 'ثبت نام',
    'why_instagram' => 'چرا با اینستاگرام وارد شوم؟',
    'why_instagram_desc' => 'با ورود از طریق اینستاگرام کالاها لایک شده در اینستاگرام را مستقیما از طریق گوگسل <br> خریداری کرده، فروشگاه های مورد علاقه خود را دنبال کنید، لایک کنید و کامنت بگذارید.',
    'remember_me' => 'مرا به خاطر بسپار',
    'forget_password' => 'فراموشی گذرواژه',
    'first_name' => 'نام',
    'your_first_name' => 'نام شما',
    'last_name' => 'نام خانوادگی',
    'persian_english' => 'فارسی یا انگلیسی',
    'password_security' => 'گذرواژه شما باید حداقل 8 حرف و دارای عدد و حروف باشد.',
    'register_done' => 'ثبت نام شما انجام شد!',
    'go_home' => 'رفتن به خانه',
    'add_instagram' => 'افزودن حساب اینستاگرام',
    'instagram_adv' => 'مزایای افزودن حساب اینستاگرام چیست؟',
    'first_info' => 'اطلاعات کاربری',
    'contact_info' => 'اطلاعات تماس',
    'about_me' => 'بیوگرافی یا شعار',
    'about_me_persian' => 'بیوگرافی یا شعار (به فارسی و بدون شماره تلفن، کانال تلگرام و وبسایت)',
    'store_title' => 'نام فروشگاه',
    'store_title_persian' => 'نام فروشگاه (ترجیحا به فارسی)',
    'customize_desc' => ' کالاهایم همگی سفارشی اند.',
    'returnable_desc' => 'کالاهایم غیر قابل برگشت اند.',
    "category_desc" => "دسته کالاهای خود را انتخاب کنید.",
    'next_step' => 'گام بعدی',
    'main_address' => 'آدرس دقیق(فروشگاه یا منزل)',
    'main_address_placeholder' => 'مثال:  شاهد 66/6، پلاک 110، واحد 9',
    'phone' => 'شماره همراه',
    'phone_placeholder' => 'مثال: 09301234567',
    'website' => 'وبسایت',
    'social_networks' => 'شبکه های اجتماعی',
    'telegram' => 'کانال تلگرام',
    'bot' => 'کد ربات تلگرام',
    'twitter' => 'توییتر',
    'facebook' => 'فیسبوک',
    'google_plus' => 'گوگل پلاس',
    'login_with_instagram_desc' => 'برای ارتقای اکانت باید ابتدا با <strong>اینستاگرام</strong> خود جهت تاییدسازی فروشگاه ثبت نام کنید!',
    'login_with_instagram_desc2' => 'بعد از ورود مرحله بعدی تاییدسازی فروشگاه انجام خواهد شد',
    'choose_one' => 'یکی از موارد را انتخاب کنید.',
    'wrong_pass' => 'گذرواژه اشتباه است.',
    'username' => 'نام کاربری.',
    'profile_picture' => 'عکس پروفایل کاربر.',
    'check_email' => 'ایمیلتان را چک کنید',
    'recheck_email' => 'لطفا دوباره تلاش کنید درستی ایمیل را چک کنید!',

    'welcome' => 'ورود و نام نویسی',
    'welcome_btn' => 'شروع',

    'steps' => [
        '1' => 'ورود با اینستاگرام',
        '2' => 'اطلاعات کاربری',
        '3' => 'اطلاعات تماس',
        '4' => 'تایید کالاها',
    ],
    'getTrust' => [
        'title' => 'راهنمای دریافت <span class="text-success">نماد اعتماد گوگسل</span>',
        'desc' => 'گوگسل بر اساس ملاکهای مختلف، فروشگاهها را بررسی می کند، چنانچه فروشگاه شما توسط تیم بررسی گوگسل معتبر شناخته شود به شما نشان اعتماد و اعتبار گوگسلی تعلق خواهد گرفت تا بتوانید برای جلب مشتریان بیشتر آن را به کار ببرید.',
        'notes' => 'نکات: ',
        'tips' => '<p class="text-md mt-0 mb-0">به شما پیشنهاد می کنیم نکات زیر را رعایت کرده تا بتوانید بدون هیچگونه مشکلی نماد اعتماد را دریافت کنید.</p>
            <p class="text-md mt-0"> <i class="zmdi zmdi-check text-content-light ml-1"></i> تایید فروشگاه خود با نام <span class="text-bold">فارسی</span></p>
            <p class="text-md mt-0"> <i class="zmdi zmdi-check text-content-light ml-1"></i> نوشتن بیو یا شعار به صورت فارسی و بدون لینک، اکانت شبکه های اجتماعی دیگر، شماره موبایل و ...</p>
            <p class="text-md mt-0"> <i class="zmdi zmdi-check text-content-light ml-1"></i> پر کردن دقیق اطلاعات شامل آدرس، شماره تلفن ها و شبکه های اجتماعی دیگر</p>
            <p class="text-md mt-0"> <i class="zmdi zmdi-check text-content-light ml-1"></i> تایید همه کالاهای موجود در فروشگاه شما در فروشگاه گوگسلی خود</p>
            <p class="text-md mt-0"> <i class="zmdi zmdi-check text-content-light ml-1"></i> فرستادن کپی کارت ملی از طریق این صفحه یا به تلگرام تیم پشتیبانی گوگسل</p>
            <p class="text-md mt-0"> <i class="zmdi zmdi-check text-content-light ml-1"></i> در صورت داشتن مجوز، فرستادن آن از طریق این برگه یا فرستادن به چت پشتیبانی یا تلگرام تیم پشتیبانی گوگسل</p>
            <p class="text-md mt-0"> <i class="zmdi zmdi-check text-content-light ml-1"></i>  فرستادن هرگونه مدرک دیگر </p>',
    ],
    'emenu' => [
        'register' => 'اولین منوی الکترونیک برای کافیشاپ ها, رستوران ها, هتل ها',
    ],

];
