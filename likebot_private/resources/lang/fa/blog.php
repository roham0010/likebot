<?php

return [

    /*
    |--------------------------------------------------------------------------
    | blog pages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'title' => 'بلاگ گوگسل',
    'title_f' => 'بلاگ',
    'Subscription_text_desc' => 'مطالب وبلاگ گوگسل را در <a href="https://twitter.com/googsell" class="text-twitter"><i class="zmdi zmdi-twitter zmdi-hc-lg"></i></a> <a href="https://t.me/googsell" class="text-telegram"><i class="zmdi zmdi-mail-send zmdi-hc-lg"></i></a>  <a href="https://instagram.com/googblog" class="text-instagram"><i class="zmdi zmdi-instagram zmdi-hc-lg"></i></a> خود دنبال کنید',
    'Subscription_input' => 'ایمیل خود را وارد کنید',
    'search' => 'اشتراک',


    'comment_header_title' => 'اینم یک خطی',
    /// blog.write_comment_title
    'write_comment_title' => 'نظر دهید',
    ///textarea_text 
    'textarea_text' => 'نظر شما بررسی خواهد شد. لطفا در نوشتن آن دقت فرمایید.',
    'user_name_text' => 'نام خود را کامل بنویسید.',
    'user_name_label' => 'نام',
    'user_email_label' => 'ایمیل',
    'user_email_text' => 'email@example.com',
    'textarea_label' => 'متن',
      // related_content
    'related_content_title' => 'مطالب مرتبط',
    'related_content_btnmore' => 'مشاهد همه',
    'cats' =>[
        '1' => 'راهنمای خرید',
        '2' => 'فروش در اینستاگرام',
        '3' => 'فروشگاههای خاص',
        '4' => 'آموزشی',
        '5' => 'بهترین کادوها',
        '6' => 'متفرقه',
        ],

    'latest_posts' => "آخرین نوشته های وبلاگ",

]; 

?>
