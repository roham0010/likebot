<?php
return [
    'discounts' => [
        1 => ['price' => 0, 'likes' => 100, 'description' => ''],
        2 => ['price' => 102, 'likes' => 20000, 'description' => 'تخفیف روی قیمت و تعداد لایک زیاد'],
        3 => ['price' => 0, 'likes' => 0, 'description' => ''],
        4 => ['price' => 0, 'likes' => 400000, 'description' => 'تعداد لایک زیاد'],
        5 => ['price' => 500, 'likes' => 500000, 'description' => 'تخفیف قیمت'],
    ],
    'plans' => [
        1 => ['price' => 101, 'likes' => 5000, 'discount_id' => 1],
        2 => ['price' => 21000, 'likes' => 10000, 'discount_id' => 2],
        3 => ['price' => 49000, 'likes' => 25000, 'discount_id' => 3],
        4 => ['price' => 89000, 'likes' => 50000, 'discount_id' => 4],
        5 => ['price' => 105, 'likes' => 100000, 'discount_id' => 5],
    ],
];
