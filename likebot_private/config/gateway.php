<?php

return [

    //-------------------------------
    // Timezone for insert dates in database
    // If you want Gateway not set timezone, just leave it empty
    //--------------------------------
    'timezone' => 'Asia/Tehran',

    //--------------------------------
    // Zarinpal gateway
    //--------------------------------
    'zarinpal' => [
        'merchant-id' => '260ceaba-10d3-11e8-a176-005056a205be',
        'type' => 'zarin-gate', // Types: [zarin-gate || normal]
        'callback-url' => '/',
        'server' => 'iran', // Servers: [germany || iran || test]
        'email' => 'hassan_shojaei38@yahoo.com',
        'mobile' => '09388391302',
        'description' => 'خرید',
    ],

    //--------------------------------
    // Mellat gateway
    //--------------------------------
    'mellat' => [
        'username' => '',
        'password' => '',
        'terminalId' => 0000000,
        'callback-url' => '/',
    ],

    //--------------------------------
    // Saman gateway
    //--------------------------------
    'saman' => [
        'merchant' => '',
        'password' => '',
        'callback-url' => '/',
    ],

    //--------------------------------
    // Payline gateway
    //--------------------------------
    'payline' => [
        'api' => 'xxxxx-xxxxx-xxxxx-xxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        'callback-url' => '/',
    ],

    //--------------------------------
    // Sadad gateway
    //--------------------------------
    'sadad' => [
        'merchant' => '',
        'transactionKey' => '',
        'terminalId' => 000000000,
        'callback-url' => '/',
    ],

    //--------------------------------
    // JahanPay gateway
    //--------------------------------
    'jahanpay' => [
        'api' => 'xxxxxxxxxxx',
        'callback-url' => '/',
    ],

    //--------------------------------
    // Parsian gateway
    //--------------------------------
    'parsian' => [
        'pin' => 'xxxxxxxxxxxxxxxxxxxx',
        'callback-url' => '/',
    ],
    //--------------------------------
    // Pasargad gateway
    //--------------------------------
    'pasargad' => [
        'terminalId' => 000000,
        'merchantId' => 000000,
        'certificate-path' => storage_path('gateway/pasargad/certificate.xml'),
        'callback-url' => '/gateway/callback/pasargad',
    ],
    //-------------------------------
    // Tables names
    //--------------------------------
    'table' => 'gateway_transactions',
];
