<?php
namespace App\Likebot\Package_extends;

use MetzWeb\Instagram\Instagram;

/**
 * Extend \vendor\cosenary\Instagram
 */
class MyInstagram extends Instagram
{
    public function __construct()
    {
        // self::parant();
    }
    const API_URL = "https://www.instagram.com/";
    protected function _myMakeCall($function, $params = null, $method = 'GET')
    {
        $authMethod = '';
        $paramString = null;
        $params['__a'] = '1';
        $query = http_build_query($params);
        $apiCall = self::API_URL . $function . "/?" . $query;
        // signed header of POST/DELETE requests
        $headerData = array('Accept: application/json');
        // if (true === $this->_signedheader && 'GET' !== $method) {
        //     $headerData[] = 'X-Insta-Forwarded-For: ' . $this->_signHeader();
        // }

        $ch = curl_init();
        // ee($apiCall);
        curl_setopt($ch, CURLOPT_URL, $apiCall);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $jsonData = curl_exec($ch);
        if (false === $jsonData) {
            throw new \Exception("Error: _makeCall() - cURL error: " . curl_error($ch));
        }
        curl_close($ch);
        return json_decode($jsonData);
    }
    public function myGetUser($username, $maxId)
    {
        // ee(file_get_contents('https://www.instagram.com/googsell?__a=1'));
        $userInfo = $this->_myMakeCall($username, ['max_id' => $maxId]);
        return $userInfo;
        ee($userInfo);
    }
    public function myGetTagPosts($tag)
    {
        // ee(file_get_contents('https://www.instagram.com/googsell?__a=1'));
        $posts = $this->_myMakeCall('explore/tags/' . $tag);
        return $posts;
    }
    public function myGetMedia($tag)
    {
        $media = $this->_myMakeCall('p/' . $tag);
        return $media;
    }
}
