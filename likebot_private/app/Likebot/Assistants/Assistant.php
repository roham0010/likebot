<?php

class Assistant
{
    /**
     * object keys of instagram
     */
    public static function objKeys($type = null)
    {
        $post = "entry_data.PostPage.0.graphql.shortcode_media";
        $comment = $post . ".edge_media_to_comment";
        $images = $post . ".display_resources";
        $user = "config.viewer";

        $stringKeys = [
            "activeVersion" => "v1",
            "v1" => [
                "extractJson" => [
                    "all" => [
                        "prefix" => "<script type=\"text/javascript\">window._sharedData =",
                        "suffix" => ";</script>",
                    ],
                    "queryId" => [
                        "prefix" => ",queryId:\"",
                        "suffix" => "\",queryParams",
                    ],
                ],
                "tags" => [
                    "nodes" => "entry_data.TagPage.0.tag.media.nodes",
                    "nodes1" => "entry_data.TagPage.0.graphql.hashtag.edge_hashtag_to_media.edges",
                    "end_cursor1" => "entry_data.TagPage.0.graphql.hashtag.edge_hashtag_to_media.page_info.end_cursor",
                    "end_cursor2" => "data.hashtag.edge_hashtag_to_media.page_info.end_cursor",
                ],
                "post" => [
                    "id" => $post . ".id",
                    "data" => $post,
                    "caption" => $post . ".edge_media_to_caption.edges.0.node.text",
                    "user" => [
                        "data" => $user,
                        "username" => $user . ".username",
                        "id" => $user . ".id",
                    ],
                    "comment" => [
                        "data" => $comment . ".edges",
                        "count" => $comment . ".count",
                    ],
                    "token" => "config.csrf_token",
                    "liked" => $post . ".viewer_has_liked",
                    "owner" => $post . ".owner",
                    "images" => [
                        "thumb" => $images . ".0.src",
                        "medium" => $images . ".1.src",
                        "link" => $images . ".2.src",
                    ],
                ],
                "objKeysAuth" => [
                    "objKeysAccessToken" => "serverAuth.serverAccessToken",
                ],
                "o_servero" => [
                    "o_liked_to_synco" => "serverStorage.setting.liked_to_sync",
                ],
            ],
        ];
        $keys = json_decode(json_encode($stringKeys));
        if (empty($type)) {
            return $keys->{$keys->activeVersion};
        }
        return $keys;
    }

    /**
     * get object keys of instagram keys
     */
    public static function getObjectKeys($object, $keys, $valueIfNotExists = '')
    {
        $keys = explode('.', $keys);
        $obj = $object;
        if (is_object($object)) {
            $obj = json_decode(json_encode($object), true);
        }
        foreach ($keys as $key) {
            if (isset($obj[$key])) {
                $obj = $obj[$key];
            } else {
                $obj = $valueIfNotExists;
                break;
            }
        }
        $obj = json_decode(json_encode($obj));
        // ee($obj);

        return $obj;
    }

    public static function multi_explode($string, $delimiters = ['،', ',', '-', '_', ';', ':', '.', ' '])
    {
        //preg_split( "/ (@|vs) /", $input );
        //preg_split("/(\/|\?|=)/", $foo);

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $outputs = explode($delimiters[0], $ready);
        $tmpOutputs = $outputs;
        foreach ($tmpOutputs as $key => $tmpOutput) {
            $outputs[$key] = trim($tmpOutput);
            if (empty($tmpOutput)) {
                unset($outputs[$key]);
            }
        }
        return $outputs;
    }
}
