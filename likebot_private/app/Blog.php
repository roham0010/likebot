<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * Get the user that owns the Blog.
     */
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'type', 'status', 'user_id', 'tag_id', 'title', 'abstract', 'full_text', 'visit_count', 'like_count', 'share_count', 'comment_count', 'tag_ids', 'tag_titles', 'product_ids', 'media_ids'
        /**
         * types
         * 1 Blog for orders
         * >=10 general Blogs in trans(Blog.types)
         */
    ];
    public function getColumns($type='fillable')
    {
        return $this->fillable;
    }
    public function users()
    {
        return $this->join('users','blogs.user_id','=','users.id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
