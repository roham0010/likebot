<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Telegram\Bot\Api;

class Telegram_post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'content_type', 'text', 'status', 'user_name', 'file_name', 'user_title', 'media_id', 'telegram_update_id', 'user_id', 'url', 'from', 'extra'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '',
    ];

    public static $bots = [
        'googsell_bot' => '312090865:AAHc-dNdBMN8fFfWVnJ-zvWtHl22zc5RAS8',
        'juddy_abbot' => '311819068:AAHrSAgRLU-g4v3xlptULAQN4yr6356tDEk',
    ];
    public static $types = [
        'all' => ['text', 'image', 'video', 'audio', 'document', 'voice', 'wallpaper', 'instafunvideo', 'craft', 'morning'],
        'video' => ['instafunvideo', 'video', 'craft'],
        'multimedia' => ['instafunvideo', 'video', 'craft', 'image', 'wallpaper', ''],
    ];
    public static $mimeTypes = [
        'video' => 'video/mp4',
        'gif' => 'video/mp4',
        'pdf' => 'application/pdf',
        'audio' => 'audio/mpeg',
        'voice' => 'application/pdf',
    ];

    public function getColumns($type = 'fillable')
    {
        return $this->fillable;
    }
    public static function getBots($bot = null)
    {
        if (is_null($bot)) {
            return self::$bots;
        }
        return self::$bots[$bot];

    }
    public static function getTypes($type = null)
    {
        if (is_null($type)) {
            return self::$types['all'];
        }
        return self::$types[$type];

    }
    public static function getMimeTypes($mimeType = null)
    {
        if (is_null($mimeType)) {
            return self::$mimeTypes;
        }
        return self::$mimeTypes[$mimeType];

    }
    public static function sendTelegram($posts, $channel)
    {
        $telegram = new Api();
        $typeTags = [
            // 'text' => '\n #متن', 'image' => '\n #متن_تصویر', 'video' => '\n #ویدئو'
        ];
        foreach ($posts as $post) {
            $data['chat_id'] = $channel['id'];
            $text = $post->text . ($typeTags[$post->type] ?? '');
            $symbol = (preg_match('/[ي ك اآبپتثئجچحخدذرزژسشصضطظعغف  قکگلمنوهی \s]/', $post->user_title[0])) ? '↞' : '↠';
            $sign = $channel['sign'];
            $userSign = '';
            if (!empty($post->user_name) && $post->from != 'telegram' && $post->from != 'instagram') {
                $userSign = " \n \n ↠ $post->user_name ";
            }
            $extra = json_decode($post->extra);
            if (!empty($post->file_name)) {
                $postType = preg_match('/\.mp4$/', $post->file_name) ? 'video' : 'image';
                $localUrl = "build/channel_files/$post->from/$postType/$post->file_name";
            } else {
                if (isset($extra->images)) {
                    $localUrl = $extra->images[0]->url;
                } else if (isset($extra->videos)) {
                    $localUrl = $extra->videos[0]->url;
                }
            }

            $textFinal = $text . $sign;
            $textKey = $post->type == 'text' ? 'text' : 'caption';
            $data[$textKey] = $textFinal;
            $flagLongCaption = false;
            if (mb_strlen($textFinal) >= 200) {
                $flagLongCaption = true;
                $data[$textKey] = "توضیحات پست بعدی \n" . $sign;
            }
            if ($post->type == 'text') {
                // $data['text'] = $text . $sign;
                $res = $telegram->sendMessage($data);

            } else if ($post->type == 'image') {
                $mediaUrl = $localUrl;
                $data['photo'] = $mediaUrl;

                // $data['caption'] = $text . $sign;
                $res = $telegram->sendPhoto($data);

            } else if (in_array($post->type, self::getTypes('video'))) {
                $mediaUrl = $localUrl;
                $data['video'] = $mediaUrl;
                // ee($data);
                // $data['caption'] = $text . $sign;
                $res = $telegram->sendVideo($data);

            } else if ($post->type == 'document') {
                $media = $extra->documents;
                $text = (empty($text) || $text == 'text') ? $extra->documentInfo->file_name : $text;
                // $tag = '';
                // if ($extra->documentInfo->mime_type == self::getMimeTypes('pdf')) {
                //     $tag = " \n #کتاب ";
                // }

                $mediaUrl = $localUrl ?? $media[0]->url ?? '';
                $data['document'] = str_replace('https:', 'http:', $mediaUrl);
                // $data['caption'] = $text . $sign;
                $res = $telegram->sendDocument($data);

            } else if ($post->type == 'wallpaper') {
                $tag = '#تصویرـزمینه';
                $extra = json_decode($post->extra);
                $media = $extra->images;
                $data['caption'] = $tag . $sign;
                $mediaUrl = $media[0]->url;
                if (urlExists($mediaUrl)) {
                    $img = Image::make($mediaUrl);
                    $img->trim('right', ['left', 'right'], 15)->save('temp.jpg');
                    $data['photo'] = 'temp.jpg';
                    $telegram->sendPhoto($data);

                    $data['caption'] = $tag . " #کیفیتـاصلی" . $sign;
                    $data['document'] = 'temp.jpg';
                    $res = $telegram->sendDocument($data);
                } else {
                    $res = false;
                }

            } else if ($post->type == 'audio') {
                $media = $extra->audios;
                // $data['caption'] = $text . $sign;
                $mediaUrl = $localUrl ?? $media[0]->url ?? '';
                if (urlExists($mediaUrl)) {
                    $data['audio'] = str_replace('https:', 'http:', $mediaUrl);
                    $res = $telegram->sendAudio($data);
                } else {
                    $res = false;
                }

            } else if ($post->type == 'wallpaper') {
                $tag = '#تصویرـزمینه';
                $media = $extra->images;
                $data['caption'] = $tag . $sign;
                $mediaUrl = $localUrl ?? $media[0]->url ?? '';
                if (urlExists($mediaUrl)) {
                    $img = Image::make($mediaUrl);
                    $img->trim('right', ['left', 'right'], 17)->save('temp.jpg');
                    $data['photo'] = 'temp.jpg';
                    $telegram->sendPhoto($data);

                    $data['caption'] = $tag . " #کیفیتـاصلی" . $sign;
                    $data['document'] = 'temp.jpg';
                    $res = $telegram->sendDocument($data);
                } else {
                    $res = false;
                }
            }
            if ($flagLongCaption) {
                $res = $telegram->sendMessage(['chat_id' => $channel['id'], 'text' => $textFinal, 'reply_to_message_id' => $res['message_id']]);
            }
            if (is_array($extra)) {
                $extra['sentData'] = $res ?? '';
            } else {
                $extra->sentData = $res ?? '';

            }
            $post->update(['status' => 'sent', 'extra' => json_encode($extra)]);

            if ($res && in_array($post->type, ['video', 'instafunvideo'])) {
                $forwards = [
                    // 36434691, // me
                    // , "413109672" // ali
                    // "313500033"
                    // "-1001265112636" best_of_social
                    "-1001053371343"
                    , "-1001065140994"
                    , "-1001107724225",
                    "-1001048091950",
                    // , "-1001033224324"
                ];

                $forwardBot = new Api(Telegram_post::getBots('juddy_abbot'));
                foreach ($forwards as $id) {
                    $messageId = $res['message_id'];
                    $data = ['chat_id' => $id, 'from_chat_id' => '@' . $res['chat']['username'], 'message_id' => $messageId];
                    // ee($data);
                    // ee($groupMessage['message']['chat'], 0);
                    $result = $forwardBot->forwardMessage($data);
                    \App\Test::create(['a' => $id . json_encode($result), 'b' => 'forwardResult']);
                    // ee($result, 0);
                    if (isset($result['parameters']['migrate_to_chat_id']) && empty($result['ok'])) {
                        $data['chat_id'] = $result['parameters']['migrate_to_chat_id'];
                        $result = $forwardBot->forwardMessage($data);
                    }

                }
            }
            if ($res) {
                if (isset($_SESSION['try_send_telegram'])) {
                    unset($_SESSION['try_send_telegram']);
                }
                return $res;
            } else {
                if ($posts) {
                    if (!isset($_SESSION['try_send_telegram'])) {
                        $_SESSION['try_send_telegram'] = 0;
                    } else {
                        $_SESSION['try_send_telegram']++;
                    }
                    if ($_SESSION['try_send_telegram'] < 4) {
                        $posts = Telegram_post::where('status', 'active')->where('type', $post->type)
                            ->orderByRaw('rand()')
                            ->limit(1)->get();
                        Telegram_post::sendTelegram($posts);
                    } else {
                        unset($_SESSION['try_send_telegram']);
                    }
                } else {
                    if (isset($_SESSION['try_send_telegram'])) {
                        unset($_SESSION['try_send_telegram']);
                    }
                    return 'no posts';
                }

            }
        }
    }
}
