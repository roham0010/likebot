<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telegram2_post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'text', 'status', 'user_name', 'file_name', 'user_title', 'media_id', 'telegram_update_id', 'user_id', 'url', 'from', 'extra'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '',
    ];

    public static $bots = [
        'googsell_bot' => '312090865:AAHc-dNdBMN8fFfWVnJ-zvWtHl22zc5RAS8',
        'juddy_abbot' => '311819068:AAHrSAgRLU-g4v3xlptULAQN4yr6356tDEk',
    ];
    public static $types = [
        'all' => ['text', 'image', 'video', 'audio', 'document', 'voice', 'wallpaper', 'instafunvideo', 'craft', 'morning'],
        'video' => ['instafunvideo', 'video', 'craft'],
        'multimedia' => ['instafunvideo', 'video', 'craft', 'image', 'wallpaper', ''],
    ];
    public static $mimeTypes = [
        'video' => 'video/mp4',
        'gif' => 'video/mp4',
        'pdf' => 'application/pdf',
        'audio' => 'audio/mpeg',
        'voice' => 'application/pdf',
    ];

    public function getColumns($type = 'fillable')
    {
        return $this->fillable;
    }
    public static function getBots($bot = null)
    {
        if (is_null($bot)) {
            return self::$bots;
        }
        return self::$bots[$bot];

    }
    public static function getTypes($type = null)
    {
        if (is_null($type)) {
            return self::$types['all'];
        }
        return self::$types[$type];

    }
    public static function getMimeTypes($mimeType = null)
    {
        if (is_null($mimeType)) {
            return self::$mimeTypes;
        }
        return self::$mimeTypes[$mimeType];

    }
}
