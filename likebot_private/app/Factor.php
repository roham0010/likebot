<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factor extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'user_id', 'price', 'likes', 'real_price', 'real_likes', 'type', 'discount_description', 'gateway_id',
    ];

    public static $statuses = [
        'init' => 'اتصال به درگاه',
        'failed' => 'پرداخت نشده',
        'payed' => 'پرداخت شده',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    public function status()
    {
        return self::$statuses[$this->status];
    }
    public function priceDiscount()
    {
        return (($this->real_price > 0) ? ($this->real_price - $this->price) : 0);
    }
    public function likeDiscount()
    {
        return (($this->real_likes > 0) ? ($this->likes - $this->real_likes) : 0);
    }
}
