<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Error extends Model
{
    /**
     * Get the user that owns the Blog.
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'page', 'data', 'error',
        /**
         * types
         * 1 for store products
         *
         */
    ];
}
