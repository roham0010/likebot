<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liked_post extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'type', 'status', 'instagram_username', 'instagram_userid', 'content_type', 'tag', 'shortcode', 'text', 'extra',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    public function media($type = 'thumb')
    {
        $extra = json_decode($this->extra);
        $images = $extra->images;
        // ee($images);
        if ($type == 'thumb') {
            $image = $images->thumb[0];
            // $image = $images->standard[0];
        }
        return $image;
    }
}
