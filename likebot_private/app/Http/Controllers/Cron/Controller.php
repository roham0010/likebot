<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    // api response helpers
    public function json_return($data)
    {
        return response()->json($data, $data['code']);
    }

    public function response_error($data = '', $status = 'false', $callback = null, $errCode = 400)
    {
        $callbackArray = $callback ? ['callback' => $callback] : [];
        $metadataArray = [];
        $messageArray = [];
        $dataArray = [];
        $data = json_decode(json_encode($data), true);
        if (is_string($data) || empty($data)) {
            $messageArray = $data ? ['message' => $data] : [];
        } elseif (is_array($data) && count($data) > 0) {
            if (isset($data['metadata']) && count($data['metadata']) > 0) {
                $metadataArray = ['message' => $data['metadata']];
                $dataArray = ['results' => $data['data']];
            }
            $dataArray = ['results' => $data];
        }
        if (strpos(\Request::url(), '/api/') !== false || \Request::input('ajax')) {
            return self::json_return(array_merge(['status' => $status, 'code' => $errCode], $dataArray, $metadataArray, $messageArray));
        } else {
            if (!empty(\Request::server('HTTP_REFERER'))) {
                return redirect()->back()->with(['message' => $message]);
            } else {
                return redirect('/myProfile')->with(['message' => trans('messages.error.access_denied')]);
            }

        }
    }

    public function response_success($data = '', $status = 'OK', $callback = null, $errCode = 200)
    {
        $callbackArray = $callback ? ['callback' => $callback] : [];
        $metadataArray = [];
        $messageArray = [];
        $dataArray = [];
        $data = json_decode(json_encode($data), true);
        if (is_string($data) || empty($data)) {
            $messageArray = $data ? ['message' => $data] : [];
        } elseif (is_array($data) && count($data) > 0) {
            if (isset($data['metadata']) && count($data['metadata']) > 0) {
                // $metadataArray = ['metadata' => $data['metadata']];
                $dataArray = ['results' => $data['data']];
            } else {
                $dataArray = ['results' => $data];
            }

        }
        if (strpos(\Request::url(), '/api/') !== false || \Request::input('ajax')) {
            return self::json_return(array_merge(['status' => $status, 'code' => $errCode], $metadataArray, $dataArray, $messageArray));
        } else {
            if (!empty(\Request::server('HTTP_REFERER'))) {
                return redirect()->back()->with(['message' => $message]);
            } else {
                return redirect('/myProfile')->with(['message' => $message]);
            }

        }
    }

    public function response_form_error($data = '', $status = 'formError', $callback = null, $errCode = 400)
    {
        $callbackArray = $callback ? ['callback' => $callback] : [];
        $metadataArray = [];
        $messageArray = [];
        $dataArray = [];
        $data = json_decode(json_encode($data), true);
        if (is_string($data) || empty($data)) {
            $messageArray = $data ? ['message' => $data] : [];
        } elseif (is_array($data) && count($data) > 0) {
            if (isset($data['metadata']) && count($data['metadata']) > 0) {
                $metadataArray = ['message' => $data['metadata']];
                $dataArray = ['results' => $data['data']];
            }
            $dataArray = ['results' => $data];
        }
        if (strpos(\Request::url(), '/api/') !== false || \Request::input('ajax')) {
            return self::json_return(array_merge(['status' => $status, 'code' => $errCode], $dataArray, $metadataArray, $messageArray));
        } else {
            if (!empty(\Request::server('HTTP_REFERER'))) {
                return redirect()->back()->withInput()->withErrors($formErrors);
            } else {
                return redirect('/myProfile')->withErrors($formErrors);
            }

        }
    }
    public function isApiRequest()
    {
        return (strpos(\Request::fullUrl(), '/api/') !== false);
    }
}
