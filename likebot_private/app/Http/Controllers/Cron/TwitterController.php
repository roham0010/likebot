<?php

namespace App\Http\Controllers\Cron;

use App\Telegram_post;
use Illuminate\Http\Request;
use Twitter;

class TwitterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $badWords = ['Kos', 'Kir', 'Kiram', 'Koon', 'Pestoon', 'Jakesh', 'Khayeh', 'cos', 'jende', 'keer', 'جقی', 'جق', 'کس', 'کیر', 'کون', 'پستون', 'ممه', 'جاکش', 'خایه', 'جنده', 'کییر', 'کیییر',
        ];
        $replaces = [
            'words' => ['کصشر', 'کسکشه', 'توييتر', 'توييت', 'توييت', 'توييت', 'توييت'],
            'replaces' => ['چرت و پرت', 'خاک دو عالم', 'تلگرام', 'کامنت', 'کامنت', 'کامنت', 'کامنت'],
        ];
        $users = [
            'Faezze3',
            'mahlagha_hs',
            'buuuugh',
            'kayotRaRa',
            'sinavaliollah',
            'bohluol',
            'Ashkan_hn',
            'Shahgrap',
            'jansnow36',
            'mahaya_t',
            'Pinadoye',
            'sobbekheir',
            'HastamDelbar',
            'mortezaoleg',
            'reasomania',
            'Miisteer_Biiing',
            'mis_noell',
            'imbishour',
            'rad7dar',
            'rooobah_naranji',
            'Tiredgnat',
            'parisayaa',
            'srnmhda',
            'Mr_ajam15',
            'Rozh_I',
            'ASayehh',
            'elahetwiiter',
            'Gavaazn',
            'imanbrando',
            'emovm',
            'khashayar5h',
            'habilbabil',
            'faraan1996',
            '_maripela',
            'ricki_sunchez',
            '_amirjoey',
            'bozmajjje',
            'misSsSs_X',
            'M_Asmmm',
            'velgard100',
            'saraaaa1994',
            'intheWISH',
            'pmiliii',
            'Aghae_C',
        ];
        $i = 0;
        foreach ($users as $key => $user) {
            $timeline = Twitter::getUserTimeline(['screen_name' => $user, 'count' => 20, 'format' => 'json', 'tweet_mode' => 'extended']);
            $twitts = json_decode($timeline);
            $lastPost = Telegram_post::where(['user_name' => $user])->orderBy('id', 'desc')->first();
            $posts = [];
            // ee($twitts);
            // if ($i++ >= 2) {
            //     ee();
            // }
            foreach ($twitts as $twitt) {
                if ($lastPost && $twitt->id == $lastPost->media_id) {
                    break;
                }
                if (!empty($twitt->in_reply_to_status_id)) {
                    continue;
                }
                $text = $twitt->full_text;
                $flagBadword = false;
                foreach ($badWords as $badWord) {
                    if (preg_match("/ $badWord /", $text)) {
                        $flagBadword = true;
                        break;
                    }
                }
                if ($flagBadword) {
                    continue;
                }
                $post = [];
                $post['reply'] = '';
                $extra = [];
                $user = $twitt->user;
                $ctwitt = collect($twitt);
                $type = 'text';
                if (isset($twitt->extended_entities->media) && $twitt->extended_entities->media[0]) {
                    $media = $twitt->extended_entities->media[0];
                    $type = $twitt->extended_entities->media[0]->type == 'photo' ? 'image' : 'video';
                }
                // $post = $ctwitt->only(['text', 'id'])->toArray();
                $post['text'] = $text;
                $post['media_id'] = (string) $twitt->id;
                if ($type == 'image') {
                    $post['text'] = str_replace($media->url, '', $post['text']);
                }
                if (preg_match('/RT @(.*):/', $post['text'], $matches)) {
                    // ee($matches);
                    continue;
                    $post['reply'] = $matches[1];
                    $post['text'] = preg_replace('/RT @(.*):/', '', $post['text']);
                }
                $post['user_title'] = $user->name;
                $post['user_name'] = $user->screen_name;
                $post['user_id'] = (string) $user->id;
                $post['from'] = 'twitter';
                $post['type'] = $type;
                if ($type == 'image') {
                    $extra['images'] = [['url' => $media->media_url_https]];
                } else if ($type == 'video') {
                    foreach ($media->video_info->variants as $video) {
                        if (@$video->bitrate == 320000) {
                            break;
                        }
                    }
                    $extra['videos'] = [['url' => $video->url]];
                    $extra['size'] = $video->bitrate;

                }
                $post['extra'] = json_encode($extra);
                $posts[] = $post;
            }
            $posts = array_reverse($posts);
            if ($posts) {
                Telegram_post::insert($posts);
            }
        }
    }
    /**
     * Route: cron/twitter/send
     */
    public function sendToTelegram(Request $request)
    {
        $current_time = date('H', time());
        $times = [
            // 9 => 'text',
            9 => 'instafunvideo',
            // 10 => 'craft',
            // 11 => 'instafunvideo',
            12 => 'instafunvideo',
            // 12 => 'wallpaper',
            // 13 => 'text',
            14 => 'instafunvideo',
            // 15 => 'image',
            // 16 => 'instafunvideo',
            17 => 'instafunvideo',
            // 19 => 'text',
            19 => 'instafunvideo',
            // 20 => 'instafunvideo',
            // 21 => 'instafunvideo',
            22 => 'instafunvideo',
            23 => 'instafunvideo',
            0 => 'image',
            // 1 => 'image',
        ];
        if (!in_array($current_time, array_keys($times))) {
            return 'false';
        }
        if ($current_time > 1 && $current_time < 9) {
            return 'false';
        }
        $type = $_GET['type'] ?? $times[$current_time] ?? 'text';
        $posts = Telegram_post::where('status', 'active')->where('type', $type)->where(function ($q) {
            return $q->whereNull('content_type')->orWhere('content_type', '<>', 'cake')->orWhere('content_type', '');
        })
            ->orderByRaw('rand()')
            ->limit(1)->get();
        // ee($type, 0);
        // ee($posts);
        $channel = channel('best_of_social');
        $res = Telegram_post::sendTelegram($posts, $channel);

        ee($res);
    }
}
