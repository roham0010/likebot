<?php

namespace App\Http\Controllers\Cron;

use App\Likebot\Package_extends\MyInstagram;
use Illuminate\Http\Request;
use Telegram\Bot\Api;
use \App\Telegram_post;

class TelegramController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function get(Request $request)
    {
        // video
        // $json = '
        // {"update_id":8471586921,"message":{"message_id":66,"from":{"id":413109672,"is_bot":false,"first_name":"( Ali )","username":"Seyed_ali_alone","language_code":"en"},"chat":{"id":413109672,"first_name":"( Ali )","username":"Seyed_ali_alone","type":"private"},"date":1512074689,"forward_from_chat":{"id":-1001095960268,"title":"\ud83d\ude1c\u0639\u0640\u0640\u0640\u0640\u0640\u0640\u0645\u0647 \u0637\u0640\u0640\u0640\u0640\u0644\u0627\ud83d\ude1c\ud83d\udca6","type":"channel"},"forward_from_message_id":181983,"forward_date":1508759934,"video":{"duration":60,"width":400,"height":320,"mime_type":"video\/mp4","thumb":{"file_id":"AAQEABMB6d8ZAASBms62BP8XidIpAAIC","file_size":1856,"width":90,"height":72},"file_id":"BAADBAADcwEAAlHciFGb3hsDG_H3HAI","file_size":2368693},"caption":"\u0627\u06cc\u0646 \u0645\u06cc\u06a9\u0633 \u0639\u0627\u0644\u06cc\u0647 \ud83d\ude02\ud83d\udc4c\n\n\u0641\u0642\u0637 \u0627\u0648\u0646 \u0641\u06cc\u0644\u0647 ...\ud83d\ude02\ud83d\ude02\n\n\u0627\u0632 \u062f\u0633\u062a\u0634 \u0628\u062f\u06cc \u0628\u0627\u062e\u062a\u06cc\ud83d\ude02\ud83d\ude02\n\n\u0639\u0645\u062a \u062e\u0648\u0634\u06af\u0644\u0647 \u0628\u06cc\u0627\ud83d\ude02\ud83d\udd96\ud83d\udc47\nhttps:\/\/telegram.me\/joinchat\/AAAAAEFTBsz-mJz-rhbd2g","caption_entities":[{"offset":87,"length":51,"type":"url"}]}}
        // ';

        // image
        // $json = '
        // {"update_id":8471588621,"message":{"message_id":132,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512214048,"forward_from_chat":{"id":-1001002283119,"title":"\u06a9\u062a\u0627\u0628\u062e\u0627\u0646\u0647 \u0633\u06cc\u0645\u0631\u063a","username":"seemorghbook","type":"channel"},"forward_from_message_id":35841,"forward_date":1512196449,"photo":[{"file_id":"AgADBAAD96oxG4C6EVHbdwtZbJSNgG9A-hkABO10g_hvrhLsYwABBAABAg","file_size":2150,"width":70,"height":90},{"file_id":"AgADBAAD96oxG4C6EVHbdwtZbJSNgG9A-hkABIgQj7vJwuwbZAABBAABAg","file_size":25855,"width":250,"height":320},{"file_id":"AgADBAAD96oxG4C6EVHbdwtZbJSNgG9A-hkABLyN_hC_DjPwZQABBAABAg","file_size":84518,"width":626,"height":800},{"file_id":"AgADBAAD96oxG4C6EVHbdwtZbJSNgG9A-hkABALzv1_kfevFYgABBAABAg","file_size":130801,"width":1002,"height":1280}],"caption":"\u269c\u200c\u062f\u0648 \u0631\u0627\u0647 \u0628\u06cc\u0634\u062a\u0631 \u0648\u062c\u0648\u062f \u0646\u062f\u0627\u0631\u062f :\n\n\u06cc\u0627 \u0630\u0644\u062a\u0650 \u00ab\u062a\u0642\u0644\u06cc\u062f\u00bb \u0631\u0627 \u062a\u062d\u0645\u0644 \u06a9\u0646 \u060c\n\u06cc\u0627 \u0631\u0646\u062c\u0650 \u00ab\u062a\u0641\u0627\u0648\u062a\u00bb \u0631\u0627 \u0628\u0647 \u062f\u0648\u0634 \u0628\u06a9\u0634 .\n\n\u062a\u0642\u0644\u06cc\u062f \u0628\u062f\u0648\u0646 \u0622\u06af\u0627\u0647\u06cc \u060c\n\u0633\u0631\u0627\u0646\u062c\u0627\u0645\u0634 \u062a\u0628\u0627\u0647\u06cc\u200c\u0633\u062a !\n\n@seemorghbook","caption_entities":[{"offset":131,"length":13,"type":"mention"}]}}
        // ';

        // document git
        // $json = '
        // {"update_id":847158709,"message":{"message_id":82,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512083409,"forward_from":{"id":413109672,"is_bot":false,"first_name":"( Ali )","username":"Seyed_ali_alone","language_code":"en"},"forward_date":1512062950,"document":{"file_name":"Finish-Him.gif.mp4","mime_type":"video\/mp4","thumb":{"file_id":"AAQEABPCeo8wAARp6S7LH1vprfosAAIC","file_size":2461,"width":90,"height":50},"file_id":"CgADBAADZggAAkMprAMsOOIP9CAq7gI","file_size":272629}}}
        // ';

        // document pdf
        // $json = '
        // {"update_id":8471587111,"message":{"message_id":83,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512084336,"forward_from_chat":{"id":-1001002283119,"title":"\u06a9\u062a\u0627\u0628\u062e\u0627\u0646\u0647 \u0633\u06cc\u0645\u0631\u063a","username":"seemorghbook","type":"channel"},"forward_from_message_id":35724,"forward_date":1512046434,"document":{"file_name":"\u062c\u0646\u0628\u0634 \u062d\u0631\u0648\u0641\u06cc\u0647 \u0648 \u0646\u0647\u0636\u062a \u067e\u0633\u06cc\u062e\u0627\u0646\u06cc\u0627\u0646.pdf","mime_type":"application\/pdf","file_id":"BQADBAADiAADtwooURYlaCOU11NMAg","file_size":5357934}}}
        // ';

        // document pdf 1 mb
        // $json = '
        // {"update_id":847158714,"message":{"message_id":84,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512087794,"forward_from_chat":{"id":-1001002283119,"title":"\u06a9\u062a\u0627\u0628\u062e\u0627\u0646\u0647 \u0633\u06cc\u0645\u0631\u063a","username":"seemorghbook","type":"channel"},"forward_from_message_id":35657,"forward_date":1511965356,"document":{"file_name":"\u0639\u0642\u0644\u0627 \u0628\u0631\u062e\u0644\u0627\u0641 \u0639\u0642\u0644 \u0639\u0644\u06cc \u062f\u0634\u062a\u06cc.pdf","mime_type":"application\/pdf","file_id":"BQADBAADKwUAAggcrggLuB83Diyq-AI","file_size":1995608}}}
        // ';

        // audio
        // $json = '
        // {"update_id":8471587171,"message":{"message_id":86,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512088116,"forward_from":{"id":508509745,"is_bot":false,"first_name":"F.salimi"},"forward_date":1512073815,"audio":{"duration":96,"mime_type":"audio\/mpeg","title":"Mia & Sebastian\'s Theme","performer":"Justin Hurwitz-www.downloadha.com","file_id":"CQADBAADOgUAAi5XAAFRg0Gt2xkm8joC","file_size":3915860}}}
        // ';

        // $jsonLink = '
        // {"update_id":8471588021,"message":{"message_id":108,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512167503,"text":"https:\/\/www.instagram.com\/p\/BcLLzz0FZWl\/?tagged=video","entities":[{"offset":0,"length":53,"type":"url"}]}}
        // ';

        // instagram linkVideo
        // $json = '
        // {"update_id":8471588041,"message":{"message_id":110,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512167697,"text":"https:\/\/www.instagram.com\/p\/BcJivh3lUpn\/?tagged=video","entities":[{"offset":0,"length":53,"type":"url"}]}}
        // ';
        // $jsons = [
        //     'video' => '{"update_id":847158691231,"message":{"message_id":66,"from":{"id":413109672,"is_bot":false,"first_name":"( Ali )","username":"Seyed_ali_alone","language_code":"en"},"chat":{"id":413109672,"first_name":"( Ali )","username":"Seyed_ali_alone","type":"private"},"date":1512074689,"forward_from_chat":{"id":-1001095960268,"title":"\ud83d\ude1c\u0639\u0640\u0640\u0640\u0640\u0640\u0640\u0645\u0647 \u0637\u0640\u0640\u0640\u0640\u0644\u0627\ud83d\ude1c\ud83d\udca6","type":"channel"},"forward_from_message_id":181983,"forward_date":1508759934,"video":{"duration":60,"width":400,"height":320,"mime_type":"video\/mp4","thumb":{"file_id":"AAQEABMB6d8ZAASBms62BP8XidIpAAIC","file_size":1856,"width":90,"height":72},"file_id":"BAADBAADcwEAAlHciFGb3hsDG_H3HAI","file_size":2368693},"caption":"\u0627\u06cc\u0646 \u0645\u06cc\u06a9\u0633 \u0639\u0627\u0644\u06cc\u0647 \ud83d\ude02\ud83d\udc4c\n\n\u0641\u0642\u0637 \u0627\u0648\u0646 \u0641\u06cc\u0644\u0647 ...\ud83d\ude02\ud83d\ude02\n\n\u0627\u0632 \u062f\u0633\u062a\u0634 \u0628\u062f\u06cc \u0628\u0627\u062e\u062a\u06cc\ud83d\ude02\ud83d\ude02\n\n\u0639\u0645\u062a \u062e\u0648\u0634\u06af\u0644\u0647 \u0628\u06cc\u0627\ud83d\ude02\ud83d\udd96\ud83d\udc47\nhttps:\/\/telegram.me\/joinchat\/AAAAAEFTBsz-mJz-rhbd2g","caption_entities":[{"offset":87,"length":51,"type":"url"}]}}',
        //     'image' => '{"update_id":84715886121,"message":{"message_id":132,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512214048,"forward_from_chat":{"id":-1001002283119,"title":"\u06a9\u062a\u0627\u0628\u062e\u0627\u0646\u0647 \u0633\u06cc\u0645\u0631\u063a","username":"seemorghbook","type":"channel"},"forward_from_message_id":35841,"forward_date":1512196449,"photo":[{"file_id":"AgADBAAD96oxG4C6EVHbdwtZbJSNgG9A-hkABO10g_hvrhLsYwABBAABAg","file_size":2150,"width":70,"height":90},{"file_id":"AgADBAAD96oxG4C6EVHbdwtZbJSNgG9A-hkABIgQj7vJwuwbZAABBAABAg","file_size":25855,"width":250,"height":320},{"file_id":"AgADBAAD96oxG4C6EVHbdwtZbJSNgG9A-hkABLyN_hC_DjPwZQABBAABAg","file_size":84518,"width":626,"height":800},{"file_id":"AgADBAAD96oxG4C6EVHbdwtZbJSNgG9A-hkABALzv1_kfevFYgABBAABAg","file_size":130801,"width":1002,"height":1280}],"caption":"\u269c\u200c\u062f\u0648 \u0631\u0627\u0647 \u0628\u06cc\u0634\u062a\u0631 \u0648\u062c\u0648\u062f \u0646\u062f\u0627\u0631\u062f :\n\n\u06cc\u0627 \u0630\u0644\u062a\u0650 \u00ab\u062a\u0642\u0644\u06cc\u062f\u00bb \u0631\u0627 \u062a\u062d\u0645\u0644 \u06a9\u0646 \u060c\n\u06cc\u0627 \u0631\u0646\u062c\u0650 \u00ab\u062a\u0641\u0627\u0648\u062a\u00bb \u0631\u0627 \u0628\u0647 \u062f\u0648\u0634 \u0628\u06a9\u0634 .\n\n\u062a\u0642\u0644\u06cc\u062f \u0628\u062f\u0648\u0646 \u0622\u06af\u0627\u0647\u06cc \u060c\n\u0633\u0631\u0627\u0646\u062c\u0627\u0645\u0634 \u062a\u0628\u0627\u0647\u06cc\u200c\u0633\u062a !\n\n@seemorghbook","caption_entities":[{"offset":131,"length":13,"type":"mention"}]}}',
        //     'gif' => '{"update_id":8471518709,"message":{"message_id":82,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512083409,"forward_from":{"id":413109672,"is_bot":false,"first_name":"( Ali )","username":"Seyed_ali_alone","language_code":"en"},"forward_date":1512062950,"document":{"file_name":"Finish-Him.gif.mp4","mime_type":"video\/mp4","thumb":{"file_id":"AAQEABPCeo8wAARp6S7LH1vprfosAAIC","file_size":2461,"width":90,"height":50},"file_id":"CgADBAADZggAAkMprAMsOOIP9CAq7gI","file_size":272629}}}',
        //     'pdf' => '{"update_id":84715871111,"message":{"message_id":83,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512084336,"forward_from_chat":{"id":-1001002283119,"title":"\u06a9\u062a\u0627\u0628\u062e\u0627\u0646\u0647 \u0633\u06cc\u0645\u0631\u063a","username":"seemorghbook","type":"channel"},"forward_from_message_id":35724,"forward_date":1512046434,"document":{"file_name":"\u062c\u0646\u0628\u0634 \u062d\u0631\u0648\u0641\u06cc\u0647 \u0648 \u0646\u0647\u0636\u062a \u067e\u0633\u06cc\u062e\u0627\u0646\u06cc\u0627\u0646.pdf","mime_type":"application\/pdf","file_id":"BQADBAADiAADtwooURYlaCOU11NMAg","file_size":5357934}}}',
        //     'audio' => '{"update_id":84715871171,"message":{"message_id":86,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512088116,"forward_from":{"id":508509745,"is_bot":false,"first_name":"F.salimi"},"forward_date":1512073815,"audio":{"duration":96,"mime_type":"audio\/mpeg","title":"Mia & Sebastian\'s Theme","performer":"Justin Hurwitz-www.downloadha.com","file_id":"CQADBAADOgUAAi5XAAFRg0Gt2xkm8joC","file_size":3915860}}}',
        //     'instagramlink' => '{"update_id":84715881041,"message":{"message_id":110,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512167697,"text":"https:\/\/www.instagram.com\/p\/BcJivh3lUpn\/?tagged=video","entities":[{"offset":0,"length":53,"type":"url"}]}}',
        //     'error' => '{"update_id":84715871171,"message":{"message_id":86,"from":{"id":36434691,"is_bot":false,"first_name":"BlackSword","username":"Hassan_Shojaei","language_code":"en"},"chat":{"id":36434691,"first_name":"BlackSword","username":"Hassan_Shojaei","type":"private"},"date":1512088116,"forward_from":{"id":508509745,"is_bot":false,"first_name":"F.salimi"},"forward_date":1512073815,"audio":{"duration":96,"mime_type":"audio\/mpeg","title":"Mia & Sebastians Themes","performer":"Justin Hurwitz-www.downloadha.com","file_id":"CQADBAADOgUAAi5XAAFRg0Gt2xkm8joC","file_size":3915860}}}',
        // ];
        // $updates = json_decode($jsons['error']);
        // ee($updates);

        $updates = json_decode($request->json);
        $message = $updates->message ?? $updates->channel_post ?? $updates->edited_channel_post ?? '';
        // ee($updates);
        if ($message->chat->type != 'private') {
            return 'true';
        }
        \App\Test::create(['a' => json_encode($updates), 'b' => json_encode(@$message->chat), 'c' => json_encode(@$message->chat->id)]);
        // $message->text = 'https://www.instagram.com/p/BcK7FqwHUHB/?tagged=video';
        // $message->text = 'https://www.instagram.com/p/BcLBygojPtI/?tagged=video';
        // $message->text = 'https://www.instagram.com/p/BcLLzz0FZWl/?tagged=video';

        /**
         * This is a instagram link
         */
        if (isset($message->text) && preg_match('/http[|s]:.*instagram\.com/', @$message->text)) {

            if (isset($updates->update_id) && !Telegram_post::where('telegram_update_id', $updates->update_id)->first()) {
                $instagram = new MyInstagram([
                    'apiKey' => '4d1191f25f654d1cbe91ffeb8f8eeb77',
                    'apiSecret' => '4092bcdf55ff43d0bcb817474def008c',
                    'apiCallback' => "likebot.ir", // must point to success.php
                ]);
                preg_match_all('/\/p\/(.*)\//', $message->text, $matches);
                // ee($matches);
                $code = $matches[1][0];
                $node = $instagram->myGetMedia($code);
                $node = $node->graphql ?? '';
                $node = $node->shortcode_media ?? '';
                $post = [];
                if ($node) {
                    if (isset($node->id) && !Telegram_post::where('media_id', $node->id)->where('type', 'like', '%instagram%')->first()) {
                        // ee($node);
                        $post['from'] = 'instagram_link';
                        $post['text'] = 'بدون شرح';
                        $post['telegram_update_id'] = $updates->update_id;
                        $post['media_id'] = $node->id;
                        $extra = [];
                        $text = $node->edge_media_to_caption->edges[0]->node->text ?? '';
                        $extra['oldInfo']['text'] = $text;
                        // ee($text);
                        $text = preg_replace('/@.*[^\s]/', ' ', $text);
                        $text = preg_replace("/\n\n/", "\n", $text);
                        $text = preg_replace("/http.*[^\s]/", "\n", $text);
                        if (!empty($node->is_video)) {
                            $post['type'] = $post['type'] ?? 'video';
                            $extra['videos'] = [['url' => $node->video_url]];
                        } else {
                            $post['type'] = $post['type'] ?? 'image';
                            foreach ($node->display_resources as $image) {
                            }
                            $extra['images'] = [['url' => @$image->src]];
                        }
                        $post['extra'] = json_encode($extra);
                        // ee($post);
                        Telegram_post::create($post);

                    }
                }
            }

        } else {
            /**
             * Save other type of media forward picture video document pdf audio
             */
            $maxSize = (1024 * 1024 * 20);
            if (isset($updates->update_id) && !Telegram_post::where('media_id', $updates->update_id)->first()) {
                // ee($updates);
                $botId = Telegram_post::getBots('juddy_abbot');
                $telegram = new Api($botId);

                // \App\Test::create(['a' => json_encode($request->input())]);
                // exit;
                // ee($updates);
                if ($updates) {
                    $node = $updates->message;
                    $post = [];
                    $post['reply'] = '';
                    $post['from'] = 'telegram';
                    $extra = [];
                    $text = $node->caption ?? $node->text ?? '';
                    $text = preg_replace('/@.*[^\s]/', ' ', $text);
                    // $text = preg_replace('/\#.*[^\s]/', ' ', $text);
                    $text = preg_replace("/\n\n/", "\n", $text);
                    $text = preg_replace("/http.*[^\s]/", "\n", $text);
                    // $text = preg_replace("/\n\n.*/", ' ', $node->caption);
                    // $user = $node->user;
                    if (isset($node->forward_from_chat)) {
                        $user = $node->forward_from_chat;
                        $post['user_title'] = $user->title;
                        $post['user_name'] = $user->username ?? '';
                        $post['user_id'] = $user->id;
                        $extra['from'] = @$node->from;
                    } else {
                        $user = $node->from;
                        $post['user_title'] = $user->first_name;
                        $post['user_name'] = $user->username;
                        $post['user_id'] = $user->id;
                    }
                    $post['media_id'] = $updates->update_id;
                    if (isset($node->photo)) {
                        $photo = $node->photo;
                        $post['type'] = 'image';
                        foreach ($photo as $file) {
                        }
                        $filename = time() . ".jpg";
                        $post['file_name'] = $filename;
                        $fileId = $file->file_id;
                        $extra['file_id'] = $fileId;
                        $filepath = "build/channel_files/telegram/image/$filename";
                        if ($photo) {
                            $fileInfo = $telegram->getFile(['file_id' => $file->file_id]);
                            if ($fileInfo) {
                                $url = "https://api.telegram.org/file/bot$botId/" . $fileInfo['file_path'];
                                if (!file_exists($filepath)) {
                                    $fileSrc = file_get_contents($url);
                                    file_put_contents($filepath, $fileSrc);
                                }
                                $extra['images'] = [['url' => $url]];
                                $extra['imageInfo'] = $file;
                            }
                        }
                    } else if (isset($node->video)) {
                        $file = $node->video;
                        if ($file->file_size > $maxSize) {
                            return 'too big';
                        }
                        $post['type'] = 'video';

                        $filename = $file->title ?? time();
                        $filename = $filename . ".mp4";
                        $post['file_name'] = $filename;
                        $fileId = $file->file_id;
                        $extra['file_id'] = $fileId;
                        $filepath = "build/channel_files/telegram/video/$filename";
                        if ($file) {
                            $fileInfo = $telegram->getFile(['file_id' => $file->file_id]);
                            // ee($fileInfo);
                            if ($fileInfo) {
                                $url = "https://api.telegram.org/file/bot$botId/" . $fileInfo['file_path'];
                                // ee($url);
                                if (!file_exists($filepath)) {
                                    $fileSrc = file_get_contents($url);
                                    file_put_contents($filepath, $fileSrc);
                                }
                                $extra['videos'] = [['url' => $url, 'id' => $file->file_id]];
                                $extra['videoInfo'] = $file;
                            }
                        }
                    } else if (isset($node->document)) {
                        $file = $node->document;
                        if ($file->file_size > $maxSize) {
                            return 'too big';
                        }
                        $post['type'] = 'document';
                        $filename = $file->file_name;
                        $post['file_name'] = $filename;
                        $fileId = $file->file_id;
                        $extra['file_id'] = $fileId;
                        $filepath = "build/channel_files/telegram/document/$filename";
                        if ($file) {
                            $fileInfo = $telegram->getFile(['file_id' => $file->file_id]);
                            // ee($fileInfo);
                            if ($fileInfo) {
                                $url = "https://api.telegram.org/file/bot$botId/" . $fileInfo['file_path'];
                                if (!file_exists($filepath)) {
                                    $fileSrc = file_get_contents($url);
                                    file_put_contents($filepath, $fileSrc);
                                }
                                $extra['documents'] = [['url' => $url, 'id' => $file->file_id]];
                                $extra['documentInfo'] = $file;
                            }
                        }
                    } else if (isset($node->audio)) {
                        $file = $node->audio;
                        if ($file->file_size > $maxSize) {
                            return 'too big';
                        }
                        $post['type'] = 'audio';

                        $filename = $file->title . ".mp3";
                        $post['file_name'] = $filename;
                        $fileId = $file->file_id;
                        $extra['file_id'] = $fileId;
                        $filepath = "build/channel_files/telegram/audio/$filename";
                        if ($file) {
                            $fileInfo = $telegram->getFile(['file_id' => $file->file_id]);
                            if ($fileInfo) {
                                $url = "https://api.telegram.org/file/bot$botId/" . $fileInfo['file_path'];
                                if (!file_exists($filepath)) {
                                    $fileSrc = file_get_contents($url);
                                    file_put_contents($filepath, $fileSrc);
                                }
                                $extra['audios'] = [['url' => $url, 'id' => $file->file_id]];
                                $file->file_name = $file->title . ' - ' . $file->performer;
                                $extra['audioInfo'] = $file;
                            }
                        }
                    }
                    $post['text'] = $text;
                    $post['extra'] = json_encode($extra);
                    // ee($post);
                    // \App\Test::create(['a' => http_build_query($post)]);
                    Telegram_post::create($post);

                }
            }
        }
    }

}
