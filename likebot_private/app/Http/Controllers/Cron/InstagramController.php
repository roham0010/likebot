<?php

namespace App\Http\Controllers\Cron;

use App\Likebot\Package_extends\MyInstagram;
use App\Telegram2_post;
use App\Telegram_post;
use Illuminate\Http\Request;
use Image;
use Telegram\Bot\Api;
use Twitter;

class InstagramController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    public function instagram()
    {
        return new MyInstagram([
            'apiKey' => '4d1191f25f654d1cbe91ffeb8f8eeb77',
            'apiSecret' => '4092bcdf55ff43d0bcb817474def008c',
            'apiCallback' => "likebot.ir", // must point to success.php
        ]);
    }
    /**
     * @route cron/instagram/getTags
     */
    public function getTags()
    {
        $searchTags = ['صبح_بخیر'];
        $typeTags = ['صبح_بخیر' => '#صبح_بخیر'];
        $typeTexts = ['صبح بخیر
روز شد چشمان خود را باز کن صبح ات بخیر
آفتـاب آمـد , بـرایــش ناز کــن صبــح ات بخیر',
            'خورشید به تو می تابد
حال
هر روز
همین موقع
وقتی در خیابان
پرسه می زنی
صبح بخیر',
            'آسمان
جای ماه
روی تو را به انتظار نشسته است
صبحت بخیر',
            'بلبلان بالای شاخه خواندند آوای عشــــــق
با صدای صبح خندان آواختند آوای عشــــق

کوچ کردند از دیاری شاد و خندان آمدند
در دشت زیبای محبت یافتند آوای عشــــق
صبح بخیر',
            'صبح شد …
آی نمی‌باید خفت
چشم بگشای که خورشید شکفت
باز کن پنجره را با دمِ صبح
باید از خانه‌ی دل
گَرد پریشانی رُفت',
            'من برگ گلم باغ شبستان من است
وآن‌ بلبل خوش‌ لهجه غزلخوان من‌ است
نوباوهٔ شب که شبنمش می‌ خوانند
هر صبح به نیم‌ بوسه مهمان من است',
            'کی میرسد…
آن صُبح
که من صدایِت بزنم
تو بگویی جِانا…',
            'باز هم صبح شد ، از پنجره‌ ی احساسم
نورِ عشقِ تو بر این سینه‌ ی من می تابد…
',
            'تو غافلگیـرِ “ای وای” و کمی صبح
سری زیـر و “بفرمای” و کمی صبح
حیـــاط و میــهمان و عطــــرِ باران
چه حالی میدهد چای و کمی صبح
',
            'غبارِ صبح تماشاست! هرچه باداباد!
تو هم بخند،جهانِ خراب می‌خندد!
',
            'نسیم، عطر تو را
صبح با خودش آورد
و گفت:
روزی عشاق با خداوند است!',
            'صبح
طلوع آفتاب است
در نگاه تو
و من
بزرگترین مزرعه
آفتاب گردان',
            'صبحت
به خیر خورشید !
وقتی که می دمیدی
ماهِ مرا ندیدی…؟!',
            'با یاد تو رقص قاصدکـــــها زیباست
رنگیـــنه کمانی از تماشـــــا زیباست
فنجان صدف، موج و علف، چایی و کف
صبحــــانه ی باران لب دریا زیباست',
            'شادی اش طلوع همه آفتاب هاست
و صبحانه
و نان گرم
و پنجره ای که صبحگاهان
به هوای پاک گشوده می شود
و طراوت شمعدانی ها
در پاشویه ی حوض',
            'صبح است و صبا مشک فشان می‌گذرد
دریاب که از کوی فلان می‌گذرد
برخیز چه خسبی که جهان می‌گذرد
بوئی بستان که کاروان می‌گذرد',
            'می‌ خواهم
هر صبح که پنجره
را باز می‌ کنی
آن درخت روبه‌ رو من باشم
فصل تازه من باشم
آفتاب من باشم
استکان چای من باشم
و هر پرنده‌ای
که نان از انگشتان
تو می‌ گیرد',
            'صبح شد بیدار شو ، مهتاب بر بالین توست
آسمان با آن شکوهش ،بهت این آذین توست
چشم دل را بازکن برنغمه خوان خوش سخن
دلبری ها می کند،هر صبح عطرآگین توست
',
            'صبح است و هوای دل من مثل بهار است
پلکی بزن و صبــح بخیر غــــزلم باش',
            'چای سر صبح را
باید با نگاه تو طعم دار کرد
چه چیزی تلخ تر از قند
در حضور چشم های تو …
',
            'صبح است ساقیا  قدحی پر شراب کن
دور فلک درنگ شتاب ندارد  شتاب کن
زان پیشتر که عالم فانی  شود خراب
مارازجام باده گلگون خراب کن',
            'لبخند خدا یعنی همین باز شدن پلک های هر روزت…
دوباره امروز متولد شدی “آدم”
پس خجسته باد هر روزت…',
            'هر صبح!
پلکهایت؛
فصل جدیدی از زندگی را ورق میزند،
سطر اول همیشه این است:
“خدا همیشه با ماست”
',
            'باز کن پنجره را
من تو راخواهم برد
به سر رود خروشان حیات
آب این رود به سرچشمه نمی گردد باز
بهتر آنست که غفلت نکنیم از آغاز
باز کن پنجره را
صبح دمید',
            'صبحی که با سلام تو آغاز می شود
صد پنجره به باغ غزل باز می شود
',
            'انتخاب با توست!
صبح که می شود می توانی بگویی:
“صبح بخیر خداوند بزرگ…”
یا بگویی:
“خدا بخیر کند باز صبح شده!”',
            'تا دست بر اتفاق بر هم نزنیم
پایی ز نشاط بر سر غم نزنیم

خیزیم و دمی زنیم پیش از دم صبح
کاین صبح بسی دمد که ما دم نزنیم',
            'من و خورشید نشستیم و توافق کردیم
صبح را با تپش قلب تو آغاز کنیم',
            'صبح است و بهار میزند، هی
برخیز که عمر میشود طی

بر بــال نسیم صبــح بنشيـن
از کاسه ی لاله سربکش می

از بیش و کم جهان میندیش
کو جام جم و خزانه ی کی!؟',
            'دوش در خواب، لب نوش تو را بوسیدم
خواب ما بود، بِه از عالم بیداری ما!',
            'هر پنجره، آواز رهایی شده است
غرق عسل، استکان چایی شده است

خورشید شکوفه میزند روی لبم
با نام تو صبحم چه طلایی شده است',
            'صبحت بخیر صبح قشنگ و نجیب تو
پیچیده در حیاط غزل، بوی سیب تو

در قوری تغزل من، دم کشیده عشق
با طعم ناز خاطره های غریب تو',
            'تا سفره ی نان و عسلم باز شود
تا باغ پرندگان پرآواز شود

وا کن مژه ی پنجره ها را به نسیم
لبخند بزن که صبح آغاز شود…!',
        ];
        $instagram = $this->instagram();
        foreach ($searchTags as $tag) {
            if (!isset($_SESSION['tags'])) {
                // ee('ss');
                $tags = $instagram->myGetTagPosts($tag);
                $_SESSION['tags'] = $tags;
            } else {
                // ee('ss1');
                $tags = $_SESSION['tags'];
            }

            if (!isset($tags->tag, $tags->tag, $tags->tag->top_posts->nodes)) {
                return false;
            }
            $lastPost = Telegram_post::where(['user_title' => $tag])->orderBy('id', 'desc')->first();
            $posts = [];
            $nodes = $tags->tag->top_posts->nodes;
            // ee($nodes);
            foreach ($nodes as $node) {
                // ee($node);
                if ($lastPost && $lastPost->media_id == $node->id) {
                    break;
                }
                if ($node->is_video) {
                    continue;
                }
                $post = $this->nodeToPost($node, $typeTags);
                $post['user_title'] = $tag;
                $post['type'] = 'morning';
                $post['text'] = $typeTexts[rand(0, 30)] . " \n " . $typeTags[$tag];
                $posts[] = $post;
                // if (isLocal()) {
                //     break;
                // }

            }
            $posts = array_reverse($posts);
            ee($posts);
            if ($posts) {
                Telegram_post::insert($posts);
                // ee($posts);
            }

            if (!isset($tags->tag, $tags->tag->media, $tags->tag->media->nodes)) {
                return false;
            }
            $lastPost = Telegram_post::where(['user_title' => $tag])->orderBy('id', 'desc')->first();
            $posts = [];
            $nodes = $tags->tag->media->nodes;
            foreach ($nodes as $node) {
                // ee($node);
                if ($lastPost && $lastPost->media_id == $node->id) {
                    break;
                }
                $post = $this->nodeToPost($node, $texts);
                $post['user_title'] = $tag;
                $post['type'] = 'morning';
                $post['text'] = $typeTexts[rand(0, 30)] . " \n " . $typeTags[$tag];
                $posts[] = $post;
                // if (isLocal()) {
                //     break;
                // }

            }
            $posts = array_reverse($posts);
            if ($posts) {
                Telegram_post::insert($posts);
                // ee($posts);
            }
        }
    }
    public function nodeToPost($node, $user = null)
    {
        $instagram = $this->instagram();
        $post = [];
        $post['reply'] = '';
        $post['from'] = 'instagram';
        $extra = [];
        $extra['code'] = $node->code;
        $text = $node->caption ?? '';
        $extra['oldInfo'] = ['text' => $text];
        $post['media_id'] = $node->id;
        $post['user_title'] = $user->user->full_name ?? '';
        $post['user_name'] = $user->user->username ?? '';
        $post['user_id'] = $user->user->id ?? '';
        if (!empty($node->is_video)) {
            $post['type'] = $post['type'] ?? 'video';
            $extra['thumb_src'] = $node->thumbnail_src;
            $filename = $post['user_name'] . '_' . $node->id . ".mp4";
            $post['file_name'] = $filename;
            $filepath = "build/channel_files/instagram/video/$filename";
            if (!file_exists($filepath)) {
                $videoPost = $instagram->myGetMedia($node->code);
                $url = $videoPost->graphql->shortcode_media->video_url;
                // dd($videoPost);
                $file = file_get_contents($url);
                file_put_contents($filepath, $file);
                // ee('up');
                $extra['videos'] = [['url' => $videoPost->graphql->shortcode_media->video_url]];
            }
        } else {
            $post['type'] = $post['type'] ?? 'image';
            $filename = $post['user_name'] . '_' . $node->id . ".jpg";
            $filepath = "build/channel_files/instagram/image/$filename";
            $post['file_name'] = $filename;

            foreach ($node->thumbnail_resources as $image) {
            }
            if (!file_exists($filepath)) {
                $file = file_get_contents($node->display_src);
                file_put_contents($filepath, $file);
                $extra['images'] = [['url' => @$node->display_src]];
            }
        }
        $extra['node'] = $node;
        $post['extra'] = json_encode($extra);
        return $post;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $badWords = ['fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'fldsjfk', 'kos', 'Kir', 'Kiram', 'Koon', 'Pestoon', 'Jakesh', 'Khayeh', 'cos', 'jende', 'keer', 'کس', 'کیر', 'کون', 'پستون', 'سینه', 'جاکش', 'خایه', 'جنده', 'کییر', 'کیییر',
        ];
        $users = [
            'desserts_drinks' => ['content_type' => 'cakelearn'],
            'deser_land' => ['content_type' => 'cakelearn'],
            'cakevideo2' => ['content_type' => 'cake'],
            'cakes_ideas_videos' => ['content_type' => 'cake'],
            //
            // 'Fun2three' => 'instafunvideo',
            // 'Dubamash.ir98' => 'instafunvideo',
            // 'Itsoheil' => 'instafunvideo',
            // 'Sorour_media' => 'instafunvideo',
            // 'goatwow' => 'instafunvideo',
            // 'insanity.videos' => 'instafunvideo',
            // 'humansareamazing' => 'instafunvideo',
            // 'amazing.pictures' => 'instafunvideo',
            // 'amazing.vidss' => 'instafunvideo',
            // 'freeruntv' => 'instafunvideo',
            // 'vinefailsdaily' => 'instafunvideo',
            // 'videosposts' => 'instafunvideo',
            // 'bestvinesnow' => 'instafunvideo',
            // 'vines.failed' => 'instafunvideo',
            // 'lol_vines' => 'instafunvideo',
            // 'funnyvideosvine' => 'instafunvideo',
            // 'funnyvideos.tv' => 'instafunvideo',
            // 'natgeo' => null,
            // 'funny_videos' => 'instafunvideo',
            // 'bestvines' => 'instafunvideo',
            // 'clips' => 'instafunvideo',
            // '5.min.crafts' => 'craft',
            // 'diplycrafty' => 'craft',
            // 'mobilehdwalls' => 'wallpaper',
            // 'mobile_wallpaper_/' => 'wallpaper',
            // 'wallpaper_favourite' => 'wallpaper',

            // '_bbackgrounds' => 'wallpaper',
        ];
        $texts = [
            'cakevideo2' => '#آموزش #سرگرمی',
            'cakes_ideas_videos' => '#ایده_تزیین_کیک',
            'deser_land' => '#آموزش_پخت #متن',
            'desserts_drinks' => '#آموزش_پخت #متن',
            'fun2three' => '#دابسمش',
            'dubsmash.ir98' => '#دابسمش',
            'Itsoheil' => '#دابسمش سهیل',
            'Sorour_media' => '#دابسمش سرور',
            'natgeo' => '#نشنال_جئو_گرافیک',
            'humansareamazing' => "بدون شرح \n #ویدئو_فان",
            'goatwow' => "بدون شرح \n #ویدئو_فان",
            'insanity.videos' => "بدون شرح \n #ویدئو_فان",
            'funny_videos' => 'بدون شرح #ویدئو_فان',
            'bestvines' => 'بدون شرح #ویدئو_فان',
            'clips' => 'بدون شرح #ویدئو_فان',
            '5.min.crafts' => '#ایده',
            'diplycrafty' => '#ایده',
            // '_bbackgrounds' => '#تصویرـزمینه',
            'mobilehdwalls' => '#تصویرـزمینه',
            'mobile_wallpaper_/' => '#تصویرـزمینه',
            'wallpaper_favourite' => '#تصویرـزمینه',
        ];
        // user_name = 'natgeo' or
        // user_name = 'funny_videos' or
        // user_name = 'bestvines' or
        // user_name = 'clips' or
        // user_name = '5.min.crafts' or
        // user_name = 'diplycrafty' or
        // user_name = '_bbackgrounds' or
        // user_name = 'mobilehdwalls' or
        // user_name = 'mobile_wallpaper_/' or
        // user_name = 'wallpaper_favourite'
        // ee(base_path('../likebot.ir/build/channel_files/images/instagram/theimage.jpg'));
        $instagram = $this->instagram();
        // $instagram->myGetTagPosts('googsell');
        foreach ($users as $userId => $type) {
            $maxId = '';
            $i = 0;
            $posts = [];
            $lastPost = Telegram_post::where(['user_name' => $userId])->orderBy('id', 'desc')->first();
            do {
                $user = $instagram->myGetUser($userId, $maxId);
                // ee($user);
                if (!isset($user->user) || !isset($user->user->media) || !isset($user->user->media->nodes) || count($user->user->media->nodes) <= 0) {
                    break;
                }
                $media = $user->user->media;
                $nodes = $media->nodes;
                $hasNextPage = false;
                foreach ($nodes as $node) {
                    if ($lastPost && $node->id == $lastPost->media_id) {
                        break 2;
                    }
                    $post = $this->nodeToPost($node, $user);
                    $post['type'] = $type['type'] ?? $post['type'];
                    $post['content_type'] = $type['content_type'] ?? $post['type'];
                    $post['text'] = $texts[$userId] ?? 'بدون شرح!';
                    $posts[] = $post;
                    if (isLocal()) {
                        break;
                    }
                }
                $i++;
                if (@$media->page_info->has_next_page == 'true') {
                    $hasNextPage = true;
                    $maxId = $media->page_info->end_cursor;
                }
                if (isLocal()) {
                    break;
                }
            } while (!$lastPost && $i < 10 && $hasNextPage);
            $posts = array_reverse($posts);
            if ($posts) {
                Telegram_post::insert($posts);
                // ee($posts);
            }
        }
    }

    public function renewNodeToPost($node, $user = null)
    {
        $instagram = $this->instagram();
        $post = [];
        $post['reply'] = '';
        $post['from'] = 'instagram';
        $extra = [];
        $extra['code'] = $node->code;
        $text = $node->caption ?? '';
        $extra['oldInfo'] = ['text' => $text];
        $post['media_id'] = $node->id;
        $post['user_title'] = $user->user->full_name ?? '';
        $post['user_name'] = $user->user->username ?? '';
        $post['user_id'] = $user->user->id ?? '';
        if (!empty($node->is_video)) {
            $post['type'] = $post['type'] ?? 'video';
            $extra['thumb_src'] = $node->thumbnail_src;
            $oldFilename = $node->id . ".mp4";
            $filename = $post['user_name'] . "_" . $node->id . ".mp4";
            $post['file_name'] = $filename;
            $filepath = "build/channel_files/instagram/video/$filename";
            $oldFilepath = "build/channel_files/instagram/video/$oldFilename";
            $videoPost = $instagram->myGetMedia($node->code);
            $url = $videoPost->graphql->shortcode_media->video_url;
            if (!file_exists($oldFilepath) && !file_exists($filepath)) {
                // dd($videoPost);
                $file = file_get_contents($url);
                file_put_contents($filepath, $file);
                // ee('up');
            }
            $extra['videos'] = [['url' => $videoPost->graphql->shortcode_media->video_url]];
        } else {
            $post['type'] = $post['type'] ?? 'image';
            $filename = $node->id . ".jpg";

            $oldFilename = $node->id . ".jpg";
            $filename = $post['user_name'] . "_" . $node->id . ".jpg";
            $post['file_name'] = $filename;

            foreach ($node->thumbnail_resources as $image) {
            }
            $oldFilepath = "build/channel_files/instagram/image/$oldFilename";
            $filepath = "build/channel_files/instagram/image/$filename";
            if (!file_exists($oldFilepath) && !file_exists($filepath)) {
                $file = file_get_contents($node->display_src);
                file_put_contents($filepath, $file);
            }
            $extra['images'] = [['url' => @$node->display_src]];
        }
        $extra['node'] = $node;
        $post['extra'] = json_encode($extra);
        return $post;
    }
    /**
     * renew crashed urls
     */
    public function renewUrls()
    {
        $badWords = ['Kos', 'Kir', 'Kiram', 'Koon', 'Pestoon', 'Jakesh', 'Khayeh', 'cos', 'jende', 'keer', 'کس', 'کیر', 'کون', 'پستون', 'سینه', 'جاکش', 'خایه', 'جنده', 'کییر', 'کیییر',
        ];
        $users = [

            // 'natgeo' => null,
            // 'funny_videos' => 'instafunvideo',
            // 'bestvines' => 'instafunvideo',
            // '5.min.crafts' => 'craft',
            // 'diplycrafty' => 'craft',
            // 'mobilehdwalls' => 'wallpaper',
            // 'mobile_wallpaper_/' => 'wallpaper',
            // 'wallpaper_favourite' => 'wallpaper',
        ];
        $texts = [
            'natgeo' => '#نشنال_جئو_گرافیک',
            'humansareamazing' => "بدون شرح \n #ویدئو_فان",
            'goatwow' => "بدون شرح \n #ویدئو_فان",
            'insanity.videos' => "بدون شرح \n #ویدئو_فان",
            'funny_videos' => 'بدون شرح #ویدئو_فان',
            'bestvines' => 'بدون شرح #ویدئو_فان',
            'clips' => 'بدون شرح #ویدئو_فان',
            '5.min.crafts' => '#ایده',
            'diplycrafty' => '#ایده',
            // '_bbackgrounds' => '#تصویرـزمینه',
            'mobilehdwalls' => '#تصویرـزمینه',
            'mobile_wallpaper_/' => '#تصویرـزمینه',
            'wallpaper_favourite' => '#تصویرـزمینه',
        ];
        $instagram = $this->instagram();
        foreach ($users as $userId => $type) {
            $maxId = '';
            $i = 0;
            $posts = [];
            do {
                $user = $instagram->myGetUser($userId, $maxId);
                // ee($user);
                if (!isset($user->user) || !isset($user->user->media) || !isset($user->user->media->nodes) || count($user->user->media->nodes) <= 0) {
                    break;
                }
                $media = $user->user->media;
                $nodes = $media->nodes;
                $hasNextPage = false;
                foreach ($nodes as $node) {
                    $exists = Telegram_post::where(['from' => 'instagram', 'media_id' => $node->id])->where(function ($q) {
                        return $q->where('status', 'new')->orWhere('status', 'active');
                    })->whereNull('file_name')->get()->first();
                    if ($exists) {
                        $post = $this->renewNodeToPost($node, $user);
                        $post['type'] = $type ?? $post['type'];
                        $post['text'] = $texts[$userId] ?? 'بدون شرح!';
                        $posts[] = $post;
                    }
                }
                $i++;
                if (@$media->page_info->has_next_page == 'true') {
                    $hasNextPage = true;
                    $maxId = $media->page_info->end_cursor;
                }
            } while ($i < 10 && $hasNextPage);
            $posts = array_reverse($posts);
            // ee($posts);
            if ($posts) {
                Telegram2_post::insert($posts);
                // ee($posts);
                // exit;
            }
        }
    }
    /**
     * @route: cron/instagram/sendSweetVideo
     */
    public function sendSweetVideo(Request $request)
    {
        $current_time = date('H', time());
        $times = [9, 11, 14, 17, 20, 22, 0];
        if ($current_time > 1 && $current_time < 9) {
            return 'false';
        }
        if (!in_array($current_time, $times)) {
            return 'false';
        }
        $types = [
            9 => 'cake',
            11 => 'cake',
            // 14 => 'cake',
            17 => 'cake',
            20 => 'cakelearn',
            22 => 'cake',
            0 => 'cake',
        ];
        $type = $_GET['type'] ?? $types[$current_time] ?? 'cake';
        $posts = Telegram_post::where('status', 'active')->where('content_type', $type)
            ->orderByRaw('rand()')
            ->limit(1)->get();
        // ee($type, 0);
        // ee($posts);
        $channel = channel('sweet_video');
        $res = Telegram_post::sendTelegram($posts, $channel);

        ee($res);
    }
    /**
     * Route: cron/twitter/send
     */
    public function sendToTelegramWallpaper(Request $request)
    {
        return false;

        $current_time = date('H', time());
        if ($current_time != 11 && $current_time != 17) {
            return 'false' . $current_time;
            exit;
        }
        $posts = Telegram_post::where('status', '!=', 'sent')
            ->where('type', 'wallpaper')
            ->orderByRaw('rand()')
            ->limit(1)->get();
        $telegram = new Api();
        $channel = '@best_of_social';
        foreach ($posts as $post) {
            $data['chat_id'] = $channel;
            $tag = '#تصویرـزمینه';
            //
            $sign = "\n \n بهترین های شبکه های اجتماعی 👇";
            $sign .= "\n $channel";
            $extra = json_decode($post->extra);
            $media = $extra->images;
            $data['caption'] = $tag . $sign;
            $img = Image::make($media[0]->url);
            // ee(base_path('../likebot.ir'));

            $img->trim('right', ['left', 'right'], 15)->save('temp.jpg');
            // $image = new \InputFile($img);
            $data['photo'] = 'temp.jpg';
            $telegram->sendPhoto($data);

            $data['caption'] = $tag . " #کیفیتـاصلی" . $sign;
            $data['document'] = 'temp.jpg';
            $telegram->sendDocument($data);
            // unlink('temp.jpg');
            $post->update(['status' => 'sent']);

        }
    }

    public function fillFileNames(Request $request)
    {

        if (file_exists('build/channel_files/instagram/video/1662244481153506291.mp4')) {
            ee('$');
        }
    }
}
