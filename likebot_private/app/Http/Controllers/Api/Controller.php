<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\MyController;

// use Illuminate\Routing\Controller as BaseController;

class Controller extends MyController
{
    // use myController;
    // public function __construct()
    // {
    //     self::parent();
    // }
    // public function json_return($data)
    // {
    //     return response()->json($data, $data['code']);
    // }

    public function _error($data = '', $status = 'false', $callback = null, $errCode = 400)
    {
        return $this->response_error($data, $status, $callback, $errCode);
    }

    public function _success($data = '', $callback = null, $status = 'OK', $errCode = 200)
    {
        return $this->response_success($data, $callback, $status, $errCode);
    }

    public function _form_error($data = '', $status = 'formError', $callback = null, $errCode = 400)
    {
        return $this->response_form_error($data, $status, $callback, $errCode);
    }
}
