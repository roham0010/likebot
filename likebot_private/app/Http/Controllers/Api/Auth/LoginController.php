<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        if (isset($_POST['username']) && is_numeric($_POST['username'])) {
            return 'mobile';
        }
        return 'email';
    }

    /**
     * create access token
     * @Route: /token/create
     */
    public function createAccessToken(Request $request)
    {
        $request->merge([$this->username() => $request->username]);
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {

            // $user = \App\User::where(['username' => $request->username, 'password' => bcrypt($request->password)])->first();
            $user = $request->user();
            $array = ['access_token' => $user->createToken('bb')->accessToken];
            return json_encode($array);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->api_form_error(['message' => trans('auth.wrong email or password')]);
    }

    protected function validateLoginWithCode(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
        ]);
    }
    /**
     * @Route: /token/create/withCode
     */
    public function createAccessTokenWithCode(Request $request)
    {
        $this->validateLoginWithCode($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            // ee('fsdfd');
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($request->code >= 100000 && $user = \App\User::where('extension_code', $request->code)->first()) {
            // $user = \App\User::where(['username' => $request->username, 'password' => bcrypt($request->password)])->first();
            $user->update(['extension_code' => 0]);
            $array = ['access_token' => $user->createToken('bb')->accessToken];
            return $this->_success($array, 'p_setAccessTokenp');
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->_form_error(['message' => trans('auth.wrong email or password'), 'errors' => ['code' => 'کد صحیح نیست']]);
    }

}
