<?php

namespace App\Http\Controllers\Admin;

use App\Telegram_post;
use Illuminate\Http\Request;

class TelegramController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function all(Request $request)
    {
        $status = $_GET['status'] ?? 'new';
        $posts = Telegram_post::where('status', $status);
        if ($request->has('type')) {
            $posts->where('type', $request->type);
        }if ($request->has('content_type')) {
            $posts->where('content_type', $request->content_type);
        }
        $posts = $posts->paginate(50);

        $counts = new \stdClass();
        $counts->active = Telegram_post::where('status', 'active')->count();
        $counts->text = Telegram_post::where(['status' => 'active', 'type' => 'text'])->count();
        $counts->instafunvideo = Telegram_post::where(['status' => 'active', 'type' => 'instafunvideo'])->count();
        $counts->image = Telegram_post::where(['status' => 'active', 'type' => 'image'])->count();
        $counts->video = Telegram_post::where(['status' => 'active', 'type' => 'video'])->count();
        return view('admin.telegram.all', compact('posts', 'counts'));
    }
    public function sentPostsFix()
    {
        $posts = Telegram_post::where('content_type', 'cake')->where('extra', 'like', "%sentData%")->update(['status'=> 'sent']);
        ee($posts);
        // foreach ($posts as $post) {
        //     $post->update(['status'=> 'sent']);
        // }
    }
    public function tests(Request $request)
    {
        foreach ($groupForwards as $groupMessage) {
            // ee($groupMessage['message']);
            $groupMessage = json_decode($groupMessage['a']);
            // print_r($groupMessage);
            // exit;
            $groupId = @$groupMessage->message->chat->id;
            if (!empty($groupId) && !in_array($groupId, $groupToForwardKeys)) {
                // $messageId = $message['message_id'];
                // $data = ['chat_id' => $groupId, 'from_chat_id' => '@' . $message['chat']['username'], 'message_id' => $messageId];
                // // ee($groupMessage['message']['chat'], 0);
                // $result = $forwardBot->forwardMessage($data);
                // if (isset($result['parameters']['migrate_to_chat_id']) && empty($result['ok'])) {
                //     $data['chat_id'] = $result['parameters']['migrate_to_chat_id'];
                //     $result = $forwardBot->forwardMessage($data);
                // }
                $groupToForwardKeys[] = $groupId;
            }
        }
        print_r($groupToForwardKeys);
        exit;
        $status = $_GET['status'] ?? 'new';
        $tests = \App\Test::orderBy('id', 'desc')->paginate(50);
        return view('admin.tests.all', compact('tests'));
    }
    public function delete(Request $request)
    {
        $actions = [["#row-post-$request->post_id", 'delete']];
        if (Telegram_post::where('id', $request->post_id)->update(['status' => 'deactive'])) {
            $res = ['actions' => $actions];
            return $this->_success($res);
        } else {
            $res = ['actions' => $actions, 'message' => 'error'];
            return $this->_error($res);
        }
    }
    public function active(Request $request)
    {

        $actions = [["#row-post-$request->post_id", 'delete']];
        $post = Telegram_post::where('id', $request->post_id)->get()->first();
        $contentChannels = ['cake' => 'sweet_video'];
        if ($request->has('immidiatelysend')) {
            $post->text = $request->text;
            $post->save();
            $channel = $contentChannels[$post->content_type] ?? 'best_of_social';
            $channel = channel($channel);
            Telegram_post::sendTelegram([$post], $channel);
            $res = ['actions' => $actions];
            return $this->_success($res);
        } else {

            if ($post->update(['status' => 'active', 'text' => $request->text])) {
                $res = ['actions' => $actions];
                return $this->_success($res);
            } else {
                $res = ['message' => 'error'];
                return $this->_error($res);
            }
        }
    }

    public function fillFileNames()
    {
        $path = "build/channel_files/";
        $posts = Telegram_post::where(['from' => 'telegram'])->get();
        foreach ($posts as $post) {
            $extra = json_decode($post->extra);
            $instaType = isset($extra->images) ? 'image' : 'video';
            $extension = isset($extra->images) ? 'jpg' : 'mp4';
            $fileName = $post->media_id . "." . $extension;
            $filePath = $path . "$post->from/$instaType/$fileName";
            // ee($filePath);
            if (file_exists($filePath)) {
                $post->update(['file_name' => $fileName]);
            }
        }
    }
}
