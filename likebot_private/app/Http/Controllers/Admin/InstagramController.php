<?php

namespace App\Http\Controllers\Admin;

use App\Telegram2_post;
use App\Telegram_post;

class InstagramController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function fillFileNames()
    {
        $path = "build/channel_files/";
        $posts = Telegram_post::where(['from' => 'instagram'])->get();
        foreach ($posts as $post) {
            $extra = json_decode($post->extra);
            $instaType = isset($extra->images) ? 'image' : 'video';
            $extension = isset($extra->images) ? 'jpg' : 'mp4';
            $fileName = $post->media_id . "." . $extension;
            $filePath = $path . "$post->from/$instaType/$fileName";
            // ee($filePath);
            if (file_exists($filePath)) {
                $post->update(['file_name' => $fileName]);
            }
        }
    }

    public function newFileNames()
    {
        $posts = Telegram_post::where(['from' => 'instagram'])->where(function ($q) {
            return $q->where('status', 'new')->orWhere('status', 'active');
        })->whereNull('file_name')->get();
        foreach ($posts as $post) {
            $post2 = Telegram2_post::where('media_id', $post->media_id)->get()->first();
            if ($post2) {
                $updates = $post2->only(['file_name', 'extra']);
                $post->update($updates);
            }
        }
    }

}
