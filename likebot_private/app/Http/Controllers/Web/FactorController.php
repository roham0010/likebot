<?php

namespace App\Http\Controllers\Web;

use App\Factor;
use Illuminate\Http\Request;
use Larabookir\Gateway\Zarinpal\Zarinpal;

class FactorController extends Controller
{
    /**
     * @Route: /profile/payments/callback
     */
    public function paymentsCallback(Request $request)
    {
        $transactionId = $request->transaction_id;
        $factorPayed = Factor::where('gateway_id', $transactionId)->first();
        try {
            $gateway = \Gateway::verify();
            $trackingCode = $gateway->trackingCode();
            $resultFlag = 'success';
            $message = 'پرداخت با موفقیت انجام شد';
            if ($factorPayed) {
                $factorUpdate = $factorPayed->update(['status' => 'payed', 'tracking_code' => $trackingCode]);
                $userUpldate = user()->update(['like_count' => \DB::raw('like_count + ' . $factorPayed->likes)]);
                if (!$factorUpdate || !$userUpldate) {
                    $resultFlag = 'db_error';
                    $message = 'خطا در تایید تراکنش لطفا در صورتی که پلن مربوطه خریداری نشده با ما در تماس باشید';
                }
            } else {
                $resultFlag = 'no_factor';
                $message = 'فاکتوری برای این پرداخت ثبت نشده لطفا با کد پیگیری با ما در تماس باشید';
            }
            if ($factorPayed && $resultFlag != 'success') {
                $factorPayed->update(['status' => 'payed', 'tracking_code' => $trackingCode]);
            }

        } catch (\Larabookir\Gateway\Exceptions\RetryException $e) {
            $resultFlag = 'refresh';
            $message = 'تایید - پرداخت با موفقیت انجام شد';
        } catch (\Exception $e) {
            $resultFlag = 'bank_error';
            $trackingCode = $transactionId;
            if ($factorPayed) {
                $factorUpdate = $factorPayed->update(['status' => 'failed']);
            }
            $message = $e->getMessage();
        }
        return view('payment-callback', compact('resultFlag', 'trackingCode', 'message'));
    }
    public function createFactorDataByPlanId($planId)
    {
        $user = user();
        $plans = consts('plans');
        $discounts = consts('discounts');
        $plan = $plans[$planId];
        $discountId = $plan['discount_id'];
        // $flagDiscounted = false;
        $factorData['user_id'] = $user->id;
        $factorData['status'] = 'init';
        $factorData['likes'] = $plan['likes'];
        $factorData['price'] = $plan['price'];
        if ($discountId > 0 && $discount = $discounts[$discountId]) {
            // $flagDiscounted = true;
            if ($discount['price'] > 0 && $discount['price'] < $plan['price']) {
                $factorData['price'] = $discount['price'];
                $factorData['real_price'] = $plan['price'];
            }
            if ($discount['likes'] > 0 && $discount['likes'] > $plan['likes']) {
                $factorData['likes'] = $discount['likes'];
                $factorData['real_likes'] = $plan['likes'];
            }
            $factorData['discount_description'] = $discount['description'];
        }
        return $factorData;
    }
    /**
     * @Route: /pricing/apply
     */
    public function applyPricing(Request $request)
    {
        $user = user();
        $plans = consts('plans');
        // $discounts = consts('discounts');
        $planId = $request->planId;
        $factorData = [];
        if (isset($plans[$planId])) {
            $factorData = $this->createFactorDataByPlanId($planId);
            try {
                // ee($factorData);
                $gateway = \Gateway::make(new Zarinpal());
                $gateway->setCallback(myRoute('profile.payments.callback'));
                $gateway->price($factorData['price'] * 10)->ready();
                // $refId = $gateway->refId(); // شماره ارجاع بانک
                // $transID = ; // شماره تراکنش
                $factorData['gateway_id'] = $gateway->transactionId();
                // ee($factorData);
                Factor::create($factorData);
                return $gateway->redirect();
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        } else {
            \Session::flash('message', trans('cart.choose_a_plan'));
            return myRedirect(myRoute('pricing.pricing'));
        }
    }
    public function payments()
    {
        $user = user();
        $payments = Factor::where('user_id', $user->id)->paginate(10);
        return view('user.payments', compact('payments'));
    }
}
