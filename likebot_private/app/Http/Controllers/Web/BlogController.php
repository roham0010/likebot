<?php

namespace App\Http\Controllers\Web;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Show All User blogs
     * @Route get: /blog or /blog/cat/{title}-{id}
     */
    public function all(Request $request, $category = '')
    {
        $callback = "blog_posts";
        $data = [];
        // $blog = new blog;
        // updateMaker($blog->getColumns(),'blogs','blog');
        $posts = Blog::select('blogs.*', \DB::raw('(select count(*) from comments where comments.blog_id=blogs.id and comments.status = 0) as blog_comments_count'))->where('blogs.status', 0)->orderBy('blogs.id', 'desc');
        if (!empty($category)) {
            $category = explode('-', $category);
            $categoryId = end($category);
            $blog->where('blogs.type', $categoryId);
        }
        // ->addSelect('blogs.*',DB::raw('(select sender_id from blogs where blogs.blog_id=blogs.id order by blogs.id desc) as sender_id'))
        $posts = $posts->paginate(6);
        for ($i = 0; $i < sizeof($posts); $i++) {
            $posts[$i]->url = myUrl('blog/' . url_optimizer($posts[$i]->title) . '-' . $posts[$i]->id);
            $posts[$i]->cat = trans('blog.cats.' . $posts[$i]->type);
            $posts[$i]->catUrl = myUrl('blog/cat/' . url_optimizer(trans('blog.cats.' . $posts[$i]->type) . '-' . $posts[$i]->type));
            $posts[$i]->tag_titles1 = explode(',', $posts[$i]->tag_titles);
            // print_r($posts[$i]->url);exit;
            $tags = '';
            for ($j = 0; $j < sizeof($posts[$i]->tag_titles1); $j++) {
                $tags .= ", " . $posts[$i]->tag_titles1[$j];
            }
            $tags = trim($tags, ' ,');
            $posts[$i]->tags = $tags;
        }
        // addAgoDate($posts);
        if ($request->input('ajax')) {
            $pagination = (string) $pagination;
            return _success($callback, ['data' => $data]);
        }
        return view('blog.all', compact('posts'));
    }
    /**
     * Show one post
     * @Route get: /blog/{title}-{id}
     */
    public function one(Request $request, $titleId)
    {
        $headers = new \stdClass;
        $callback = "blog_post";
        $postId = get_id($titleId);
        if (!$postId) {
            return redirect('blog');
        }
        $post = Blog::with(['user', 'comments' => function ($q) {
            $q->where('status', 0)->orderBy('comments.id', 'desc');
        }])->where('blogs.id', $postId)->get()->first();

        if (!isset($_COOKIE['v_' . $post->id])) {
            setcookie('v_' . $post->id, true, time() + 3600);
            $post->visit_count += 1;
            $post->save();
        }
        $post->url = url('blog/' . url_optimizer($post->title) . '-' . $post->id);
        $post->cat = trans('blog.cats.' . $post->type);
        $post->catUrl = url('blog/cat/' . url_optimizer(trans('blog.cats.' . $post->type) . '-' . $post->type));
        $post->image = files('blog', '/') . '/' . $post->id . '.jpg';
        $post->tag_titles1 = explode(',', $post->tag_titles);
        $tags = '';
        for ($j = 0; $j < sizeof($post->tag_titles1); $j++) {
            $tags .= $post->tag_titles1[$j] . " ,";
        }
        $tags = trim($tags, ' ,');
        $post->tags = $tags;
        $headers->canonical = $post->url;
        if (!empty($tags)) {
            $headers->keywords = $tags;
        }

        $headers->openGraph = new \stdClass;
        $headers->openGraph = (object) [
            'type' => 'blog',
            'title' => $post->title,
            'description' => $post->abstract,
            'url' => $post->url,
            'image' => $post->image,
        ];

        // addAgoDate($post);
        if ($request->input('ajax')) {
            return _success($callback, ['data' => ['post' => $post]]);
        }
        return view('blog.one', compact('post', 'comments', 'headers'));
    }

//
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    public function checkOwner($blog_user_id, $callback)
    {
        $ajax = \Request::input('ajax') ? true : false;
        if (!in_array(Auth::user()->type, (array) constants("user_type.admin"))) {
            return _error($callback, 78, trans("messages.error.access_denied"));
        }

    }
    public function update(Request $request, blog $blog)
    {
        $callback = 'blog_add';
        if ($this->checkOwner($blog->user_id, $callback)) {
            return $this->checkOwner($product->user_id, $callback);
        }

        $validationFields = [
            "full_text" => "required|string",
            "abstract" => "required|string|max:500",
            "title" => "required|string|max:64",
            "type" => "required|integer|max:20"];
        $validation = Validator::make($request->input(), $validationFields);
        $inputData = [];
        if (!empty($request->input("tags"))) {
            $oldTagsTitles = [];
            $oldTagsIds = [];
            $tagTitles = explode(' ', trim($request->input("tags")));
            // $tagTitles = array_merge($tagTitles, explode(',', $request->input("tags")));
            foreach ($tagTitles as &$tag) {
                $tag = trim($tag);
            }
            // ee($tagTitles);
            $oldTags = Tag::whereIn('title', $tagTitles)->get();
            foreach ($oldTags as $oldTag) {
                $oldTagsTitles[] = $oldTag->title;
                $oldTagsIds[] = $oldTag->id;
            }

            $newTags = array_diff($tagTitles, $oldTagsTitles);

            $tagData = array();
            foreach ($newTags as $newTag) {
                $newTag = trim($newTag);
                if (!empty($newTag)) {
                    $tagData[] = array('title' => $newTag);
                }
            }

            if (Tag::insert($tagData)) {
                $mergedTags = Tag::whereIn('title', $tagTitles)->get();
                $tagTitles = $tagIds = array();
                foreach ($mergedTags as $mergedTag) {
                    $tagTitles[] = $mergedTag->title;
                    $tagIds[] = $mergedTag->id;
                }
            } else {
                $tagTitles = $oldTagsTitles;
                $tagIds = $oldTagsIds;
            }
            $inputData['tag_titles'] = implode(',', $tagTitles);
            $inputData['tag_ids'] = implode(',', $tagIds);
        }
        // print_r($inputData);exit;

        foreach ($request->input() as $key => $value) {
            if (in_array($key, $blog->getColumns()) && $value != $blog->{$key}) {
                if ($value == 'on') {
                    $value = 1;
                }

                $inputData[$key] = $value;
            }
        }

        if (!$validation->fails()) {
            if (isset($blog->id) && !empty($blog->id)) {
                // if (!empty($request->file('photo')) && $request->file('photo')->isValid()) {
                //     $file = $request->file('photo');
                //     $fileName = $request->file('photo')->getClientOriginalName();
                //     // ee($file);
                //     if (strpos($fileName, '.jpg') === false) {
                //         $func = "imagecreatefromjpeg";
                //     } else {
                //         $func = "imagecreatefrompng";
                //     }

                //     $im = $func('pics/' . $fileName);

                //     $marge_right = 10;
                //     $marge_bottom = 100;
                //     $sx = imagesx($stamp);
                //     $sy = imagesy($stamp);
                //     $newwidth = imagesx($im) * 0.041;
                //     $newheight = imagesx($im) * 0.041;
                //     $stamp1 = imagecopyresized($im,
                //         $stamp,
                //         imagesx($im) / 40,
                //         (imagesy($im) / 3),
                //         0,
                //         0,
                //         $newwidth,
                //         $newheight * 4.68,
                //         imagesx($stamp),
                //         imagesy($stamp)
                //     );
                //     imagecopy($im, $stamp1, imagesx($im) / 47, (imagesy($im) / 32), 0, 0, imagesx($stamp), (imagesy($stamp)));
                //     ee($im, 0);
                //     ee($request->file('photo'));

                //     $request->file('photo')->move(blog_path(), $blog->id . '.jpg');
                // }
                if (!empty($request->file('photo')) && $request->file('photo')->isValid()) {
                    if (strpos($request->file('photo')->getClientOriginalName(), '.jpg') !== false) {
                        $func = "imagecreatefromjpeg";
                    } else {
                        $func = "imagecreatefrompng";
                    }

                    // ee($func);
                    // ee($request->file('photo')->getRealPath());
                    // ee(base_path());
                    $stamp = imagecreatefrompng(my_path('build/img/water-mark-sign.png'));
                    // ee($stamp1);
                    $im = $func($request->file('photo')->getRealPath());

                    $marge_right = 10;
                    $marge_bottom = 10;
                    $newwidth = imagesx($im) * 0.041;
                    $newheight = imagesx($im) * 0.041;
                    imagecopyresized($im,
                        $stamp,
                        imagesx($im) / 40,
                        ((imagesy($im) / 2) - (imagesy($stamp) / 2)),
                        0,
                        0,
                        $newwidth,
                        $newheight * 4.68,
                        imagesx($stamp),
                        imagesy($stamp)
                    );
                    // $im->move(blog_path(), $blog->id.'.jpg');
                    // $request->file('photo')->move(blog_path(), $blog->id.'.jpg');
                    imagejpeg($im, blog_path() . '/' . $blog->id . '.jpg');
                }
                if ($blog->update($inputData)) {
                    return _success($callback, ['blog' => $blog]);
                } else {
                    return _error($callback, 78, trans("messages.unknown"));
                }

            } else {
                $inputData['user_id'] = Auth::user()->id;
                if ($blog = blog::firstOrCreate($inputData)) {

                    if (!empty($request->file('photo')) && $request->file('photo')->isValid()) {
                        if (strpos($request->file('photo')->getClientOriginalName(), '.jpg') !== false) {
                            $func = "imagecreatefromjpeg";
                        } else {
                            $func = "imagecreatefrompng";
                        }

                        // ee($func);
                        // ee($request->file('photo')->getRealPath());
                        // ee(base_path());
                        $stamp = imagecreatefrompng(my_path('build/img/water-mark-sign.png'));
                        // ee($func);
                        $im = $func($request->file('photo')->getRealPath());

                        $marge_right = 10;
                        $marge_bottom = 10;
                        $newwidth = imagesx($im) * 0.041;
                        $newheight = imagesx($im) * 0.041;
                        imagecopyresized($im,
                            $stamp,
                            imagesx($im) / 40,
                            ((imagesy($im) / 2) - (imagesy($stamp) / 2)),
                            0,
                            0,
                            $newwidth,
                            $newheight * 4.68,
                            imagesx($stamp),
                            imagesy($stamp)
                        );
                        $desired_width = $width = imagesx($im);
                        $desired_height = $height = imagesy($im);
                        while ($desired_width > 300) {
                            $desired_width *= 0.10;
                            $desired_height *= 0.10;
                        }
                        /* create a new, "virtual" image */
                        $thumb_image = imagecreatetruecolor($desired_width, $desired_height);

                        /* copy source image at a resized size */
                        imagecopyresampled($thumb_image, $im, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
                        imagejpeg($thumb_image, blog_path() . '/thumbs/' . $blog->id . '.jpg');

                        imagejpeg($im, blog_path() . '/' . $blog->id . '.jpg');
                    }
                    return _success($callback, ['blog' => $blog]);
                } else {
                    return _error($callback, 78, trans("messages.unknown"));
                }

            }
        } else {
            return _form_error($callback, $validation->errors());
        }
    }

    public function delete(Request $request, blog $blog)
    {
        // print_r($blog);exit;
        $callback = 'blog_delete';
        $ajax = $request->input('ajax') ? true : false;
        if (Auth::user()->id != $blog->user_id && !in_array(Auth::user()->type, (array) constants("user_type.admin"))) {
            return _error($callback, 79, trans("messages.error.access_denied"));
        }

        if ($blog->update(['status' => constants('statuses.user.delete')])) {
            return _success($callback, ['blog' => $blog], trans('messages.success.delete'));
        } else {
            return _error($callback, 79, trans("messages.unknown"));
        }

    }
    /**
     * Show All User blogs
     */
    public function blogsUser(blog $blog, Request $request)
    {

        $data = [];
        if ($this->checkOwner($blog->user_id, '')) {
            return $this->checkOwner($blog->user_id, '');
        }

        $blog = new blog;
        // updateMaker($blog->getColumns(),'blogs','blog');
        $blogs = $blog->blogs()->join('users as sender', 'sender.id', '=', 'blogs.sender_id') //->join('users as user','user.id','=','blogs.user_id')
            ->select('blogs.*'
                , 'sender.instagram_profile_picture_url as sender_picture'
                // , 'user.instagram_profile_picture_url as user_picture'
                , DB::raw('concat(sender.first_name," ",sender.last_name) as sender_name') /*, DB::raw('concat(user.first_name," ",user.last_name) as user_name')*/);
        $blogs = $blogs->paginate(constants('pagination.user.perPage'));
        // print_r($blogs);exit;
        $pagination = $blogs->links();
        // print_r($blogs);exit;
        $data['pagination'] = $pagination;
        $data['blogs'] = $blogs;
        $data['blog'] = $blog;
        return view('blogs.blogs_user', $data);
    }

    /**
     * Show All User blogs
     */
    public function post(Request $request, $post)
    {
        $headers = new \stdClass;
        $callback = "blog_post";
        $data = [];
        $blog = new blog;
        $postId = get_id($post);
        // $blog = Blog::find($postId);
        if (!$postId) {
            return redirect('blog');
        }

        $blog = $blog->with(['user', 'comments' => function ($q) {$q->where('status', 0)->orderBy('comments.id', 'desc');}])->where('blogs.id', $postId)->get()->first();
        // ee($blog);
        // $blog = $blog->users()->select('users.*','blogs.*')->where('blogs.status', 0)->where('blogs.id', $postId)->get()->first();
        // // print_r($blog);exit;
        // if(!$blog)
        //     return redirect('blog');
        // $comments=$blog->comments()->select('comments.*', 'users.first_name', 'users.last_name', 'users.instagram_profile_picture_url')->get();
        // ee($blog->comments);
        addAgoDate($blog->comments);
        if (!isset($_COOKIE['v_' . $blog->id])) {
            setcookie('v_' . $blog->id, true, time() + 3600);
            $blog->visit_count += 1;
            $blog->save();
        }
        $blog->url = url('blog/' . url_optimizer($blog->title) . '-' . $blog->id);
        $blog->cat = trans('blog.cats.' . $blog->type);
        $blog->catUrl = url('blog/cat/' . url_optimizer(trans('blog.cats.' . $blog->type) . '-' . $blog->type));
        $blog->image = blog_file_url('/') . '/' . $blog->id . '.jpg';
        $blog->tag_titles1 = explode(',', $blog->tag_titles);
        $tags = '';
        for ($j = 0; $j < sizeof($blog->tag_titles1); $j++) {
            $tags .= $blog->tag_titles1[$j] . " ,";
        }
        $tags = trim($tags, ' ,');
        $blog->tags = $tags;
        $headers->canonical = $blog->url;
        if (!empty($tags)) {
            $headers->keywords = $tags;
        }

        $headers->openGraph = new \stdClass;
        $headers->openGraph = (object) [
            'type' => 'blog',
            'title' => $blog->title,
            'description' => $blog->abstract,
            'url' => $blog->url,
            'image' => $blog->image,
        ];

        addAgoDate($blog);
        if ($request->input('ajax')) {
            return _success($callback, ['data' => ['post' => $blog]]);
        }

        // $blogs=$blogs->getAttribute();

        $data = ['post' => $blog, 'comments' => $blog->comments, 'headers' => $headers];
        return view('blog.blog-single', $data);
    }
    /**
     * comment for a blog by user
     */
    public function commentAdd(Request $request, blog $blog)
    {
        $callback = 'comment_add';
        if (limitRequest($callback)) {
            return limitRequest($callback);
        }

        $validation = Validator::make($request->input(), ["description" => "required|string|max:2048",
            "user_name" => "sometimes|required|string|max:32",
        ]);
        if (!$validation->fails()) {
            limitRequestTimes($callback);
            $commentClass = new Comment;
            $commentData = ['status' => 1, 'description' => $request->description, 'user_name' => @$request->user_name, 'user_id' => @Auth::user()->id, 'blog_id' => $blog->id];
            if (Auth::check() && Auth::user()->type == 4) {
                $commentData['status'] = 0;
            }
            if ($comment = comment::create($commentData)) {
                $comments = $commentClass->with(['user'])->where('comments.id', $comment->id)->get();
                //  => function($q){ $q->addSelect('users.first_name', 'users.last_name', 'users.instagram_profile_picture_url');}
                // ee($comments);
                // $comment->status = 1;
                // $comment->save();
                addAgoDate($comments);
                $actions = [['#description', 'val', '']];
                if (!Auth::check()) {
                    $actions[] = ['#user_name', 'val', ''];
                }
                return _success($callback, ['actions' => $actions, 'data' => ['comment' => $comments]], trans('messages.show_after_check'));
            } else {
                return _error($callback, 79, trans("messages.unknown"));
            }

        } else {
            return _form_error($callback, $validation->errors());
        }

    }
    /**
     * comment for a blog by user
     */
    public function like(Request $request, blog $blog)
    {

        if (!auth::check()) {
            return _error('login_needed', 79, trans("messages.login_neded"));
        }

        $callback = 'like_blog';
        $like = new Like;
        if (!$like->where('type', constants('like_type.blog'))->where('item_id', $blog->id)->where('user_id', Auth::user()->id)->count()) {
            if (like::create(['type' => constants('like_type.blog'), 'user_id' => Auth::user()->id, 'item_id' => $blog->id])) {
                $blog->like_count += 1;
                $blog->save();
                return _success($callback);
            } else {
                return _error($callback, 79, trans("messages.unknown"));
            }

        } else {
            return _error($callback, 79, trans("messages.liked_befor"));
        }

    }
    public function index(blog $blog, Request $request)
    {
        $data = [];
        $blog = new blog;
        $mymodel = new Mymodel;
        $query = DB::table('blogs');
        $consideredColumns = $mymodel->search($query, $request, $blog->getColumns(), 'blogs');
        $blogs = $query->orderBy('blogs.id', 'desc')->paginate(10);
        $pagination = $blogs->appends($consideredColumns)->links();
        // adminTableMaker($blogs,'blogs','blog');
        $data['blogs'] = $blogs;
        $data['pagination'] = $pagination;

        return view('admin/blog', $data);

    }
    // public function blog(Request $request)
    // {
    //     return view('admin/blog');
    // }
    public function blog_signle(Request $request)
    {
        return view('admin/blog-single');
    }
    public function create(Request $request, Blog $blog)
    {
        $data['blog'] = $blog;

        return view('admin/blog-create', $data);
    }
}
