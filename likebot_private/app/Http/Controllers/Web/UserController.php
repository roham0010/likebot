<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Web\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * @Route: /profile/dashboard
     */
    public function dashboard()
    {
        $likedPosts = \App\Liked_post::where('user_id', user()->id)->orderBy('id', 'desc')->paginate(15);
        return view('user.dashboard', compact('likedPosts'));
    }

    /**
     * @Route: /profile/extension/code/create
     */
    public function extensionCodeCreate()
    {
        do {
            $extensionCode = rand(100000, 999999);
        } while (User::where('extension_code', $extensionCode)->first());

        if (user()->update(['extension_code' => $extensionCode])) {
            return $this->_success(['redirectTo' => myRoute('profile.dashboard')]);
        } else {
            return $this->_error('Please try again later.');
        }
    }

}
