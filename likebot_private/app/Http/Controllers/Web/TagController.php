<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Web\Controller;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * @Route: /profile/tags/edit
     */
    public function edit(Request $request)
    {
        $tags = $request->tags;
        $tags = explode(',', $tags);
        $tags = array_map('trim', $tags);
        $tags = json_encode($tags);
        if (user()->update(['tags' => $tags])) {
            return $this->_success('OK');
        }
    }
}
