<?php

namespace App\Http\Controllers\Web;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function pricing()
    {
        $plans = consts('plans');
        $discounts = consts('discounts');
        return view('pricing', compact('plans', 'discounts'));
    }
    public function logout()
    {
        \Auth::logout();
        return myRedirect();
    }

}
