<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Web\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Override: login view
     */
    public function showLoginForm()
    {
        return view('auth.login1');
    }

    /**
     * Override: Get the login username to be used by the controller.
     */
    public function username()
    {
        if (is_numeric($_POST['username'])) {
            return 'mobile';
        }
        return 'email';
    }

    /**
     * check if entered username(phone or email) exists
     * @Route /checkUsername
     */
    public function checkUsername(Request $request)
    {
        $request->merge([$this->username() => $request->username]);
        $this->validate($request, [
            'username' => "required|" . $this->username(),
        ]);
        $user = \App\User::where($this->username(), $request->username)->first();
        if ($user && $user->id) {
            $profile = (!empty($user->instagram_profile_picture_url)) ? $user->instagram_profile_picture_url : asset('/build') . "/img/avatar.jpg";
            $data = ['profile_picture_url' => $profile, 'fullName' => $user->first_name, 'targetContainer' => '#user-login-with-email'];

            $data['actions'] = [
                ['#username-login-step1', 'addClass', 'collapse'],
                ['#user-login-with-username', 'removeClass', 'collapse'],
                ['.user-name', 'html', $user->name],
                ['.login-username', 'val', $request->username],
                ['.avatar-md img', 'prop', 'src', '$user->name'],
            ];
            // ee($data)
            return $this->_success($data);
        }
        $data['actions'] = [
            ['#user-register-with-email', 'removeClass', 'collapse'],
            ['.login-username', 'val', $request->username],
        ];
        return $this->_success($data);
    }

    /**
     * Override
     */

    public function login(Request $request)
    {
        $request->merge([$this->username() => $request->username]);
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    /**
     * Override
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user()) ?: $this->_success(['redirectTo' => myUrl('/profile/dashboard')]);
    }
}
