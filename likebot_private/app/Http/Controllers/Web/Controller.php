<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\MyController;

class Controller extends MyController
{
    // api response helpers
    public function json_return($data)
    {
        return response()->json($data, $data['code']);
    }

    public function _error($data = '', $status = 'false', $callback = null, $errCode = 400)
    {
        $callbackArray = $callback ? ['callback' => $callback] : [];
        $metadataArray = [];
        $messageArray = [];
        $dataArray = [];
        $message = (!empty($data) && (is_string($data) || is_numeric($data))) ? ['message' => $data] : [];
        $data = json_decode(json_encode($data), true);
        if (is_string($data) || empty($data)) {
            $messageArray = $data ? ['message' => $data] : [];
        } elseif (is_array($data) && count($data) > 0) {
            if (isset($data['metadata']) && count($data['metadata']) > 0) {
                $metadataArray = ['message' => $data['metadata']];
                $dataArray = ['result' => $data['data']];
            }
            $dataArray = ['result' => $data];
        }
        if (strpos(\Request::url(), '/api/') !== false || \Request::input('ajax')) {
            return self::json_return(array_merge(['status' => $status, 'code' => $errCode], $dataArray, $metadataArray, $messageArray, $callbackArray));
        } else {
            if (!empty(\Request::server('HTTP_REFERER'))) {
                return redirect()->back()->with($message);
            } else {
                return redirect('/profile')->with(['message' => trans('messages.error.access_denied')]);
            }

        }
    }

    public function _success($data = '', $callback = null, $status = 'OK', $errCode = 200)
    {
        $callbackArray = $callback ? ['callback' => $callback] : [];
        $metadataArray = [];
        $messageArray = [];
        $dataArray = [];
        $message = (!empty($data) && (is_string($data) || is_numeric($data))) ? ['message' => $data] : [];
        $data = json_decode(json_encode($data), true);
        if (is_string($data) || empty($data)) {
            $messageArray = $data ? ['message' => $data] : [];
        } elseif (is_array($data) && count($data) > 0) {
            if (isset($data['metadata']) && count($data['metadata']) > 0) {
                // $metadataArray = ['metadata' => $data['metadata']];
                $dataArray = ['result' => $data['data']];
            } else {
                $dataArray = ['result' => $data];
            }

        }
        if (strpos(\Request::url(), '/api/') !== false || \Request::input('ajax')) {
            return self::json_return(array_merge(['status' => $status, 'code' => $errCode], $metadataArray, $dataArray, $messageArray, $callbackArray));
        } else {
            if (!empty(\Request::server('HTTP_REFERER'))) {
                return redirect()->back()->with($message);
            } else {
                return redirect('/profile')->with($message);
            }
        }
    }

    public function _form_error($data = '', $status = 'formError', $callback = null, $errCode = 400)
    {
        $callbackArray = $callback ? ['callback' => $callback] : [];
        $metadataArray = [];
        $messageArray = [];
        $dataArray = [];
        $message = (!empty($data) && (is_string($data) || is_numeric($data))) ? ['message' => $data] : [];
        $data = json_decode(json_encode($data), true);
        if (is_string($data) || empty($data)) {
            $messageArray = $data ? ['message' => $data] : [];
        } elseif (is_array($data) && count($data) > 0) {
            if (isset($data['metadata']) && count($data['metadata']) > 0) {
                $metadataArray = ['message' => $data['metadata']];
                $dataArray = ['result' => $data['data']];
            }
            $dataArray = ['result' => $data];
        }
        if (strpos(\Request::url(), '/api/') !== false || \Request::input('ajax')) {
            return self::json_return(array_merge(['status' => $status, 'code' => $errCode], $dataArray, $metadataArray, $messageArray, $callbackArray));
        } else {
            if (!empty(\Request::server('HTTP_REFERER'))) {
                return redirect()->back()->withInput()->withErrors($formErrors);
            } else {
                return redirect('/profile')->withErrors($formErrors);
            }

        }
    }
    public function isApiRequest()
    {
        return (strpos(\Request::fullUrl(), '/api/') !== false);
    }
}
