<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/cron/f90dsafsuf0fjsd9a0fsuf0700y5klvr4y8b9tqut4q8t4q270/telegram/get',
    ];
}
