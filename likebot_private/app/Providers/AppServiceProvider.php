<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        if (!isset($_SESSION) && !\Request::is('api*')) {
            session_start();
        }
        Validator::extend('mobile', function ($attribute, $value, $parameters, $validator) {
            if (!preg_match('/(0|)9(0[1-3]|1[0-9]|3[0-9]|2[0-9]|9[0-1])-?[0-9]{3}-?[0-9]{4}$/', $value) && !preg_match('/\+989(0[1-3]|1[0-9]|3[0-9]|2[0-9]|9[0-1])-?[0-9]{3}-?[0-9]{4}$/', $value)) {
                return false;
            }
            return true;
        });
        if (\Request::has('local')) {
            App::setLocale(\Request::input('local'));
        } else {
            $locale = \Request::segment(1);

            if (!in_array($locale, array_keys(\Config::get('app.locales')))) {
                $locale = 'fa';
            }
            App::setLocale($locale);
            // $locale = App::getLocale();
            // ee($locale);

        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
