var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
/*
elixir(function(mix) {
	mix.less("mini-style.less");
    mix.version("css/mini-style.css");
});*/
elixir(function(mix) {
    mix.scripts([
        'ajax-form.js',
	], 'likebot.ir/build/js/myapp.js');
});
//elixir(function(mix) {
//	mix.less('../../../public/build/less/style.less', 'public/build/css/style.css');
//	mix.version( 'build/css/style.css');
//});